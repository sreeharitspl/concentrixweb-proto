import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { CanDashboardComponent } from './core/modules/cnx-dashboard/can-dashboard/can-dashboard.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent, pathMatch: 'full' },
    { path: 'auth',  loadChildren: './core/modules/cnx-dashboard/cnx-dashboard.module#CnxDashboardModule'  },
    { path: 'report', loadChildren: './report/report.module#ReportModule' },
    { path: '',      redirectTo: 'login', pathMatch: 'full'  },
    { path: '**',    redirectTo: 'login', pathMatch: 'full'  },
  ];

  @NgModule({
    imports: [ RouterModule.forRoot(routes, { useHash: true }) ],
    exports: [ RouterModule ],
    providers: []
  })

  export class AppRoutingModule {}
