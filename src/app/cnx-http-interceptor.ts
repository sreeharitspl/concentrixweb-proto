/*
 * Author: T0423
 * Date: 05/Sept/2018
 * Description: HTTP Interceptor for the application
 * 
 * */

import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CnxGlobalService } from './shared/modules/cnx-utils/cnx-global/cnx-global.service';


@Injectable()
export class CnxHttpInterceptor implements HttpInterceptor {

    constructor(private cnxGlobalService: CnxGlobalService) { }

    public countHit = 0;
    
    // intercept request and add token
  	intercept(request: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>> {
        this.countHit += 1;
	    return next.handle(request)
	    .pipe(
	        tap(event => {
	          if (event instanceof HttpResponse) {
                this.countHit -= 1;
                if(this.countHit == 0) {
                    this.cnxGlobalService.hideLoader();
                }
	          }
	        }, error => {
                this.countHit -= 1;
                if(this.countHit == 0) {
                    this.cnxGlobalService.hideLoader();
                }
	        })
	      )

    };



}