import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap';

import { LoginComponent } from './login/login.component';
import { SharedModule } from '../shared/shared.module';
import { AuthService } from './auth-service.service';
import { CnxTermsofuseandpolicyComponent } from '../shared/modules/cnx-termsofuseandpolicy/cnx-termsofuseandpolicy/cnx-termsofuseandpolicy.component';
import { CnxFooterModule } from '../shared/modules/cnx-footer/cnx-footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    SharedModule,
    CnxFooterModule
  ],
  declarations: [
    LoginComponent,
    CnxTermsofuseandpolicyComponent
  ],
  entryComponents: [
    CnxTermsofuseandpolicyComponent
  ]
})
export class AuthModule { }
