import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, NgForm, Validators, ReactiveFormsModule } from '@angular/forms';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { CnxLanguageService } from '../../shared/service/cnx-language/cnx-language.service';
import { AuthService } from '../auth-service.service';
import { CnxLocalStorageService } from '../../shared/service/cnx-local-storage/cnx-local-storage.service';
import { CnxConstants } from '../../shared/modules/cnx-utils/cnx-constants/cnx-constants';
import { CnxGlobalService } from '../../shared/modules/cnx-utils/cnx-global/cnx-global.service';
import { CnxTermsofuseandpolicyComponent } from '../../shared/modules/cnx-termsofuseandpolicy/cnx-termsofuseandpolicy/cnx-termsofuseandpolicy.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public modalService: BsModalService, public cnxLanguageService: CnxLanguageService,
              public authservice: AuthService, public localStorage: CnxLocalStorageService,
              public globalService: CnxGlobalService) { }

  public loginForm: FormGroup;
  public modalRef: BsModalRef;
  public isFormSubmitted = false;

  ngOnInit() {
    // Initialising form values with default values
    this.loginForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required]),
      'rememberMe': new FormControl(false),
      'acceptTerms': new FormControl(false)
    });
    this.getLoginCredentials();
  }

  /*
  * Author: Tspl-0518
  * Function: getTermsOfUse
  * Description: Show Concentrix privacy policy popup
  * Arguments: Nil
  * Return: Nil
  */
  public showPrivacyPolicy() {
    const initialState = {
      contentType: CnxConstants.POLICYANDTERMSKEYS.LGN_PRIVACY_POLICY
    };

    this.modalRef = this.modalService.show(CnxTermsofuseandpolicyComponent,
      { initialState, ignoreBackdropClick: false, class: 'modal-lg' });
  }


  /*
  * Author: Tspl-0518
  * Function: getTermsOfUse
  * Description:  Show Concentrix terms of use popup
  * Arguments: Nil
  * Return: Nil
  */
  public getTermsOfUse() {
    const initialState = {
      contentType: CnxConstants.POLICYANDTERMSKEYS.MENU_HELP_TERMS_OF_USE
    };

    this.modalRef = this.modalService.show(CnxTermsofuseandpolicyComponent,
      { initialState, ignoreBackdropClick: false, class: 'modal-lg' });
  }

  /*
   * Author: T0510
   * Function: getLoginCredentials
   * Description:  Function to get the login credentials
   * Arguments: Nil
   * Return: Nil
   */
  public getLoginCredentials() {
    const savedUserName = this.localStorage.getItem(CnxConstants.LOCALSTORAGEKEYS.CNX_SELECTED_USER_NAME);
    const savedPassword = this.localStorage.getItem(CnxConstants.LOCALSTORAGEKEYS.CNX_SELECTED_PASSWORD);

    if (savedPassword && savedUserName) {
      this.loginForm.patchValue({
        'email': savedUserName,
        'password': savedPassword,
        'rememberMe': true,
        'acceptTerms': true
      });
    }
  }

  /*
   * Author: T0510
   * Function: loginUser
   * Description:  For user login
   * Arguments: Nil
   * Return: Nil
   */
  public loginUser() {
    this.isFormSubmitted = true;
    const isValid = this.loginForm.valid;

    if (this.loginForm.valid) {
      if (this.loginForm.value.acceptTerms === false) {
        alert('please accept terms and conditions');
      } else {
        // TODO: Need to remove the header constants
        const params = {
          Email: this.loginForm.value.email,
          Password: this.loginForm.value.password,
          UserName: this.loginForm.value.email,
          DeviceId: 'no_registration_id',
          DeviceInfo: {},
          Language: 'en-Us',
          Latitude: '',
          Longitude: '',
          MobileAppVersion: '1.6.14',
          UserType: 1
        };

        if (this.loginForm.value.rememberMe) {
          // Remember me feature
          this.localStorage.setItem(CnxConstants.LOCALSTORAGEKEYS.CNX_SELECTED_USER_NAME, this.loginForm.value.email);
          this.localStorage.setItem(CnxConstants.LOCALSTORAGEKEYS.CNX_SELECTED_PASSWORD, this.loginForm.value.password);
        } else {
          // Clear local storage for username and password
          this.localStorage.removeItem(CnxConstants.LOCALSTORAGEKEYS.CNX_SELECTED_USER_NAME);
          this.localStorage.removeItem(CnxConstants.LOCALSTORAGEKEYS.CNX_SELECTED_PASSWORD);
        }

        const showLoader = true;
        this.authservice.userLogin('ValidateUser', params, (respData: any) => {
          if (respData.Acknowledge === 1) {
            this.localStorage.setItem(CnxConstants.LOCALSTORAGEKEYS.CNX_TOKEN, respData.Token);
            this.localStorage.setItem(CnxConstants.LOCALSTORAGEKEYS.CNX_LOGIN_USER_DETAILS, JSON.stringify(respData));
            this.globalService.navigateTo(['auth', 'content', 'feeds', 'timeline']);
          } else {
            alert(respData.Message);
          }
        },
          (errData: any) => {
            alert(errData.statusText);
          }, showLoader);
      }
    }
  }

}
