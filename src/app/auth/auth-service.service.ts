/*
 * @Author: T0510
 * @Created On: 06-Sep-2018
 */
import { Injectable } from '@angular/core';

import { HttpService } from '../shared/service/http/http.service';
import { CnxGlobalService } from '../shared/modules/cnx-utils/cnx-global/cnx-global.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpService: HttpService, private cnxGlobalService: CnxGlobalService) { }

  /*
   * Author: T0510
   * Function: userLogin
   * Description:  For Validating user login details
   */
  public userLogin(url, params, cbSuccess, cbError, showLoader?) {
    if (showLoader) {
      this.cnxGlobalService.showLoader();
    }
    this.httpService.postX(url, params)
    .subscribe(result => {
      cbSuccess(result);
    },
    error => cbError(error)
    );
  }

}
