import { CnxLcsDetailviewModule } from './cnx-lcs-detailview.module';

describe('CnxLcsDetailviewModule', () => {
  let cnxLcsDetailviewModule: CnxLcsDetailviewModule;

  beforeEach(() => {
    cnxLcsDetailviewModule = new CnxLcsDetailviewModule();
  });

  it('should create an instance', () => {
    expect(cnxLcsDetailviewModule).toBeTruthy();
  });
});
