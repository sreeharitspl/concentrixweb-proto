import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxPostShareModalComponent } from './cnx-post-share-modal.component';

describe('CnxPostSharePopupComponent', () => {
  let component: CnxPostShareModalComponent;
  let fixture: ComponentFixture<CnxPostShareModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxPostShareModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxPostShareModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
