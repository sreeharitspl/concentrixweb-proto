import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxPostShareModalComponent } from './cnx-post-share-modal.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { TranslatePipe } from '../../../pipes/translate/translate.pipe';
import { SharedModule } from '../../../shared.module';
import { CnxSharedUserDetailsModule } from '../../cnx-shared-user-details/cnx-shared-user-details.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    InfiniteScrollModule,
    FormsModule,
    ReactiveFormsModule,
    CnxSharedUserDetailsModule
  ],
  declarations: [CnxPostShareModalComponent],
  exports: [CnxPostShareModalComponent],
  providers: [
    TranslatePipe
  ],
})
export class CnxPostShareModalModule { }
