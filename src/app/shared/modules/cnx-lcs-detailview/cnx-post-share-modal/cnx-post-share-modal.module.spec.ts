import { CnxPostShareModalModule } from './cnx-post-share-modal.module';

describe('CnxPostShareModalModule', () => {
  let cnxPostShareModalModule: CnxPostShareModalModule;

  beforeEach(() => {
    cnxPostShareModalModule = new CnxPostShareModalModule();
  });

  it('should create an instance', () => {
    expect(cnxPostShareModalModule).toBeTruthy();
  });
});
