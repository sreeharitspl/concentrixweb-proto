/**
 * Author: TSPL-T0445
 * Date Created: 05 Sept 2018
 */
import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { BsModalRef } from 'ngx-bootstrap';

import { CnxPostLcsService } from '../../cnx-post-lcs/cnx-post-lcs.service';
import { ShareRequestDetails } from '../../../models/cnx-lcs-models/cnx-share-request.model';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';

@Component({

  selector: 'app-cnx-post-share-modal',
  templateUrl: './cnx-post-share-modal.component.html',
  styleUrls: ['./cnx-post-share-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})

/**
 * Author: TSPL-T0445
 * Created Date: 05 Sept 2018
 * Class Name: CnxPostShareModalComponent
 * Description: Modal to share the feed/post.
 */
export class CnxPostShareModalComponent implements OnInit {

  submitted: boolean;
  descriptionMaxLength: number;
  shareTitle: string;

  shareForm: FormGroup;

  shareDetails: ShareRequestDetails;
  @Input() feedDetails;

  constructor (
    private shareModalRef: BsModalRef,
    private postLCSService: CnxPostLcsService,
    public cnxLanguageService: CnxLanguageService
    ) {
    this.submitted = false;
    }

  ngOnInit() {
    this.descriptionMaxLength = CnxConstants.NUMERICCONSTANTS.CNX_DESCRIPTIONMAXLENGTH;
    this.shareTitle =  CnxConstants.SHARETITLELANGUAGEKEY[this.feedDetails.FeedType];
    this.shareForm = new FormGroup({
      description: new FormControl(
        '',
        [Validators.required,
        Validators.maxLength(this.descriptionMaxLength)]
        ),
    });

    this.shareDetails = new ShareRequestDetails();

  }

 /**
  * Author: TSPL-T0445
  * Created Date: 05 Sept 2018
  * Method Name: hideSharePopup
  * Description: To hide the popup.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
  hideSharePopup() {

    this.shareModalRef.hide();

  }

 /**
  * Author: TSPL-T0445
  * Created Date: 06 Sept 2018
  * Method Name: sharePost
  * Description: To share the post using service.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
  sharePost() {

    this.submitted = true;

    this.shareDetails.ContentId = this.feedDetails.ContentId;
    this.shareDetails.SharedDiscription = this.shareForm.controls.description.value;

    if (this.shareForm.valid) {
      this.shareModalRef.hide();
      this.postLCSService.selectUsersToShare(this.shareDetails);
    }
  }
}
