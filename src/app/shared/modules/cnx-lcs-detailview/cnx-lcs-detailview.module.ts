import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { CnxLCSUserListModalComponent } from './cnx-lcs-user-list-modal/cnx-lcs-user-list-modal.component';
import { CnxPostShareModalComponent } from './cnx-post-share-modal/cnx-post-share-modal.component';
import { PipeModule } from '../../pipes/pipe/pipe.module';
import { CnxCommentListComponent } from './cnx-comment-list/cnx-comment-list.component';
import { CnxCommentModalComponent } from './cnx-comment-modal/cnx-comment-modal.component';
import { CnxUserImageModule } from '../cnx-user-image/cnx-user-image.module';
import { SharedModule } from '../../shared.module';
import { CnxUserDetailsCardModule } from '../cnx-user-details-card/cnx-user-details-card.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CnxSharedUserDetailsModule } from '../cnx-shared-user-details/cnx-shared-user-details.module';
import { CnxCommentListModule } from './cnx-comment-list/cnx-comment-list.module';
import { CnxCommentModalModule } from './cnx-comment-modal/cnx-comment-modal.module';
import { CnxPostShareModalModule } from './cnx-post-share-modal/cnx-post-share-modal.module';
import { CnxLcsUserListModalModule } from './cnx-lcs-user-list-modal/cnx-lcs-user-list-modal.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    // PipeModule,
    CnxUserImageModule,
    // SharedModule,
    CnxUserDetailsCardModule,
    InfiniteScrollModule,
    CnxSharedUserDetailsModule,
    CnxCommentListModule,
    CnxCommentModalModule,
    CnxLcsUserListModalModule,
    CnxPostShareModalModule
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class CnxLcsDetailviewModule { }
