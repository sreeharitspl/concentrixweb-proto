import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxCommentModalComponent } from './cnx-comment-modal.component';
import { TranslatePipe } from '../../../pipes/translate/translate.pipe';
import { SharedModule } from '../../../shared.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CnxCommentListModule } from '../cnx-comment-list/cnx-comment-list.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    InfiniteScrollModule,
    CnxCommentListModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [CnxCommentModalComponent],
  exports: [CnxCommentModalComponent],
  providers: [
    TranslatePipe
  ],
})
export class CnxCommentModalModule { }
