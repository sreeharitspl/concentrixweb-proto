 /*
  * Author: TSPL T0475
  * Created on: 05-Sep-2018
  * Description:
    This component will show modal in which all comment given to particular post is listed and option to add new comments
  */
import { Component, OnInit, Input } from '@angular/core';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { ChangeDetectionStrategy } from '@angular/core';

import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxPostLcsService } from '../../cnx-post-lcs/cnx-post-lcs.service';

import { CnxComments } from '../../../interfaces/cnx-comments';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';

@Component({
  selector: 'app-cnx-comment-modal',
  templateUrl: './cnx-comment-modal.component.html',
  styleUrls: ['./cnx-comment-modal.component.scss']
})
export class CnxCommentModalComponent implements OnInit {

 /* Infinit scroll properties */
  public invokedStatus: boolean;
  public lodingStatus: boolean;
  public throttle: number;
  public scrollDistance: number;
  /* Ends */

 /*  input object defining */
  public commentParams: {
    ArticleVideoType: string;
    ContentId: string,
    PageNumber: number;
    PerPage: number;
  };
  /* End */

/** comment form declaration */
  public commentForm: FormGroup;

/** bool variable to store whether form is submitted or not */
  public submitted: boolean;

/** variable to store comment list */
  public commentList: CnxComments[] = [];
  public commentCount: number;

/*  Values got from intialstate of Modal */
  // public contentId;
  // public feedType;
  // public feedDetails;

/*from aprent*/
 @Input() feedDetails: any;
 @Input() feedType: any;
 @Input() contentId: any;

/** variable to set number of comment list in each getCommentList() call*/
  public pageNumber: number;
  public perPage: number;
  descriptionMaxLength: number;

  constructor(public commentModalRef: BsModalRef,
              public cnxLanguageService: CnxLanguageService,
              private postLcsService: CnxPostLcsService,
            ) {
  }

  ngOnInit() {
    this.invokedStatus = true;
    this.lodingStatus = this.feedDetails.CommentCount > 0;
    this.throttle = CnxConstants.MODALSCROLLPARAMETERS.Throttle;
    this.scrollDistance = CnxConstants.MODALSCROLLPARAMETERS.ScrollDistance;
    this.pageNumber = 0;
    this.perPage = CnxConstants.PAGINATIONLENGTH.CNX_MODEL_PAGINATION_COUNT;
    this.submitted = false;
    this.descriptionMaxLength = CnxConstants.NUMERICCONSTANTS.CNX_COMMENTLENGTH;
    this.commentList = [];
    this.commentForm = new FormGroup({
      comment: new FormControl('', [Validators.required, Validators.maxLength(this.descriptionMaxLength)]),
    });

    this.getCommentList();

  }

  /*
  * Author: TSPL T0475
  * Created on :05-Sep-2018
  * Function: onCommentSubmit
  * Description: function to check whether form is valid or not.if valid modal is closed function for save comment is called
  * Return:nil
  */
  onCommentSubmit() {
    this.submitted = true;
    if (this.commentForm.valid) {
      this.saveComment();
      this.commentModalRef.hide();
    }
  }

  /*
  * Author: TSPL T0475
  * Created on :05-Sep-2018
  * Function: getCommentList
  * Description:function to get all comments of particular contentId
  * Return:commentList - list of comments
  */
  getCommentList() {
     if (this.invokedStatus && this.feedDetails.CommentsCount > 0) {
         this.invokedStatus = false;
         this.lodingStatus = true;
         this.commentParams = {
          ArticleVideoType: this.feedType,
          ContentId: this.contentId,
          PageNumber: this.pageNumber += 1,
          PerPage: this.perPage
        };
        this.postLcsService.getCommentList('GetAllArticleVideoCommentsList', this.commentParams, (respData: any) => {
          if (respData.Acknowledge === 1) {
            // tslint:disable-next-line:max-line-length
            if (respData && respData.CommentList && respData.CommentList.length < CnxConstants.PAGINATIONLENGTH.CNX_MODEL_PAGINATION_COUNT) {
              this.invokedStatus = false;
              this.lodingStatus = false;
            } else {
              this.lodingStatus = true;
              this.invokedStatus = true;
            }
            this.setCommentData(respData.CommentList);
          } else {
            this.lodingStatus = false;
            alert(respData.Message);
          }
        },
        (errData: any) => {
          this.invokedStatus = true;
          this.lodingStatus = false;
          alert(errData.statusText);
        });
     }
  }

/*
  * Author: TSPL T0475
  * Created on :05-Sep-2018
  * Function: setCommentData
  * Description: function will append comment list (given as parameter) to global comment list
  * Arguments: Param 1: commentData : CnxComments
  * Return:nil
  */
 setCommentData(commentData) {
   this.commentList = this.commentList.concat(commentData);
 }

  /*
  * Author: TSPL T0475
  * Created on :05-Sep-2018
  * Function: saveComment
  * Description:function to save newly added comments
  * Return:error message
  */
  saveComment() {
     const params = {
      ArticleVideoType: this.feedType,
      CommentId: 0,
      Comments: this.commentForm.value.comment,
      ContentId: this.contentId,
      ModeType: 'Insert',
    };
    if (this.feedDetails && this.feedDetails.CommentsCount) {
      this.feedDetails.CommentsCount += 1;
    }
    this.postLcsService.saveComment('UpdateArticleVideoComments', params, (respData: any) => {
      if (respData && respData.Acknowledge === 1) {
        this.feedDetails.CommentsCount = respData.CommentsCount;
      } else {
        if (this.feedDetails && this.feedDetails.CommentsCount) {
          if (this.feedDetails.CommentsCount >= 0 ) {
            this.feedDetails.CommentsCount -= 1;
          }
        }
        alert(respData.Message);
      }
    },
    (errData: any) => {
      if (this.feedDetails && this.feedDetails.CommentsCount) {
        if (this.feedDetails.CommentsCount >= 0 ) {
          this.feedDetails.CommentsCount -= 1;
        }
      }
      alert(errData.statusText);
    });
  }
}
