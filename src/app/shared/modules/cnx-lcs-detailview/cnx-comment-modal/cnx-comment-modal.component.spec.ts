import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxCommentModalComponent } from './cnx-comment-modal.component';

describe('CnxCommentModalComponent', () => {
  let component: CnxCommentModalComponent;
  let fixture: ComponentFixture<CnxCommentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxCommentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxCommentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
