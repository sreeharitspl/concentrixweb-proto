import { CnxCommentModalModule } from './cnx-comment-modal.module';

describe('CnxCommentModalModule', () => {
  let cnxCommentModalModule: CnxCommentModalModule;

  beforeEach(() => {
    cnxCommentModalModule = new CnxCommentModalModule();
  });

  it('should create an instance', () => {
    expect(cnxCommentModalModule).toBeTruthy();
  });
});
