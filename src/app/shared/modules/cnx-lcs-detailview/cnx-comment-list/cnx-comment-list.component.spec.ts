import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxCommentListComponent } from './cnx-comment-list.component';

describe('CnxCommentListComponent', () => {
  let component: CnxCommentListComponent;
  let fixture: ComponentFixture<CnxCommentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxCommentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxCommentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
