import { CnxCommentListModule } from './cnx-comment-list.module';

describe('CnxCommentListModule', () => {
  let cnxCommentListModule: CnxCommentListModule;

  beforeEach(() => {
    cnxCommentListModule = new CnxCommentListModule();
  });

  it('should create an instance', () => {
    expect(cnxCommentListModule).toBeTruthy();
  });
});
