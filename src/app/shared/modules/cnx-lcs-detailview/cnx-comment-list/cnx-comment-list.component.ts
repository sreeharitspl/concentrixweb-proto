 /*
  * Author: TSPL T0475
  * Created on: 05-Sep-2018
  * Description:
    This component to display all comment given to particular post
  */
import { Component, OnInit, Input } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';

import { CnxComments } from '../../../interfaces/cnx-comments';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { CnxCOnfirmationComponent } from '../../cnx-confirmation/cnx-confirmation/cnx-confirmation.component';

@Component({
  selector: 'app-cnx-comment-list',
  templateUrl: './cnx-comment-list.component.html',
  styleUrls: ['./cnx-comment-list.component.scss']
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class CnxCommentListComponent implements OnInit {
  enableConfirmModel: boolean;
  @Input() commentItem: CnxComments;
  confirmationModel: BsModalRef;

  constructor(
    public cnxLanguageService: CnxLanguageService,
    private confrimationsModel: BsModalService
    ) {
    this.enableConfirmModel = false;
   }

  ngOnInit() { }
    /*
  * Author: TSPL-0446
  * Function: getDefaultIcon
  * Description:  Function return static icon based on the key.
  * Arguments: string : Type of icon
  * Return: return coresponding icon url.
  */
 getDefaultIcon(type) {
  return CnxConstants.CNX_DEFAULT_STATICICON_GET[type];
}

  commentDelete(commentDetails: {}) {
    const initialState = {
      commentdata : commentDetails,
      confirmationMsg: this.cnxLanguageService.languageDictionary.DELETE_COMMENT_MSG
    };
    this.confirmationModel = this.confrimationsModel.show(CnxCOnfirmationComponent,
          {
            initialState,
            backdrop: true,
            ignoreBackdropClick: false // When its true - model will not close on outside click
          }
        );
       /* this.confirmationModel.content.title = 'Save Modal';
        this.confirmationModel.content.event.subscribe(modaldata => {
          this.commentDeleteemitter(modaldata);
        });*/
  }
  commentDeleteemitter(data: any) {
    alert('inside');
  }
}
