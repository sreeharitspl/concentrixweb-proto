import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxCommentListComponent } from './cnx-comment-list.component';
import { CnxUserImageModule } from '../../cnx-user-image/cnx-user-image.module';
import { SharedModule } from '../../../shared.module';
import { TranslatePipe } from '../../../pipes/translate/translate.pipe';

@NgModule({
  imports: [
    CommonModule,
    CnxUserImageModule,
    SharedModule
  ],
  declarations: [CnxCommentListComponent],
  exports: [CnxCommentListComponent],
  providers: [
    TranslatePipe
  ],
})
export class CnxCommentListModule { }
