import { CnxLcsUserListModalModule } from './cnx-lcs-user-list-modal.module';

describe('CnxLcsUserListModalModule', () => {
  let cnxLcsUserListModalModule: CnxLcsUserListModalModule;

  beforeEach(() => {
    cnxLcsUserListModalModule = new CnxLcsUserListModalModule();
  });

  it('should create an instance', () => {
    expect(cnxLcsUserListModalModule).toBeTruthy();
  });
});
