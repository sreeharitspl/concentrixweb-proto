/**
 * Author: TSPL-T0445
 * Date Created: 10 Sept 2018
 */
import { Component, OnInit, Input } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap';

import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { CnxPostLcsService } from '../../cnx-post-lcs/cnx-post-lcs.service';
import { CnxUserLCSData } from '../../../models/cnx-lcs-models/cnx-user-lcs-data.model';
import { CnxLcsUserListRequest } from '../../../models/cnx-lcs-models/cnx-lcs-user-list-request.model';
import { CnxLCSUserListResponse } from '../../../models/cnx-lcs-models/cnx-lcs-user-list-response.model';
import { CnxLcsUserListModalData } from '../../../models/cnx-lcs-models/cnx-lcs-user-list-modal-data.model';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';

@Component({
  selector: 'app-cnx-lcs-user-list-modal',
  templateUrl: './cnx-lcs-user-list-modal.component.html',
  styleUrls: ['./cnx-lcs-user-list-modal.component.scss'],
})

/**
 * Author: TSPL-T0445
 * Created Date: 10 Sept 2018
 * Class Name: CnxLCSUserListModalComponent
 * Description: Modal to view the shared users.
 */
export class CnxLCSUserListModalComponent implements OnInit {

  backButtonVisibility: boolean;
  invokedStatus: boolean;
  lodingStatus: boolean;
  modalTitle: string;
  pageNumber: number;
  perPage: number;
  throttle: number;
  scrollDistance: number;
  scrollUpDistance: number;
  parentContentId: number;
  hideBack: boolean;

  userList: CnxUserLCSData[];
  @Input() lcsUserListModalData: CnxLcsUserListModalData;


  constructor(
    private sharedUserListModalRef: BsModalRef,
    private lcsService: CnxPostLcsService,
    public cnxLanguageService: CnxLanguageService
     ) { }

  ngOnInit() {
    this.parentContentId = this.lcsUserListModalData.ContentId;
    this.initializeProperties();
    this.getUserList();
  }

 /**
  * Author: TSPL-T0445
  * Created Date: 13 Sept 2018
  * Method Name: initializeProperties
  * Description: To initialize the properties of the user list modal.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
  initializeProperties() {
    this.lodingStatus = true;
    this.invokedStatus = true;
    this.pageNumber = 0;
    this.userList = [];
    this.modalTitle =  CnxConstants.LCSACTIONTYPETITLEKEY[this.lcsUserListModalData.ActionType];
    this.perPage = CnxConstants.PAGINATIONLENGTH.CNX_MODEL_PAGINATION_COUNT;
    this.throttle = CnxConstants.MODALSCROLLPARAMETERS.Throttle;
    this.scrollDistance = CnxConstants.MODALSCROLLPARAMETERS.ScrollDistance;
    this.scrollUpDistance = CnxConstants.MODALSCROLLPARAMETERS.ScrollUpDistance;
    this.backButtonVisibility = this.lcsUserListModalData.ActionType === CnxConstants.LCSACTIONTYPEKEY.Share
      && this.lcsUserListModalData.StatusFor === CnxConstants.LCSSTATUSFORKEY.sharedWithPeople && !this.hideBack ;
   }



 /**
  * Author: TSPL-T0445
  * Created Date: 10 Sept 2018
  * Method Name: hideUserListPopup
  * Description: To hide the shared user list popup.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
  hideUserListPopup() {
   this.sharedUserListModalRef.hide();
 }

   /**
  * Author: TSPL-T0445
  * Created Date: 13 Sept 2018
  * Method Name: getUserList
  * Description: To get the user list according to the content id and status for.
  * Arguments:
  *     contentId: number: Unique Id for Shared Content
  *     statusFor: string: Status for key to identify the type of user list.
  * Return:
  *     void
  */
getUserList() {
  if (this.invokedStatus) {
    let lcsUserListRequest: CnxLcsUserListRequest;
    lcsUserListRequest = this.prepareUserListRequestData();

    this.lcsService.getLCSUserList(lcsUserListRequest, (sharedUserResponse: CnxLCSUserListResponse) => {
      if (sharedUserResponse && sharedUserResponse.LikedSharedDetails &&
        sharedUserResponse.LikedSharedDetails.length < CnxConstants.PAGINATIONLENGTH.CNX_MODEL_PAGINATION_COUNT) {
        this.invokedStatus = false;
        this.lodingStatus = false;
      }
      this.userList = this.userList.concat(sharedUserResponse.LikedSharedDetails);
    },
    (errData: any) => {
      console.log('Something went wrong', errData);
    });

  }
}

 /*
  * Author: TSPL-T0445
  * Created on :14-Sep-2018
  * Function: prepareUserListRequestData
  * Description:To prepare User list request data.
  * Return:commentList - list of comments
  */
prepareUserListRequestData(): CnxLcsUserListRequest {

    let lcsUserListRequest: CnxLcsUserListRequest;
    lcsUserListRequest = new CnxLcsUserListRequest();
    lcsUserListRequest.ContentId = this.lcsUserListModalData.ContentId;
    lcsUserListRequest.MediaType = this.lcsUserListModalData.MediaType;
    lcsUserListRequest.PageNumber = ++this.pageNumber;
    lcsUserListRequest.PerPage = this.perPage;
    lcsUserListRequest.StatusFor = this.lcsUserListModalData.StatusFor;
    return lcsUserListRequest;

}

 /*
  * Author: TSPL-T0445
  * Created on :05-Sep-2018
  * Function: getUserListOnScroll
  * Description:function to get all users on scroll
  * Return: Nil
  */
 getUserListOnScroll() {
  this.getUserList();
 }

 /*
  * Author: TSPL-T0445
  * Created on :14-Sep-2018
  * Function: showSharedWithUsers
  * Description:function to get all users on scroll
  * Arguments:
  *       userListModalData: CnxLcsUserListModalData: User list Modal view model.
  * Return:
  *       Nil
  */
showSharedWithUsers(userListModalData: CnxLcsUserListModalData) {
  this.lcsUserListModalData = userListModalData;

  this.initializeProperties();
  this.getUserList();
}

 /**
  * Author: TSPL-T0445
  * Created Date: 04 Sept 2018
  * Method Name: getSharedPeople
  * Description: To get shared people on back button click.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
 getSharedPeople() {

  this.lcsUserListModalData = {
    ContentId : this.parentContentId,
    MediaType : this.lcsUserListModalData.MediaType,
    ActionType: CnxConstants.LCSACTIONTYPEKEY.Share,
    StatusFor : CnxConstants.LCSSTATUSFORKEY.Share,
    SharedWithPeopleView: true,
    ActionIcon: CnxConstants.LCSACTIONICONKEY.Share
  };

   this.initializeProperties();
   this.getUserList();
}

 /**
  * Author: TSPL-T0445
  * Created Date: 12 Sept 2018
  * Method Name: trackByFn
  * Description: track by function.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
 trackByFn(index, item) {
  return index;
  }
}
