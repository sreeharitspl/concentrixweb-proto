import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared.module';
import { TranslatePipe } from '../../../pipes/translate/translate.pipe';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CnxUserDetailsCardModule } from '../../cnx-user-details-card/cnx-user-details-card.module';
import { CnxSharedUserDetailsModule } from '../../cnx-shared-user-details/cnx-shared-user-details.module';
import { CnxLCSUserListModalComponent } from './cnx-lcs-user-list-modal.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    InfiniteScrollModule,
    CnxUserDetailsCardModule,
    CnxSharedUserDetailsModule
  ],
  declarations: [CnxLCSUserListModalComponent],
  exports: [CnxLCSUserListModalComponent],
  providers: [TranslatePipe]
})
export class CnxLcsUserListModalModule { }
