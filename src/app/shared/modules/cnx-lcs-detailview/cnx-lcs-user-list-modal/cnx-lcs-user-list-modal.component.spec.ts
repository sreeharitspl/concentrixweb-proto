import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxLCSUserListModalComponent } from './cnx-lcs-user-list-modal.component';

describe('CnxSharedUserListModalComponent', () => {
  let component: CnxLCSUserListModalComponent;
  let fixture: ComponentFixture<CnxLCSUserListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxLCSUserListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxLCSUserListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
