import { Component, OnInit, Input } from '@angular/core';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { CnxLanguageService } from '../../service/cnx-language/cnx-language.service';
@Component({
  selector: 'app-cnx-no-data-display',
  templateUrl: './cnx-no-data-display.component.html',
  styleUrls: ['./cnx-no-data-display.component.scss'],
})
export class CnxNoDataDisplayComponent implements OnInit {
  @Input() message: string;

  constructor(public cnxLanguageService: CnxLanguageService) { }

  ngOnInit() {
  }

}
