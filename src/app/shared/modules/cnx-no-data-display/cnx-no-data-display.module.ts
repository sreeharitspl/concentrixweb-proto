import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxNoDataDisplayComponent } from './cnx-no-data-display.component';
import { TranslatePipe } from '../../pipes/translate/translate.pipe';
import { SharedModule } from '../../shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [CnxNoDataDisplayComponent],
  exports: [CnxNoDataDisplayComponent],
})
export class CnxNoDataDisplayModule { }
