import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxNoDataDisplayComponent } from './cnx-no-data-display.component';

describe('CnxNoDataDisplayComponent', () => {
  let component: CnxNoDataDisplayComponent;
  let fixture: ComponentFixture<CnxNoDataDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxNoDataDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxNoDataDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
