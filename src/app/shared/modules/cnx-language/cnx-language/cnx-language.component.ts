
/*
 * @Author: T0423
 * @Date  : 05-Sept-2018
 */
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, NgForm, Validators } from '@angular/forms';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxGlobalService } from '../../cnx-utils/cnx-global/cnx-global.service';
import { CnxLocalStorageService } from '../../../service/cnx-local-storage/cnx-local-storage.service';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { CnxLanguageApiService } from '../cnx-language-api.service';

@Component({
  selector: 'app-cnx-language',
  templateUrl: './cnx-language.component.html',
  styleUrls: ['./cnx-language.component.scss']
})
export class CnxLanguageComponent implements OnInit {

  constructor(public langDataService: CnxLanguageService, private languageApiService: CnxLanguageApiService,
    private cnxGlobalService: CnxGlobalService, private localStorage: CnxLocalStorageService) { }

  public cnxLanguageForm: FormGroup;
  public availableLanguages: any[] = [];

  ngOnInit() {
    this.availableLanguages = this.langDataService.languageList;
    this.cnxLanguageForm = new FormGroup({
      'selectedLanguage': new FormControl({}, [Validators.required])
    });
    let selectedLanguage = this.localStorage.getItem(CnxConstants.LOCALSTORAGEKEYS.CNX_SELECTED_LANGUAGE);
    selectedLanguage = selectedLanguage ? selectedLanguage : CnxConstants.APPLICATIONCONSTANTS.CNX_DEFAULT_LANGUAGE;
    if (selectedLanguage) {
      // If there is any language avalable in local storage. Set the selected language with that oe set the same with default language
      this.cnxLanguageForm.patchValue({
        'selectedLanguage': this.langDataService.languageList.find(language => language.LanguageCode === selectedLanguage)
      });
      this.localStorage.setItem(CnxConstants.LOCALSTORAGEKEYS.CNX_SELECTED_LANGUAGE, selectedLanguage);
    }
  }

  public onLanguageSelect() {
    const params = {'Language': this.cnxLanguageForm.value.selectedLanguage.LanguageCode};
    const showLoader = true;
    this.languageApiService.getLanguageDictionary('GetResource', params, (respData: any) => {
      // Resource fetch success fallback, Saving the selected language in local storage
      this.localStorage.setItem(CnxConstants.LOCALSTORAGEKEYS.CNX_SELECTED_LANGUAGE,
                                this.cnxLanguageForm.value.selectedLanguage.LanguageCode);
    },
    (errData: any) => {
      alert(errData.statusText);
    }, showLoader);
  }

}
