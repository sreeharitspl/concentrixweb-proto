import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxLanguageComponent } from './cnx-language.component';

describe('CnxLanguageComponent', () => {
  let component: CnxLanguageComponent;
  let fixture: ComponentFixture<CnxLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
