import { TestBed, inject } from '@angular/core/testing';

import { CnxLanguageApiService } from './cnx-language-api.service';

describe('CnxLanguageApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxLanguageApiService]
    });
  });

  it('should be created', inject([CnxLanguageApiService], (service: CnxLanguageApiService) => {
    expect(service).toBeTruthy();
  }));
});
