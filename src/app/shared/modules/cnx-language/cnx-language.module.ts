import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CnxLanguageComponent } from './cnx-language/cnx-language.component';
import { SharedModule } from '../../shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CnxLanguageComponent
  ],
  declarations: [CnxLanguageComponent]
})
export class CnxLanguageModule { }
