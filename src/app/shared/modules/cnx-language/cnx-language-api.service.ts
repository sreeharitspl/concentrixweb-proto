import { Injectable } from '@angular/core';
import { HttpService } from '../../service/http/http.service';
import { CnxLanguageService } from '../../service/cnx-language/cnx-language.service';
import { CnxConstants } from '../cnx-utils/cnx-constants/cnx-constants';
import { CnxLocalStorageService } from '../../service/cnx-local-storage/cnx-local-storage.service';
import { CnxGlobalService } from '../cnx-utils/cnx-global/cnx-global.service';

@Injectable({
  providedIn: 'root'
})
export class CnxLanguageApiService {

  constructor(private httpService: HttpService, public langDataService: CnxLanguageService,
    public localStorage: CnxLocalStorageService, private cnxGlobalService: CnxGlobalService) { }

   /*
   * Author: T0423
   * Function: getLanguageDictionary
   * Description:  To fetch the language dictionary
   * Arguments:
   *     Param 1: url       : string : For calling services with url
   *     Param 2: params    : type   : For passing params in api call
   *     Param 3: cbSuccess :        : For Success callback
   *     Param 4: cbError   :        : For Error callback
   *     param 5: showLoader: boolean: For showing spiiner or not
   * Return   : Nil
   */
  public getLanguageDictionary(url, params, cbSuccess, cbError, showLoader?) {
    if (showLoader !== false) {
      this.cnxGlobalService.showLoader();
    }

    this.httpService.postX(url, params)
      .subscribe(result => {
        this.langDataService.languageDictionary = result.ResourceContent;
        this.langDataService.lastUpdatedTime = (new Date()).getTime();
        this.saveLanguageDetails();
        cbSuccess(result);
      },
        error => cbError(error)
      );
  }

  /*
   * Author: T0510
   * Function: saveLanguageDetails
   * Description:  For saving language details on login at local storage
   * Arguments: Nil
   * Return: Nil
   */
  public saveLanguageDetails() {
    const translationKeys = CnxConstants.LOGINTRANSLATIONKEYS;
    const loginTransObject = {};

    Object.keys(translationKeys).forEach(key => {
      loginTransObject[key] = this.langDataService.languageDictionary[key];
    });

    this.localStorage.setItem(CnxConstants.LOCALSTORAGEKEYS.CNX_LOGIN_TRANSLATION, JSON.stringify(loginTransObject));
  }

  /*
   * Author: T0423
   * Function: getAllLanguages
   * Description:  To get all languages
   * Arguments:
   *     Param 1: url       : string : For calling services with url
   *     Param 2: params    : type   : For passing params in api call
   *     Param 3: cbSuccess :        : For Success callback
   *     Param 4: cbError   :        : For Error callback
   *     param 5: showLoader: boolean: For showing spiiner or not
   * Return   : Nil
   */
  public getAllLanguages(url, params, cbSuccess, cbError, showLoader?) {
    if (showLoader !== false) {
      this.cnxGlobalService.showLoader();
    }

    this.httpService.postX(url, params)
      .subscribe(result => {
        this.langDataService.languageList = result.LanguageList;
        cbSuccess(result);
      },
        error => cbError(error)
      );
  }
}
