import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxDashboardmenuitemsComponent } from './cnx-dashboardmenuitems.component';

describe('CnxDashboardmenuitemsComponent', () => {
  let component: CnxDashboardmenuitemsComponent;
  let fixture: ComponentFixture<CnxDashboardmenuitemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxDashboardmenuitemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxDashboardmenuitemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
