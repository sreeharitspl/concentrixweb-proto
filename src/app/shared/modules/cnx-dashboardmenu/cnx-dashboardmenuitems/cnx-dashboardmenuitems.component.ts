import { Component, OnInit, Input } from '@angular/core';
import { CnxLanguageService } from '../../../../shared/service/cnx-language/cnx-language.service';

@Component({
  selector: 'app-cnx-dashboard-menu-items',
  templateUrl: './cnx-dashboardmenuitems.component.html',
  styleUrls: ['./cnx-dashboardmenuitems.component.scss']
})


export class CnxDashboardmenuitemsComponent implements OnInit {

  @Input() option: any;

  constructor(public cnxLanguageService: CnxLanguageService) { }

  ngOnInit() {
  }

}
