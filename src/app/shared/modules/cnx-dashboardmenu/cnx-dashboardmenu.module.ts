import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxDashboardmenuComponent } from './cnx-dashboardmenu/cnx-dashboardmenu.component';
import { CnxDashboardmenuitemsComponent } from './cnx-dashboardmenuitems/cnx-dashboardmenuitems.component';
import { SharedModule } from '../../shared.module';
import { TooltipModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TooltipModule.forRoot()
  ],
  exports: [
    CnxDashboardmenuComponent, CnxDashboardmenuitemsComponent
  ],
  declarations: [CnxDashboardmenuComponent, CnxDashboardmenuitemsComponent]
})
export class CnxDashboardmenuModule { }
