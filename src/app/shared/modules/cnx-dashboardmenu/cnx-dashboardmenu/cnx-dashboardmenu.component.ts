import { Component, OnInit } from '@angular/core';

import { CnxDashBoardMenu } from '../../../../shared/modules/cnx-utils/cnx-constants/cnx-dashboardmenu';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { CnxGlobalService } from '../../cnx-utils/cnx-global/cnx-global.service';

@Component({
  selector: 'app-cnx-dashboardmenu',
  templateUrl: './cnx-dashboardmenu.component.html',
  styleUrls: ['./cnx-dashboardmenu.component.scss']
})
export class CnxDashboardmenuComponent implements OnInit {
  public menuOptions: any[] = [];
  public pageUrl: string;
  public homeOption: any;
  constructor(public globalService: CnxGlobalService) { }

  ngOnInit() {
    this.menuOptions = CnxDashBoardMenu.DASHBOARDMENU;
    this.homeOption = CnxConstants.HOMEMENUITEM;
  }
  /*
   * Author: T0475
   * Function: goToOption
   * Description:  Navigate to clicked page
   */
  goToQuickMenu(menuOption) {
    this.pageUrl = menuOption.Url;
    this.globalService.navigateTo([this.pageUrl]);
  }
  /*
   * Author: T0475
   * Function: goToHome
   * Description:  Navigate to home page
   */
  goToHome() {
    this.globalService.navigateTo([this.homeOption.URL]);
  }
}
