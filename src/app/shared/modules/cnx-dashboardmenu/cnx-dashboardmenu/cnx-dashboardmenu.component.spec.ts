import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxDashboardmenuComponent } from './cnx-dashboardmenu.component';

describe('CnxDashboardmenuComponent', () => {
  let component: CnxDashboardmenuComponent;
  let fixture: ComponentFixture<CnxDashboardmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxDashboardmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxDashboardmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
