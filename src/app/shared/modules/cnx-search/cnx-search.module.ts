import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxSearchComponent } from './cnx-search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [CnxSearchComponent],
  exports: [CnxSearchComponent]
})
export class CnxSearchModule { }
