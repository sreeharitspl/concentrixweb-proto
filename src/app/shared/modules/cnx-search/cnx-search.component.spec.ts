import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxSearchComponent } from './cnx-search.component';

describe('CnxSearchComponent', () => {
  let component: CnxSearchComponent;
  let fixture: ComponentFixture<CnxSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
