import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CnxConstants } from '../cnx-utils/cnx-constants/cnx-constants';

@Component({
  selector: 'app-cnx-search',
  templateUrl: './cnx-search.component.html',
  styleUrls: ['./cnx-search.component.scss']
})
export class CnxSearchComponent implements OnInit {

  searchForm: FormGroup;
  public searchTextMaxLength: number;
  public submitted: boolean;

  @Input() placeholderText: string;
  @Output() emitSearchText: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    this.submitted = false;
    this.searchTextMaxLength = CnxConstants.NUMERICCONSTANTS.CNX_SEARCHFIELD;
    this.searchForm = new FormGroup({
      searchText: new FormControl('', [Validators.required, Validators.maxLength(this.searchTextMaxLength)]
      ),
    });
  }

  /**
  * Author: TSPL-T0475
  * Created Date: 20 Nov 2018
  * Method Name: onSearchSubmit
  * Description: To pass the serach text.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
 onSearchSubmit() {
    this.submitted = true;
    if (this.searchForm.valid) {
      this.emitSearchText.emit(this.searchForm.controls.searchText.value);
    }
  }
}
