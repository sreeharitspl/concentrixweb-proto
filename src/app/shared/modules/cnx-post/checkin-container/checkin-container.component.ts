import { Component, OnInit, Input } from '@angular/core';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { CnxCheckinModel } from '../../../models/cnx-post-models/cnx-checkin-model';

@Component({
  selector: 'app-checkin-container',
  templateUrl: './checkin-container.component.html',
  styleUrls: ['./checkin-container.component.scss']
})
export class CheckinContainerComponent implements OnInit {
@Input() FeedType: { FeedType: string };
@Input() PostFeeds: CnxCheckinModel;
@Input() defaultImage = './assets/images/default_bio.png';
@Input() defaultMap = './assets/images/map-icon.png';

  constructor(public cnxLanguageService: CnxLanguageService) { }
  ngOnInit() {
  }
  getDefaultmap() {
    // console.log(CnxConstants.CNX_DEFAULT_STATICICON_GET['CNX_DEFAULT_MAP']);
    return CnxConstants.CNX_DEFAULT_STATICICON_GET['CNX_DEFAULT_MAP'];
  }

  getCheckedInLocation(feedData) {
    let returnData: string;
    returnData = this.getDefaultmap();
    if (feedData && feedData.LocationImageUrl) {
      returnData = feedData.LocationImageUrl;
    }
    return returnData;
  }
}
