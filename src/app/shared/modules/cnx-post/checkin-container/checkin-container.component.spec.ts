import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckinContainerComponent } from './checkin-container.component';

describe('CheckinContainerComponent', () => {
  let component: CheckinContainerComponent;
  let fixture: ComponentFixture<CheckinContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckinContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
