import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { TooltipModule } from 'ngx-bootstrap';
import { EmbedVideo } from 'ngx-embed-video';

import { CheckinContainerComponent } from './checkin-container/checkin-container.component';
import { ImageContainerComponent } from './image-container/image-container.component';
import { PollsContainerComponent } from './polls-container/polls-container.component';
import { UrlsContainerComponent } from './urls-container/urls-container.component';
import { VideosContainerComponent } from './videos-container/videos-container.component';
import { AlertContainerComponent } from './alert-container/alert-container.component';

import { SharedModule } from '../../shared.module';
import { CnxUserImageModule } from '../cnx-user-image/cnx-user-image.module';
import { CnxSharedUserDetailsModule } from '../cnx-shared-user-details/cnx-shared-user-details.module';
import { TranslatePipe } from '../../pipes/translate/translate.pipe';
import { ExpirydatePipe } from '../../pipes/expirydate/expirydate.pipe';
import { PollContainerDetailesComponent } from './poll-container-detailes/poll-container-detailes.component';
import { SafeurlPipe } from '../../pipes/safeurlpipe/safeurl.pipe';
import { AnniversaryContainerComponent } from './anniversary-container/anniversary-container.component';
import { NewsContainerModule } from './news-container/news-container.module';

import { CnxAnniversaryModule } from './anniversary-container/anniversary-container.module';
import { VideosContainerModule } from './videos-container/videos-container.module';


@NgModule({
  imports: [
    CommonModule,
    NewsContainerModule,
    InfiniteScrollModule,
    SharedModule,
    CnxUserImageModule,
    CnxSharedUserDetailsModule,
    TooltipModule.forRoot(),
    EmbedVideo.forRoot(),
    CnxAnniversaryModule,
    VideosContainerModule
  ],
  declarations: [
    CheckinContainerComponent,
    ImageContainerComponent,
    PollsContainerComponent,
    UrlsContainerComponent,
    // VideosContainerComponent,
    PollContainerDetailesComponent,
    AlertContainerComponent,
  ],
  exports: [
    CheckinContainerComponent,
    ImageContainerComponent,
    PollsContainerComponent,
    UrlsContainerComponent,
    // VideosContainerComponent,
    PollContainerDetailesComponent,
    AlertContainerComponent,
    AnniversaryContainerComponent
  ],
  providers: [
    TranslatePipe,
    ExpirydatePipe,
    SafeurlPipe
  ],
  entryComponents: [
    PollContainerDetailesComponent
  ]
})
export class CnxPostModule { }
