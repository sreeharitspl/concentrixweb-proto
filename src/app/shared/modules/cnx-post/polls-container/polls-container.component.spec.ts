import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollsContainerComponent } from './polls-container.component';

describe('PollsContainerComponent', () => {
  let component: PollsContainerComponent;
  let fixture: ComponentFixture<PollsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
