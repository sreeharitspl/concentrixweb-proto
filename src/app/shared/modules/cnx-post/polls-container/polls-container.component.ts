import { Component, OnInit, Input } from '@angular/core';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { CnxPostServicesService } from '../cnx-post-services.service';
import { ExpirydatePipe } from '../../../pipes/expirydate/expirydate.pipe';
import { TranslatePipe } from '../../../pipes/translate/translate.pipe';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { PollContainerDetailesComponent } from '../poll-container-detailes/poll-container-detailes.component';
import { CnxPollsModel } from '../../../models/cnx-post-models/cnx-polls-model';

@Component({
  selector: 'app-polls-container',
  templateUrl: './polls-container.component.html',
  styleUrls: ['./polls-container.component.scss']
})
export class PollsContainerComponent implements OnInit {
  pollDetailedModelRef: BsModalRef;
@Input() FeedType: { FeedType: string };
@Input() PostFeeds: CnxPollsModel;
  constructor(
    public cnxLanguageService: CnxLanguageService,
    public cnxPostService: CnxPostServicesService,
    public expirydatePipes: ExpirydatePipe,
    public translatePipe: TranslatePipe,
    private pollDetailsModalService: BsModalService
  ) { }

  ngOnInit() {
  }
  /*
  * Author: TSPL-0446
  * Function: getDefaultIcon
  * Description:  Function return static icon based on the key.
  * Arguments: string : Type of icon
  * Return: return coresponding icon url.
  */
  getDefaultIcon(type) {
    return CnxConstants.CNX_DEFAULT_STATICICON_GET[type];
  }
  /*
  * Author: TSPL-0446
  * Function: getExpirayDate
  * Description:  Function return expiray date of the poll.
  * Arguments: date : Expiry date
  * Return: return coresponding expiry date.
  */
  getExpirayDate(expDate) {
    return this.cnxPostService.cnxGetExpiary(expDate);
  }
  /*
  * Author: TSPL-0446
  * Function: getExpiryText
  * Description:  Function return label based on expiry of the poll.
  * Arguments: date : Expiry date
  * Return: return coresponding expiry label.
  */
  getExpiryText(expdate) {
    const expDategap = this.getExpirayDate(expdate);
    return (expDategap === 0 ?
    ( this.translatePipe.transform('VIDEO_EXPIRED_TODAY', this.cnxLanguageService.lastUpdatedTime)) :
      this.expirydatePipes.transform(null, 'VIDEO_EXPIRED_ON', expDategap));
  }
/*
  * Author: TSPL-0446
  * Function: goToPollDetail
  * Description:  Function redirect to the poll details page.
  * Arguments: date : assessment id : number
  * Return: nill
  */
  goToPollDetail(assessmentId) {
    const initialState = {
      assessmentId : this.PostFeeds.AssessmentId
    };
    this.pollDetailedModelRef = this.pollDetailsModalService.show(PollContainerDetailesComponent,
          {
            initialState,
            backdrop: true,
            ignoreBackdropClick: false // When its true - model will not close on outside click
          }
        );
  }
}
