import { Injectable } from '@angular/core';
import { CnxConstants } from '../cnx-utils/cnx-constants/cnx-constants';

@Injectable({
  providedIn: 'root'
})
export class CnxPostServicesService {

  constructor() { }
  public cnxGetSharedType = function(feedType) {
    return CnxConstants.CNX_DEFAULT_SHARED_TYPE_GET[feedType];
  };

  public cnxGetExpiary = function(expDate) {
    if (expDate) {
      const input = expDate;
      const oneHr = 60 * 60 * 1000; // one hr in ms

      const postDate = new Date(input);
      const date = new Date();
      const y = date.getUTCFullYear();
      const mo = date.getUTCMonth() + 1;
      const d = date.getUTCDate();
      let h = date.getUTCHours();
      let m = date.getUTCMinutes();
      const s = date.getUTCSeconds();
      const ampm = h >= 12 ? 'PM' : 'AM';
        h = h % 12;
        h = h ? h : 12; // the hour '0' should be '12'
        const mRef = m < 10 ? '0' + m : m;

        const todayStr = mo + '/' + d + '/' + y + ' ' + h + ':' + mRef + ':' + s + ' ' + ampm;
        const today = new Date(todayStr);

        const diffTime = Math.abs(postDate.getTime() - today.getTime());
        const onedayTime = 24 * oneHr;
        return Math.abs(Math.floor(diffTime / onedayTime));
    } else {
        return '';
    }
  }

}
