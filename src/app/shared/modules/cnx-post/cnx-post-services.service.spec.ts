import { TestBed, inject } from '@angular/core/testing';

import { CnxPostServicesService } from './cnx-post-services.service';

describe('CnxPostServicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxPostServicesService]
    });
  });

  it('should be created', inject([CnxPostServicesService], (service: CnxPostServicesService) => {
    expect(service).toBeTruthy();
  }));
});
