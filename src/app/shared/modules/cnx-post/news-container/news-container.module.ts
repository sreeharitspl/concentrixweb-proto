import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsContainerComponent } from './news-container.component';
import { CnxUserImageModule } from './../../cnx-user-image/cnx-user-image.module';
import { TranslatePipe } from '../../../pipes/translate/translate.pipe';
import { SharedModule } from '../../../shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CnxUserImageModule
  ],
  declarations: [NewsContainerComponent] ,
  exports: [NewsContainerComponent],
  providers: [TranslatePipe]
})
export class NewsContainerModule { }
