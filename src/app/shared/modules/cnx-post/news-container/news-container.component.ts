import { Component, OnInit, Input } from '@angular/core';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { CnxPostServicesService } from '../cnx-post-services.service';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { CnxSharedUserDetailsComponent } from '../../cnx-shared-user-details/cnx-shared-user-details/cnx-shared-user-details.component';
import { CnxLCSUserListModalComponent } from '../../cnx-lcs-detailview/cnx-lcs-user-list-modal/cnx-lcs-user-list-modal.component';
import { ExpirydatePipe } from '../../../pipes/expirydate/expirydate.pipe';
import { TranslatePipe } from '../../../pipes/translate/translate.pipe';
import { CnxLcsUserListModalData } from '../../../models/cnx-lcs-models/cnx-lcs-user-list-modal-data.model';
import { CnxPostLcsService } from '../../cnx-post-lcs/cnx-post-lcs.service';
import { CnxNewsModel } from '../../../models/cnx-post-models/cnx-news-model';

@Component({
  selector: 'app-news-container',
  templateUrl: './news-container.component.html',
  styleUrls: ['./news-container.component.scss']
})
export class NewsContainerComponent implements OnInit {
  @Input() PostFeeds: CnxNewsModel;
  @Input() FeedType: string;

  shareModalRef: BsModalRef;

  defaultNewIcon: string;
  constructor(
    public cnxPostServicesService: CnxPostServicesService,
    public cnxLanguageService: CnxLanguageService,
    private modalService: BsModalService,
    private postLCSService: CnxPostLcsService
  ) { }

  ngOnInit() {
    console.log('type in news container', this.FeedType);
  }

  /*
  * Author: TSPL-0446
  * Function: getDefaultIcon
  * Description:  Function return static icon based on the key.
  * Arguments: string : Type of icon
  * Return: return coresponding icon url.
  */
  getDefaultIcon(type) {
    return CnxConstants.CNX_DEFAULT_STATICICON_GET[type];
  }

  /*
  * Author: TSPL-0446
  * Function: getSharedType
  * Description:  Function using to get text to show in the post grid.
  * Arguments: string : feed type
  * Return: return coresponding string based on the input.
  */
  getSharedType(feedType) {
    return this.cnxPostServicesService.cnxGetSharedType(feedType);
  }

  /*
  * Author: TSPL-0446
  * Function: listSharedUsers
  * Description:  Function using to show shared user list in a model.
  * Arguments: newsId: number, feedtype : string
  * Return: return coresponding string based on the input.
  */
 sharedUserList() {
    let lcsUserListModalData: CnxLcsUserListModalData;
    lcsUserListModalData = {
      ContentId : this.PostFeeds.ContentId, // this.postLCSService.getContentId(this.PostFeeds, this.FeedType),
      MediaType : CnxConstants.MEDIATYPEKEY[this.FeedType],
      ActionType: CnxConstants.LCSACTIONTYPEKEY.Share,
      StatusFor : CnxConstants.LCSSTATUSFORKEY.sharedWithPeople,
      SharedWithPeopleView: false,
      ActionIcon: CnxConstants.LCSACTIONICONKEY.Share
    };
  const hideBack = true; // This status for hiding back button - when clicking more button
  const initialState = { lcsUserListModalData : lcsUserListModalData, hideBack: hideBack };
  this.shareModalRef = this.modalService.show(CnxLCSUserListModalComponent, {
    initialState,
    backdrop: true,
    ignoreBackdropClick: false
  });
}
}
