import { NewsContainerModule } from './news-container.module';

describe('NewsContainerModule', () => {
  let newsContainerModule: NewsContainerModule;

  beforeEach(() => {
    newsContainerModule = new NewsContainerModule();
  });

  it('should create an instance', () => {
    expect(newsContainerModule).toBeTruthy();
  });
});
