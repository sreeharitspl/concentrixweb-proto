import { Component, OnInit, Input } from '@angular/core';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxImageModel } from '../../../models/cnx-post-models/cnx-image-model';
import { CnxPostServicesService } from '../cnx-post-services.service';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';

@Component({
  selector: 'app-image-container',
  templateUrl: './image-container.component.html',
  styleUrls: ['./image-container.component.scss']
})
export class ImageContainerComponent implements OnInit {
  @Input() PostFeeds: CnxImageModel;
  @Input() FeedType: { FeedType: string };
  @Input() defaultImage = './assets/images/default_bio.png';
  constructor(
    public cnxLanguageService: CnxLanguageService,
    public cnxPostServicesService: CnxPostServicesService) { }
  ngOnInit() {
  }

  /*
  * Author: TSPL-0475
  * Function: getDefaultIcon
  * Description:  Function return static icon based on the key.
  * Arguments: string : Type of icon
  * Return: return coresponding icon url.
  */
  getDefaultIcon(type) {
    return CnxConstants.CNX_DEFAULT_STATICICON_GET[type];
  }
  /*
  * Author: TSPL-0475
  * Function: getSharedType
  * Description:  Function using to get text to show in the post grid.
  * Arguments: string : feed type
  * Return: return coresponding string based on the input.
  */
  getSharedType(feedType) {
    return this.cnxPostServicesService.cnxGetSharedType(feedType);
  }
}
