import { Component, OnInit, Input } from '@angular/core';

import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxAlertsModel } from '../../../models/cnx-post-models/cnx-alerts-model';

@Component({
  selector: 'app-alert-container',
  templateUrl: './alert-container.component.html',
  styleUrls: ['./alert-container.component.scss']
})
export class AlertContainerComponent implements OnInit {

  @Input() PostFeeds: CnxAlertsModel;
  @Input() FeedType: { FeedType: string };
  constructor(public cnxLanguageService: CnxLanguageService) { }
  ngOnInit() {
  }
  /*
  * Author: TSPL-0475
  * Function: getDefaultIcon
  * Description:  Function return static icon based on the key.
  * Arguments: string : Type of icon
  * Return: return coresponding icon url.
  */
  getDefaultIcon(type) {
    return CnxConstants.CNX_DEFAULT_STATICICON_GET[type];
  }
}
