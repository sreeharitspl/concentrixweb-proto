import { EmbedVideo } from 'ngx-embed-video';
import { CnxUserImageModule } from './../../cnx-user-image/cnx-user-image.module';
import { SharedModule } from './../../../shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideosContainerComponent } from './videos-container.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CnxUserImageModule,
    EmbedVideo.forRoot(),
  ],
  declarations: [VideosContainerComponent],
  exports: [VideosContainerComponent]
})
export class VideosContainerModule { }
