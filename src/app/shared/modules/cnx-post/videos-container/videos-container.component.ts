import { Component, OnInit, Input } from '@angular/core';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxVideoModel } from '../../../models/cnx-post-models/cnx-video-model';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { CnxPostServicesService } from '../cnx-post-services.service';
import { CnxLcsUserListModalData } from '../../../models/cnx-lcs-models/cnx-lcs-user-list-modal-data.model';
import { CnxLCSUserListModalComponent } from '../../cnx-lcs-detailview/cnx-lcs-user-list-modal/cnx-lcs-user-list-modal.component';

import { EmbedVideoService } from 'ngx-embed-video';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { CnxPostLcsService } from '../../cnx-post-lcs/cnx-post-lcs.service';
@Component({
  selector: 'app-videos-container',
  templateUrl: './videos-container.component.html',
  styleUrls: ['./videos-container.component.scss']
})
export class VideosContainerComponent implements OnInit {
  @Input() PostFeeds: CnxVideoModel;
  @Input() FeedType: string;

  shareModalRef: BsModalRef;

  public iframe_html: any;
  constructor(
    public cnxLanguageService: CnxLanguageService,
    public cnxPostServicesService: CnxPostServicesService,
    private postLCSService: CnxPostLcsService,
    public embedService: EmbedVideoService,
    private modalService: BsModalService
  ) {
  }

  ngOnInit() {
    this.iframe_html = this.embedService.embed(this.PostFeeds.VideoUrl);
  }
  /*
  * Author: TSPL-0475
  * Function: getDefaultIcon
  * Description:  Function return static icon based on the key.
  * Arguments: string : Type of icon
  * Return: return coresponding icon url.
  */
  getDefaultIcon(type) {
    return CnxConstants.CNX_DEFAULT_STATICICON_GET[type];
  }
  /*
  * Author: TSPL-0475
  * Function: getSharedType
  * Description:  Function using to get text to show in the post grid.
  * Arguments: string : feed type
  * Return: return coresponding string based on the input.
  */
  getSharedType(feedType) {
    return this.cnxPostServicesService.cnxGetSharedType(feedType);
  }

  /*
  * Author: TSPL-0475
  * Function: getSharedType
  * Description:  Function using to show open modal to show shared userlist
  * Arguments: string : feed type
  * Return: nil.
  */
  sharedUserList() {
    let lcsUserListModalData: CnxLcsUserListModalData;
    lcsUserListModalData = {
      ContentId: this.postLCSService.getContentId(this.PostFeeds, this.FeedType),
      MediaType: CnxConstants.MEDIATYPEKEY[this.FeedType],
      ActionType: CnxConstants.LCSACTIONTYPEKEY.Share,
      StatusFor: CnxConstants.LCSSTATUSFORKEY.sharedWithPeople,
      SharedWithPeopleView: false,
      ActionIcon: CnxConstants.LCSACTIONICONKEY.Share
    };
    const hideBack = true; // This status for hiding back button - when clicking more button
    const initialState = { lcsUserListModalData: lcsUserListModalData, hideBack: hideBack };
    this.shareModalRef = this.modalService.show(CnxLCSUserListModalComponent, {
      initialState,
      backdrop: true,
      ignoreBackdropClick: false
    });
  }
}
