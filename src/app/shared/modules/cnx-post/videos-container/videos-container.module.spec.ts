import { VideosContainerModule } from './videos-container.module';

describe('VideosContainerModule', () => {
  let videosContainerModule: VideosContainerModule;

  beforeEach(() => {
    videosContainerModule = new VideosContainerModule();
  });

  it('should create an instance', () => {
    expect(videosContainerModule).toBeTruthy();
  });
});
