import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnniversaryContainerComponent } from './anniversary-container.component';

describe('AnniversaryContainerComponent', () => {
  let component: AnniversaryContainerComponent;
  let fixture: ComponentFixture<AnniversaryContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnniversaryContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnniversaryContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
