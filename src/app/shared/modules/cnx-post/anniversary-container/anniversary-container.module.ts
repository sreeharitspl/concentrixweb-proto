import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnniversaryContainerComponent } from './anniversary-container.component';
import {CnxUserImageModule} from '../../cnx-user-image/cnx-user-image.module';

@NgModule({
  imports: [
    CommonModule,
    CnxUserImageModule,
  ],
  declarations: [AnniversaryContainerComponent],
  exports: [AnniversaryContainerComponent]
})
export class CnxAnniversaryModule { }
