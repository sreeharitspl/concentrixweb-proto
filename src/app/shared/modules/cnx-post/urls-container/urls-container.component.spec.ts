import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlsContainerComponent } from './urls-container.component';

describe('UrlsContainerComponent', () => {
  let component: UrlsContainerComponent;
  let fixture: ComponentFixture<UrlsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrlsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
