import { Component, OnInit, Input } from '@angular/core';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxLcsUserListModalData } from '../../../models/cnx-lcs-models/cnx-lcs-user-list-modal-data.model';
import { CnxPostLcsService } from '../../cnx-post-lcs/cnx-post-lcs.service';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { CnxLCSUserListModalComponent } from '../../cnx-lcs-detailview/cnx-lcs-user-list-modal/cnx-lcs-user-list-modal.component';
import { CnxPostServicesService } from '../cnx-post-services.service';
import { CnxUrlModel } from '../../../models/cnx-post-models/cnx-url-model';

@Component({
  selector: 'app-urls-container',
  templateUrl: './urls-container.component.html',
  styleUrls: ['./urls-container.component.scss']
})
export class UrlsContainerComponent implements OnInit {
  @Input() FeedType: string;
  @Input() PostFeeds: CnxUrlModel;
  public defaultImage = './assets/images/default_bio.png';
  shareModalRef: BsModalRef;
  constructor(
    public cnxLanguageService: CnxLanguageService,
    private postLCSService: CnxPostLcsService,
    private modalService: BsModalService,
    public cnxPostServicesService: CnxPostServicesService
    ) { }

  ngOnInit() {
  }
  getSharedType(feedType) {
    return this.cnxPostServicesService.cnxGetSharedType(feedType);
  }

  sharedUserList() {
    let lcsUserListModalData: CnxLcsUserListModalData;
    lcsUserListModalData = {
      ContentId : this.postLCSService.getContentId(this.PostFeeds, this.FeedType),
      MediaType : CnxConstants.MEDIATYPEKEY[this.FeedType],
      ActionType: CnxConstants.LCSACTIONTYPEKEY.Share,
      StatusFor : CnxConstants.LCSSTATUSFORKEY.sharedWithPeople,
      SharedWithPeopleView: false,
      ActionIcon: CnxConstants.LCSACTIONICONKEY.Share
    };
  const hideBack = true; // This status for hiding back button - when clicking more button
  const initialState = { lcsUserListModalData : lcsUserListModalData, hideBack: hideBack };
  this.shareModalRef = this.modalService.show(CnxLCSUserListModalComponent, {
    initialState,
    backdrop: true,
    ignoreBackdropClick: false
  });
}
}
