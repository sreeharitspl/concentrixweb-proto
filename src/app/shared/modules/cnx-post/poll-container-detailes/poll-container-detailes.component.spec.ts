import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollContainerDetailesComponent } from './poll-container-detailes.component';

describe('PollContainerDetailesComponent', () => {
  let component: PollContainerDetailesComponent;
  let fixture: ComponentFixture<PollContainerDetailesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PollContainerDetailesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollContainerDetailesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
