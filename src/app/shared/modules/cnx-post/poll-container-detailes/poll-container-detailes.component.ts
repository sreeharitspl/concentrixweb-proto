import { Component, OnInit } from '@angular/core';
import { CnxFeedService } from '../../../service/cnx-feed/cnx-feed.service';
import { CnxPollsResponse } from '../../../models/cnx-polls-response';

@Component({
  selector: 'app-poll-container-detailes',
  templateUrl: './poll-container-detailes.component.html',
  styleUrls: ['./poll-container-detailes.component.scss']
})
export class PollContainerDetailesComponent implements OnInit {
  assessmentId: number;
  constructor(
    public feedService: CnxFeedService
  ) { }

  ngOnInit() {
    console.log(this.assessmentId);
    this.getAssessmentData();
  }

  getAssessmentData() {
    const inputs = {
      AssessmentId : this.assessmentId
    };
    this.feedService.getDataForFeed('GetAssessmentDetail', inputs, (respData: CnxPollsResponse) => {
      console.log(respData);
    },
    (errData: any) => {
      console.log('Something went wrong');
    });
  }

}
