import { CnxUserDetailsCardModule } from './cnx-user-details-card.module';

describe('CnxUserDetailsCardModule', () => {
  let cnxUserDetailsCardModule: CnxUserDetailsCardModule;

  beforeEach(() => {
    cnxUserDetailsCardModule = new CnxUserDetailsCardModule();
  });

  it('should create an instance', () => {
    expect(cnxUserDetailsCardModule).toBeTruthy();
  });
});
