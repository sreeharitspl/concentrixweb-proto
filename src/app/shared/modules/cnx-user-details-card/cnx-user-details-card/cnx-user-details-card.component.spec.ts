import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxUserDetailsCardComponent } from './cnx-user-details-card.component';

describe('CnxUserDetailsCardComponent', () => {
  let component: CnxUserDetailsCardComponent;
  let fixture: ComponentFixture<CnxUserDetailsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxUserDetailsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxUserDetailsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
