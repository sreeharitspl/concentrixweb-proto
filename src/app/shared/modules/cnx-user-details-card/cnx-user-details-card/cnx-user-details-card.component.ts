/**
 * Author: TSPL-T0445
 * Date Created: 12 Sept 2018
 */
import { Component, OnInit, Input } from '@angular/core';
import { CnxUserLCSData } from '../../../models/cnx-lcs-models/cnx-user-lcs-data.model';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';

@Component({
  selector: 'app-cnx-user-details-card',
  templateUrl: './cnx-user-details-card.component.html',
  styleUrls: ['./cnx-user-details-card.component.scss']
})

/**
 * Author: TSPL-T0445
 * Date Created: 12 Sept 2018
 * Class Name: CnxUserDetailsCardComponent
 * Description: User details card. To be used with user listing in all modules.
 */
export class CnxUserDetailsCardComponent implements OnInit {

  @Input() userDetails: CnxUserLCSData;

  constructor( public cnxLanguageService: CnxLanguageService ) { }

  ngOnInit() {
  }

  /**
  * Author: TSPL-T0445
  * Created Date: 12 Sept 2018
  * Method Name: getUserProfile
  * Description: To redirect to the user profile.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
 getUserProfile() {

 }

}
