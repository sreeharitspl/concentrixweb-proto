/**
 * Author: TSPL-T0445
 * Date Created: 12 Sept 2018
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxUserDetailsCardComponent } from './cnx-user-details-card/cnx-user-details-card.component';
import { CnxUserImageModule } from '../cnx-user-image/cnx-user-image.module';
import { SharedModule } from '../../shared.module';

@NgModule({
  imports: [
    CommonModule,
    CnxUserImageModule,
    SharedModule
  ],
  declarations: [
    CnxUserDetailsCardComponent
  ],
  exports: [
    CnxUserDetailsCardComponent
  ]
})
export class CnxUserDetailsCardModule { }
