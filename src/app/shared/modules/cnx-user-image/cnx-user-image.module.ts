/**
 * Author: TSPL-T0445
 * Created Date: 10 Sept 2018
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxUserImageComponent } from './cnx-user-image/cnx-user-image.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CnxUserImageComponent
  ],
  exports: [
    CnxUserImageComponent
  ]
})

export class CnxUserImageModule { }
