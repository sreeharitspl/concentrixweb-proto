/**
 * Author: TSPL-T0445
 * Date Created: 10 Sept 2018
 */

import { Component, OnInit, Input } from '@angular/core';

import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';

@Component({
  selector: 'app-cnx-user-image',
  templateUrl: './cnx-user-image.component.html',
  styleUrls: ['./cnx-user-image.component.scss'],
})

/**
 * Author: TSPL-T0445
 * Created Date: 10 Sept 2018
 * Class Name: CnxUserImageComponent
 * Description: To manage user image and its actions.
 */
export class CnxUserImageComponent implements OnInit {

  @Input() profileImageUrl: string;
  @Input() containerClass: string;

  constructor() { }

  ngOnInit() {
  }

/**
  * Author: TSPL-T0445
  * Created Date: 10 Sept 2018
  * Method Name: getUserProfile
  * Description: To redirect to the user profile.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
  getUserProfile() {

  }

/**
  * Author: TSPL-T0445
  * Created Date: 10 Sept 2018
  * Method Name: getUserProfile
  * Description: To load default avatar if the user image is not available.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
  getDefaultAvatar() {
    this.profileImageUrl = CnxConstants.DEFAULTAVATARURL;
  }
}
