import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxUserImageComponent } from './cnx-user-image.component';

describe('CnxUserImageComponent', () => {
  let component: CnxUserImageComponent;
  let fixture: ComponentFixture<CnxUserImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxUserImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxUserImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
