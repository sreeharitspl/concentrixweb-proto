import { TestBed, inject } from '@angular/core/testing';

import { CnxTermsofuseandpolicyService } from './cnx-termsofuseandpolicy.service';

describe('CnxTermsofuseandpolicyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxTermsofuseandpolicyService]
    });
  });

  it('should be created', inject([CnxTermsofuseandpolicyService], (service: CnxTermsofuseandpolicyService) => {
    expect(service).toBeTruthy();
  }));
});
