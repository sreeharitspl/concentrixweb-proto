/*
* Author: Tspl-0518
* Created on 07-Sep-2018
*/
import { Component, OnInit, DoCheck, ChangeDetectionStrategy } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap';

import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxTermsofuseandpolicyService } from '../cnx-termsofuseandpolicy.service';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';


@Component({
  selector: 'app-cnx-termsofuseandpolicy',
  templateUrl: './cnx-termsofuseandpolicy.component.html',
  styleUrls: ['./cnx-termsofuseandpolicy.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush

})
export class CnxTermsofuseandpolicyComponent implements OnInit {

  public header: string;
  public contentType: string;
  public link: string;
  public htmlData: any;
  public image: string;
  public showLoader = true;
  public isTermsOfUseLoaded = false;
  public isPrivacyPolicyLoaded = false;

  constructor(public bsModalRef: BsModalRef,
              public cnxLanguageService: CnxLanguageService,
              public termsofuseandprivacyService: CnxTermsofuseandpolicyService) { }

  ngOnInit() {
    if (this.contentType === CnxConstants.POLICYANDTERMSKEYS.MENU_HELP_TERMS_OF_USE) {
      this.bindPopupContent(CnxConstants.POLICYANDTERMSKEYS.MENU_HELP_TERMS_OF_USE,
                            CnxConstants.POLICYANDTERMSKEYS.LGN_PRIVACY_POLICY,
                            'assets/images/terms.png');
      this.getTermsOfUseHtmlData();
      this.isTermsOfUseLoaded = true;
    } else if (this.contentType === CnxConstants.POLICYANDTERMSKEYS.LGN_PRIVACY_POLICY) {
      this.bindPopupContent(CnxConstants.POLICYANDTERMSKEYS.LGN_PRIVACY_POLICY,
                            CnxConstants.POLICYANDTERMSKEYS.MENU_HELP_TERMS_OF_USE,
                            'assets/images/conditions.png');
      this.getPrivacyPolicyHtmlData();
      this.isPrivacyPolicyLoaded = true;
    } else {
      // to do if new content type included
    }
  }

  /*
  * Author: Tspl-0518
  * Function: bindPopupContent
  * Description: bind header and link based on the content type
  * Arguments:
  *  head: string: popup header
  *  link: string: popup navigation link
  *  img: string: popup header image
  * Return: Nil
  */
  public bindPopupContent(head: string, link: string, img: string) {
    this.header = head;
    this.link   = link;
    this.image  = img;
  }

  /*
  * Author: Tspl-0518
  * Function: toggleContent
  * Description: Change the popup content on clicking the link
  * Arguments: Nil
  * Return: Nil
  */
  public toggleContent() {
    if (this.contentType === CnxConstants.POLICYANDTERMSKEYS.MENU_HELP_TERMS_OF_USE) {
      this.bindPopupContent(CnxConstants.POLICYANDTERMSKEYS.LGN_PRIVACY_POLICY,
                            CnxConstants.POLICYANDTERMSKEYS.MENU_HELP_TERMS_OF_USE,
                            'assets/images/conditions.png');
      this.getPrivacyPolicyHtmlData();
    } else if (this.contentType === CnxConstants.POLICYANDTERMSKEYS.LGN_PRIVACY_POLICY) {
      this.bindPopupContent(CnxConstants.POLICYANDTERMSKEYS.MENU_HELP_TERMS_OF_USE,
                            CnxConstants.POLICYANDTERMSKEYS.LGN_PRIVACY_POLICY,
                            'assets/images/terms.png');
      this.getTermsOfUseHtmlData();
    } else {
      // to do if new content type included
    }
  }

  /*
  * Author: Tspl-0518
  * Function: getTermsOfUseHtmlData
  * Description: To get concentrix terms of use as HTMLBodyElement
  * Arguments: Nil
  * Return: Nil
  */
  public getTermsOfUseHtmlData() {
    this.contentType = CnxConstants.POLICYANDTERMSKEYS.MENU_HELP_TERMS_OF_USE;
    this.htmlData = CnxConstants.STATICTEMPLATE.TERMS_OF_USE; // to bind html template, should be removed once api is available

    // this has been commented since the api is not recieved.
    // if (this.isTermsOfUseLoaded) {
    //   this.showLoader = false;
    // }

    // const params = {};
    // this.termsofuseandprivacyService.getprivacypolicy('', params, (respData: any) => {
    //   this.htmlData = respData.Data;
    // },
    //   (errData: any) => {
    //     alert(errData.statusText);
    //   }, this.showLoader);
  }

  /*
  * Author: Tspl-0518
  * Function: getPrivacyPolicyHtmlData
  * Description: To get concentrix privacy policy as HTMLBodyElement
  * Arguments: Nil
  * Return: Nil
  */
  public getPrivacyPolicyHtmlData() {
    this.contentType = CnxConstants.POLICYANDTERMSKEYS.LGN_PRIVACY_POLICY;
    this.htmlData = CnxConstants.STATICTEMPLATE.PRIVACY_POLICY; // to bind html template, should be removed once api is available

    // this has been commented since the api is not avialable.
    // if (this.isPrivacyPolicyLoaded) {
    //   this.showLoader = false;
    // }

    // const params = {};
    // this.termsofuseandprivacyService.getTermsOfUse('', params, (respData: any) => {
    //   this.htmlData = respData.Data;
    // },
    //   (errData: any) => {
    //     alert(errData.statusText);
    //   }, this.showLoader);
  }

}
