import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxTermsofuseandpolicyComponent } from './cnx-termsofuseandpolicy.component';

describe('CnxTermsofuseandpolicyComponent', () => {
  let component: CnxTermsofuseandpolicyComponent;
  let fixture: ComponentFixture<CnxTermsofuseandpolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxTermsofuseandpolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxTermsofuseandpolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
