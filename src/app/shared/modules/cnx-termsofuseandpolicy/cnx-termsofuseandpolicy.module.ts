import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxTermsofuseandpolicyComponent } from './cnx-termsofuseandpolicy/cnx-termsofuseandpolicy.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})

export class CnxTermsofuseandpolicyModule { }
