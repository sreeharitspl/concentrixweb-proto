/*
* Author: Tspl-0518
* Created on 10-Sep-2018
*/
import { Injectable } from '@angular/core';

import { HttpService } from '../../service/http/http.service';
import { CnxGlobalService } from '../cnx-utils/cnx-global/cnx-global.service';

@Injectable({
  providedIn: 'root'
})

export class CnxTermsofuseandpolicyService {

  constructor(public httpService: HttpService, public cnxGlobalService: CnxGlobalService) { }

  /*
  * Author: Tspl-0518
  * Function: getprivacypolicy
  * Description:  This function used to get concentrix privacy policy as HTMLBodyElement
  * Arguments:
  *     Param 1: url        : string
  *     Param 2: params     : object
  *     Param 3: cbSuccess  : success callback
  *     Param 4: cbError    : error callback
  *     param 5: showLoader : Boolean
  * Return:
  *     call the fallback method: function:calls cbSuccess on success and calls cbErrors on error
  */
  public getprivacypolicy(url, params, cbSuccess, cbError, showLoader) {
    if (showLoader !== false) {
      this.cnxGlobalService.showLoader();
    }

    this.httpService.postX(url, params)
      .subscribe(result => {
        cbSuccess(result);
      },
        error => cbError(error)
      );
  }

  /*
  * Author: Tspl-0518
  * Function: getTermsOfUse
  * Description:  This function used to get concentrix terms of use as HTMLBodyElement
  * Arguments:
  *     Param 1: url        : string
  *     Param 2: params     : object
  *     Param 3: cbSuccess  : success callback
  *     Param 4: cbError    : error callback
  *     param 5: showLoader : Boolean
  * Return:
  *     call the fallback method: function:calls cbSuccess on success and calls cbErrors on error
  */
  public getTermsOfUse(url, params, cbSuccess, cbError, showLoader) {
    if (showLoader !== false) {
      this.cnxGlobalService.showLoader();
    }

    this.httpService.postX(url, params)
      .subscribe(result => {
        cbSuccess(result);
      },
        error => cbError(error)
      );
  }

}
