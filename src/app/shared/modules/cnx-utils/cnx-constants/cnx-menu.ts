/*
 * Author: T0423
 * Date: 07/Sept/2018
 * Description: Application menu options
 *
 */
export class CnxMenu {

    /*
     * Author: T0510
     * Function: CNXMENU
     * Description:  Function to define the constants related to app local storage
     * Arguments: Nil
     * Return: Nil
     */
    public static get CNXMENU() {
        return [
          {
            'ItemId': 1,
            'ItemName': 'MENU_PROFILE',
            'State': '',
            'Icon': 'profile.png',
            'activeIcon': 'profile_active.png',
            'isActive': false,
            'Code': 'profile',
            'Submenu': [
              {
                'ItemId': 1.1,
                'ItemName': 'CNX_MNU_MY_PROFILE',
                'Icon': 'media/menu_icons/my_profile.png',
                'State': 'app.myprofile',
                'Code': 'myprofile',
                'URL': 'auth/content/profile/profile',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 1.2,
                'ItemName': 'KUDOS',
                'Icon': 'media/menu_icons/kudos.png',
                'State': 'app.kudostab',
                'Code': 'kudos',
                'URL': 'auth/content/kudos/kudos',
                'Type': '',
                'IconClass': 'thumbs-o-up'
              }
            ],
            'isVisible': true,
            'show': false,
            'IconClass': 'user'
          },
          {
            'ItemId': 2,
            'ItemName': 'MENU_COMMUNICATION',
            'State': '',
            'Icon': 'communication.png',
            'activeIcon': 'communication_active.png',
            'isActive': false,
            'Code': 'communication',
            'Submenu': [
              {
                'ItemId': 2.1,
                'ItemName': 'CNX_HOM_MSG',
                'Icon': 'media/menu_icons/message.png',
                'State': 'app.messageList',
                'Code': 'message',
                'URL': 'auth/content/messages/messages',
                'Type': '',
                'IconClass': 'envelope-o'
              },
              {
                'ItemId': 2.2,
                'ItemName': 'CNX_HOM_NEWS',
                'Icon': 'media/menu_icons/news.png',
                'State': 'app.news',
                'Code': 'news',
                'URL': 'auth/content/news/news',
                'Type': '',
                'IconClass': 'newspaper-o'
              },
              {
                'ItemId': 2.3,
                'ItemName': 'HOME_VIDEOS',
                'Icon': 'media/menu_icons/video.png',
                'State': 'app.video',
                'Code': 'video',
                'URL': 'auth/content/videos/videos',
                'Type': '',
                'IconClass': 'play'
              }
            ],
            'isVisible': true,
            'show': false,
            'IconClass': 'bullhorn'
          },
          {
            'ItemId': 3,
            'ItemName': 'MENU_SELF_SERVICE',
            'State': '',
            'Icon': 'selfService.png',
            'activeIcon': 'selfService.png',
            'isActive': false,
            'Code': 'selfService',
            'Submenu': [
              {
                'ItemId': 3.1,
                'ItemName': 'CNX_HOM_BENIFITS',
                'Icon': 'media/menu_icons/benefits.png',
                'State': 'app.benifits',
                'Code': 'benifits',
                'URL': 'auth/content/benefits/benefits',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 3.2,
                'ItemName': 'HOME_INCIDENT_REPORTING',
                'Icon': 'media/menu_icons/incident.png',
                'State': 'app.incidentReporting',
                'Code': 'incidentReporting',
                'URL': 'auth/content/incidentreporting/incidentreporting',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 3.3,
                'ItemName': 'CNX_HOM_MYTEAM',
                'Icon': 'media/menu_icons/my_team.png',
                'State': 'app.myTeam',
                'Code': 'myteam',
                'URL': 'auth/content/myteam/myteam',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 3.4,
                'ItemName': 'CNX_HOM_STAFF_DIRECT',
                'Icon': 'media/menu_icons/directory.png',
                'State': 'app.staffDirectory',
                'Code': 'staffdirectory',
                'URL': 'auth/content/staffdirectory/staffdirectory',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 3.5,
                'ItemName': 'CNX_HOM_NEW_JOINEE',
                'Icon': 'media/menu_icons/new_joinee.png',
                'State': 'app.staffDirectory',
                'Code': 'newjoinee',
                'URL': 'auth/content/newjoinee/newjoinee',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 3.6,
                'ItemName': 'HOME_USEFUL_APPS',
                'Icon': 'media/menu_icons/app_integration.png',
                'State': 'app.appIntegration',
                'Code': 'benifits',
                'URL': 'auth/content/usefulapps/usefulapps',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 3.7,
                'ItemName': 'TICKET_HOME',
                'Icon': 'media/menu_icons/helpdesk.png',
                'State': 'app.ticket',
                'Code': 'ticket',
                'URL': 'auth/content/helpdesk/helpdesk',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 3.8,
                'ItemName': 'CHECK_IN',
                'Icon': 'media/menu_icons/checkin.png',
                'State': '',
                'Code': 'checkin',
                'URL': 'auth/content/checkin/checkin',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 3.9,
                'ItemName': 'FRIENDS',
                'Icon': 'media/menu_icons/friends.png',
                'State': 'app.friends',
                'Code': 'friends',
                'URL': 'auth/content/friends/friends',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 3.1,
                'ItemName': 'SHARED_CONTENT',
                'Icon': 'media/menu_icons/shareItem.png',
                'State': 'app.sharedtab',
                'Code': 'sharedContent',
                'URL': 'auth/content/sharedcontent/sharedcontent',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 3.11,
                'ItemName': 'POLLS',
                'Icon': 'media/menu_icons/poll.png',
                'State': 'app.quickPollList',
                'Code': 'quickPoll',
                'URL': 'auth/content/polls/polls',
                'Type': '',
                'IconClass': 'user'
              }
            ],
            'isVisible': true,
            'show': false,
            'IconClass': 'hand-pointer-o'
          },
          {
            'ItemId': 4,
            'ItemName': 'MENU_COMPANY_INFO',
            'State': '',
            'Icon': 'company.png',
            'activeIcon': 'company_active.png',
            'isActive': false,
            'Code': 'cmpinfo',
            'Submenu': [
              {
                'ItemId': 4.1,
                'ItemName': 'CNX_HOM_HOLIDAY',
                'Icon': 'media/menu_icons/holiday.png',
                'State': 'app.holidays',
                'Code': 'holidays',
                'URL': 'auth/content/holidays/holidays',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 4.2,
                'ItemName': 'MENU_LOCATIONS',
                'Icon': 'media/menu_icons/location.png',
                'State': 'app.location',
                'Code': 'locations',
                'URL': 'auth/content/locations/locations',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 4.3,
                'ItemName': 'CNX_MNU_CONTACT_US',
                'Icon': 'media/menu_icons/contactus.png',
                'State': 'app.hotlineNumbers',
                'Code': 'contactus',
                'URL': 'auth/content/contactus/contactus',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 4.4,
                'ItemName': 'MENU_ABOUT_CNX',
                'Icon': 'media/menu_icons/leader.png',
                'State': '',
                'Code': 'aboutUs',
                'URL': 'auth/content/aboutus/aboutus',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 4.5,
                'ItemName': 'HOME_CULTURE',
                'Icon': 'media/menu_icons/culture.png',
                'State': 'app.culture',
                'Code': 'culture',
                'URL': 'auth/content/culture/culture',
                'Type': '',
                'IconClass': 'user'
              },
              {
                'ItemId': 4.6,
                'ItemName': 'HOME_VALUES',
                'Icon': 'media/menu_icons/values.png',
                'State': 'app.values',
                'Code': 'value',
                'URL': 'auth/content/operatingphilosophy/operatingphilosophy',
                'Type': '',
                'IconClass': 'user'
              }
            ],
            'isVisible': true,
            'show': false,
            'IconClass': 'building'
          },
          {
            'ItemId': 5,
            'ItemName': 'MENU_HELP',
            'State': 'app.helpmenu',
            'Icon': 'help.png',
            'activeIcon': 'help_active.png',
            'isActive': false,
            'Code': 'helpmenu',
            'Submenu': [
              {
                'ItemId': 5.1,
                'ItemName': 'MENU_HELP_TERMS_OF_USE',
                'Icon': 'media/menu_icons/Terms.png',
                'State': 'app.terms',
                'Code': 'helpmenu_submenu',
                'URL': 'auth/content/termsofuse/termsofuse',
                'Type': '1',
                'IconClass': 'user'
              },
              {
                'ItemId': 5.2,
                'ItemName': 'MENU_HELP_PRIVACY_POLICY',
                'Icon': 'media/menu_icons/Privacy.png',
                'State': 'app.terms',
                'Code': 'helpmenu_submenu',
                'URL': 'auth/content/privacypolicy/privacypolicy',
                'Type': '2',
                'IconClass': 'user'
              },
              {
                'ItemId': 5.3,
                'ItemName': 'MENU_HELP_APP_INFO',
                'Icon': 'media/menu_icons/app_info.png',
                'State': 'app.terms',
                'Code': 'helpmenu_submenu',
                'URL': 'auth/content/appinfo/appinfo',
                'Type': '3',
                'IconClass': 'user'
              },
              {
                'ItemId': 5.4,
                'ItemName': 'MENU_HELP_APP_FEEDBACK',
                'Icon': 'media/menu_icons/feedback.png',
                'State': 'app.feedback',
                'Code': 'helpmenu_submenu',
                'URL': 'auth/content/feedback/feedback',
                'Type': '',
                'IconClass': 'user'
              }
            ],
            'isVisible': true,
            'show': false,
            'IconClass': 'question-circle'
          },
          {
            'ItemId': 6,
            'ItemName': 'CNX_MNU_SOCIAL',
            'State': 'app.social',
            'Icon': 'social.png',
            'activeIcon': 'social_active.png',
            'isActive': false,
            'Code': 'social',
            'Submenu': [],
            'isVisible': true,
            'show': false,
            'IconClass': 'user'
          },
          {
            'ItemId': 7,
            'ItemName': 'HOME_TRAINING',
            'State': '',
            'Icon': 'training.png',
            'activeIcon': 'profile_active.png',
            'isActive': false,
            'Code': 'training',
            'Submenu': [
              {
                'ItemId': 7.1,
                'ItemName': 'ACE_T',
                'Icon': 'media/menu_icons/acet.png',
                'State': 'app.training',
                'Code': 'acet',
                'URL': 'auth/content/training/training',
                'Type': '',
                'IconClass': 'user'
              }
            ],
            'isVisible': true,
            'show': false,
            'IconClass': 'user'
          },
          {
            'ItemId': 8,
            'ItemName': 'SEI',
            'State': 'app.seiFolderList',
            'Icon': 'training.png',
            'activeIcon': 'profile_active.png',
            'isActive': false,
            'Code': 'sei',
            'Submenu': [],
            'isVisible': true,
            'show': false,
            'IconClass': 'user'
          }
        ];
    }
}
