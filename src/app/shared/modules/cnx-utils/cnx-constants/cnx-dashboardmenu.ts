/*
 * Author: T0489
 * Date: 10/Sept/2018
 * Description: Application Dashboard menu options
 *
 */
export class CnxDashBoardMenu {
    /*
 * Author: T0489
 * Date: 10/Sept/2018
 * Description: Function for define the dashboard menus
 *
 */
    public static get DASHBOARDMENU() {
        return [
            {
                'MenuId': 1,
                'Name': 'HOME_VIDEOS',
                'Icon': 'play-circle',
                'Url': 'auth/content/videos/videos'
            },
            {
                'MenuId': 2,
                'Name': 'MESSAGE_MESSAGES',
                'Icon': 'envelope',
                'Url': 'auth/content/messages/messages'
            },
            {
                'MenuId': 3,
                'Name': 'CNX_HOM_STAFF_DIRECT',
                'Icon': 'users',
                'Url': 'auth/content/staffdirectory/staffdirectory'
            },
            {
                'MenuId': 4,
                'Name': 'TICKET_HOME',
                'Icon': 'question-circle',
                'Url': 'auth/content/helpdesk/helpdesk'
            },
            {
                'MenuId': 5,
                'Name': 'KUDOS',
                'Icon': 'thumbs-up',
                'Url': 'auth/content/kudos/kudos'
            },
        ];
    }

}
