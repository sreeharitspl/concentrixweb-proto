/*
 * Author: T0423
 * Date: 06/Sept/2018
 * Description: Application constants used throughout the application
 *
 * */
export class CnxConstants {

    /*
    * Author: Tspl-0518
    * Function: STATICTEMPLATE
    * Description: Html template for privacy policy and terms of use
    * Arguments: Nil
    * Return:
    *  PRIVACY_POLICY: string: concentrix privacy policy
    *  TERMS_OF_USE: string: concentrix terms of use
    */
    public static get STATICTEMPLATE() {
        return {
            PRIVACY_POLICY: `<ol><li>Introduction</li><p>As a globally integrated company, Concentrix's
                            business processes increasingly go beyond the borders of one country.This globalization
                            demands not only the availability of communication and information systems across the
                            Concentrix group of companies, but also the world-wide processing, sharing and use of
                            multiple types of information including information about an individual whose identity
                            is apparent (either directly and indirectly), or can be reasonably be ascertained from
                            the information available or likely to be available (Personal Information).This Policy
                            letter sets forth the general principles which underlie Concentrix specific practices
                            for collecting, using, disclosing, storing, retaining, disposing, accessing, transferring
                            or otherwise processing Personal </p></ol>`,
            TERMS_OF_USE: `<p>These terms of use (<b>User Agreement</b>) is an agreement between you and Concentrix
                            Corporation (hereinafter referred to a  <b>Concentrix</b>). This User Agreement shall be
                            applicable to you once you download/install and access this mobile app (hereinafter called
                            as Concentrix One). The provisions of this User Agreement shall include the services
                            provided by Concentrix through Concentrix One and the time to time update of Concentrix One.</p>`
        };
    }

    /*
     * Author: T0423
     * Function: APPLICATIONCONSTANTS
     * Description:  Function to define the constants related to app
     * Arguments: Nil
     * Return: Nil
     */
    public static get APPLICATIONCONSTANTS() {
        return {
            CNX_APP_NAME: 'C0n%$8@3aN',
            CNX_DEFAULT_LANGUAGE: 'en-Us',
            CNX_MNU_SOCIAL: 'CNX_MNU_SOCIAL',
            CNX_SOCIAL_SUBMENU: 'social_submenu',
            CNX_API_TIME_FORMAT: 'yyyy/MM/dd hh:mm:ss:sss a',
            SEI: 'sei'
        };
    }

    /*
     * Author: T0510
     * Function: LOCALSTORAGEKEYS
     * Description:  Function to define the constants related to app local storage
     * Arguments: Nil
     * Return: Nil
     */
    public static get LOCALSTORAGEKEYS() {
        return {
            CNX_TOKEN: 'cnx-token',
            CNX_SELECTED_LANGUAGE:  'cnx-selected-language',
            CNX_SELECTED_USER_NAME: 'cnx-user-name',
            CNX_SELECTED_PASSWORD:  'cnx-password',
            CNX_LOGIN_TRANSLATION:  'cnx-logintranslation',
            CNX_LOGIN_USER_DETAILS: 'cnx-login-user-details'
        };
    }

    /*
     * Author: T0510
     * Function: LOGINTRANSLATIONKEYS
     * Description:  Function to define constants for login details
     * Arguments: Nil
     * Return: Object containing keys of login page
     */
    public static get LOGINTRANSLATIONKEYS() {
        return {
            PROFILE_lANGUAGE: 'PROFILE_lANGUAGE',
            CNX_LGN_USERNAME_GUEST: 'CNX_LGN_USERNAME_GUEST',
            CNX_LGN_PWD: 'CNX_LGN_PWD',
            CNX_LGN_REMEMBER: 'CNX_LGN_REMEMBER',
            CNX_GST_FORGOT_PWD: 'CNX_GST_FORGOT_PWD',
            LGN_AGREE: 'LGN_AGREE',
            LGN_TERMS_OF_USE: 'LGN_TERMS_OF_USE',
            REG_AND: 'REG_AND',
            LGN_PRIVACY_POLICY: 'LGN_PRIVACY_POLICY',
            CNX_LGN_LOGIN: 'CNX_LGN_LOGIN',
            CNX_COPY_RIGHT: 'CNX_COPY_RIGHT',
        };
    }

    /*
     * Author: TSPL-T0445
     * Function: SHARETITLELANGUAGEKEY
     * Description:  Function to get share modal titles
     * Arguments: Nil
     * Return: Nil
     */
    public static get SHARETITLELANGUAGEKEY() {
      return  {
            Image: 'SHARE_IMAGE',
            Url: 'SHARE_URL',
            URL: 'SHARE_URL',
            Article: 'SHARE_ARTICLE',
            News: 'SHARE_ARTICLE',
            video: 'SHARE_VIDEO',
        };
    }

     /*
     * Author: TSPL-T0445
     * Function: MEDIATYPEKEY
     * Description:  Function to get share modal titles
     * Arguments: Nil
     * Return: Nil
     */
    public static get MEDIATYPEKEY() {
        return  {
              Image: 'Image',
              Url: 'Url',
              URL: 'Url',
              Article: 'Article',
              News: 'Article',
              video: 'video',
          };
      }

    /*
     * Author: TSPL-T0445
     * Function: LCSACTIONTYPEKEY
     * Description: To get action types.
     * Arguments: Nil
     * Return: Nil
     */
    public static get LCSACTIONTYPEKEY() {
        return  {
              Like: 'Like',
              Share: 'Share',
              Comment: 'Comment'
          };
      }

    /*
     * Author: TSPL-T0445
     * Function: LCSACTIONTYPETITLEKEY
     * Description:  Function to get language key for
     *               Liked/Shared user list modal
     * Arguments: Nil
     * Return: Nil
     */
    public static get LCSACTIONTYPETITLEKEY() {
        return  {
              Like: 'LIKED_USERS',
              Share: 'SHARED_USERS'
          };
      }

    /*
     * Author: TSPL-T0445
     * Function: LCSSTATUSFORKEY
     * Description: Status For Keys. Need to check usage.
     * Arguments: Nil
     * Return: Nil
     */
    public static get LCSSTATUSFORKEY() {
      return {
        Like : 'LikedStatus',
        Share : 'SharedStatus',
        sharedWithPeople : 'SharedWithPeopleStatus'
      };
    }

    /*
     * Author: TSPL-T0445
     * Function: LCSACTIONTYPEKEY
     * Description:  Function to get language key for
     *               Liked/Shared user list modal
     * Arguments: Nil
     * Return: Nil
     */
    public static get DEFAULTAVATARURL() {
        return 'assets/images/default_user.svg';
      }


    /*
     * Author: TSPL-T0445
     * Function: NUMERICCONSTANTS
     * Description:  Function to get common numeric constants.
     * Arguments: Nil
     * Return: Nil
     */
    public static get NUMERICCONSTANTS() {
        return  {
            CNX_DESCRIPTIONMAXLENGTH: 300,
            CNX_COMMENTLENGTH: 100,
            CNX_SEARCHFIELD: 100
          };
      }

    /*
     * Author: Tspl-0518
     * Function: POLICYANDTERMSKEYS
     * Description:  Function to define constants for privacy policy and terms of use popup
     * Arguments: Nil
     * Return: Object containing keys of terms of use and privacy policy popup
     */
    public static get POLICYANDTERMSKEYS() {
        return {
            LGN_PRIVACY_POLICY: 'LGN_PRIVACY_POLICY',
            MENU_HELP_TERMS_OF_USE: 'MENU_HELP_TERMS_OF_USE'
        };
    }
    /*
     * Author: TSPL-0446
     * Function: PAGINATIONLENGTH
     * Description:  Function return pagination count.
     * Arguments: Nil
     * Return: Object containing pagination count
     */
    public static get PAGINATIONLENGTH() {
        return {
            CNX_PAGINATION_COUNT: 20,
            CNX_MODEL_PAGINATION_COUNT : 5
        };
    }

    /*
     * Author: TSPL-0446
     * Function: CNX_FEEDTYPEKEY
     * Description:  Function return feed type name.
     * Arguments: Nil
     * Return: return coresponding feed name.
     */
    public static get CNX_FEEDTYPEKEY() {
        return {
            CNC_NEWS    : 'News',
            CNC_POLLS   : 'Polls',
            CNC_Image   : 'Image',
            CNC_CheckIn : 'CheckIn',
            CNC_URL     : 'URL',
            CNC_Video   : 'Video',
            CNC_Alert   : 'Alert',
            CNC_BirthAnniversary: 'BirthAnniversary',
            CNC_WorkAnniversary: 'WorkAnniversary'
        };
    }
    /*
     * Author: TSPL-0446
     * Function: CNX_DEFAULT_STATICICON_GET
     * Description:  Function return static icon based on the key.
     * Arguments: Nil
     * Return: return coresponding icon url.
     */
    public static get CNX_DEFAULT_STATICICON_GET() {
        return {
            CNX_NEW     :  'assets/images/news.png',
            CNX_DEFAULT_MAP : 'assets/images/map-icon.png',
            CNX_POLL : 'assets/images/poll.png',
            CNX_FLAG_WHITE: 'assets/Staff/flag_white.png',
            CNX_ALERT     :  'assets/images/alert.png',
            CNX_IMG :  'assets/images/image.png'
        };
    }

    /*
     * Author: TSPL-0446
     * Function: CNX_DEFAULT_STATICICON_GET
     * Description:  Function return static icon based on the key.
     * Arguments: Nil
     * Return: return coresponding icon url.
     */
    public static get CNX_DEFAULT_SHARED_TYPE_GET() {
        return {
            Article     :  'SHARED_AN_ARTICLE',
            News        :  'SHARED_AN_ARTICLE',
            Video       :  'SHARED_A_VIDEO',
            URL         :  'SHARED_A_URL',
            Image       :  'SHARED_AN_IMAGE',
        };
    }

    /*
     * Author: TSPL-0423
     * Function: CNX_FEATURECODES
     * Description:  Function get the feature codes of application api call.
     * Arguments: Nil
     * Return: return coresponding feature code.
     */
    public static get CNX_FEATURECODES() {
        return {
            CNX_SOCMEDHAN : 'SOCMEDHAN'
        };

    }

/*
     * Author: TSPL-0446
     * Function: CNX_GET_REPLACEMENT_CHAR
     * Description:  Function get the feature codes of application api call.
     * Arguments: Nil
     * Return: return coresponding feature code.
     */
    public static get CNX_GET_REPLACEMENT_CHAR() {
        return {
            CNX_REPLACE_D : '%d'
        };

    }

    /*
     * Author: TSPL-T0445
     * Function: MODALSCROLLPARAMETERS
     * Description:  Function to get default parameters for modal scroll.
     * Arguments: Nil
     * Return: Nil
     */
    public static get MODALSCROLLPARAMETERS() {
        return  {
            Throttle : 50,
            ScrollDistance : 2,
            ScrollUpDistance : 1.5
          };
      }
      /*
     * Author: TSPL-T0423
     * Function: ERROR_MESSAGES
     * Description:  Function to get translation keys for erro message
     * Arguments: Nil
     * Return: Nil
     */
    public static get ERROR_MESSAGES() {
        return  {
            CNX_SESSION_EXPIRED: 'SESSION_EXPIRED',
            CNX_RESPONSE_NULL: 'SOMETHING_WENT_WRONG',
            CNX_CONNECTION_TIMEOUT: 'CONNECTION_TIMEOUT',
            CNX_TRY_AFTER_SOME_TIME: 'TRY_AFTER_SOME_TIME'
        };
    }

    /*
     * Author: TSPL-T0445
     * Function: LCSACTIONICONKEY
     * Description: To get action icons.
     * Arguments: Nil
     * Return: Nil
     */
    public static get LCSACTIONICONKEY() {
        return  {
              Like: 'heart',
              Share: 'share',
              Comment: 'commenting'
          };
    }

    /*
     * Author: TSPL-T0475
     * Function: HOMEMENUITEM
     * Description: To get which timeline url.
     * Arguments: Nil
     * Return: Nil
     */
    public static get HOMEMENUITEM() {
        return  {
            URL: 'auth/content/feeds/timeline'
          };
    }

    /*
     * Author: TSPL-T0475
     * Function: SEIPAGEURL
     * Description: To get sei page url.
     * Arguments: Nil
     * Return: Nil
     */
    public static get SEIPAGEURL() {
        return  {
            URL: 'auth/content/sei/sei'
          };
    }
    /*
     * Author: TSPL-T0475
     * Function: SEIPAGEURL
     * Description: To get sei page url.
     * Arguments: Nil
     * Return: Nil
     */
    public static get PLACEHOLDER() {
        return  {
            TIMELINESEARCH: 'Search videos, news, messages...etc',
            NEWJOINEE: 'Search any employee by first or last name',
            STAFFDIRECTORY: 'Search any employee by first or last name'
          };
    }
}
