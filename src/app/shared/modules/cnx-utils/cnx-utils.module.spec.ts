import { CnxUtilsModule } from './cnx-utils.module';

describe('CnxUtilsModule', () => {
  let cnxUtilsModule: CnxUtilsModule;

  beforeEach(() => {
    cnxUtilsModule = new CnxUtilsModule();
  });

  it('should create an instance', () => {
    expect(cnxUtilsModule).toBeTruthy();
  });
});
