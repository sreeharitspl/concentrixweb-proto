/*
 * Author: T0423
 * Date: 05/Sept/2018
 * Description: Global methods to be used throughout the projects will be defined here
 * 
 * */
import { Injectable } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CnxGlobalService {

  public userDetails: any = {};

  constructor(private router: Router) { }

  /*
   * Author: TSPL-T0423
   * Function: showLoader
   * Description:  Function to show thw spinner
   * Arguments: Nil
   * Return: Nil
  */
 public showLoader() {
    document.getElementById('nb-global-spinner').style.display = 'block';
  }

  /*
   * Author: TSPL-T0423
   * Function: hideLoader
   * Description:  Function to hide the spinner
   * Arguments: Nil
   * Return: Nil
  */
  public hideLoader() {
    document.getElementById('nb-global-spinner').style.display = 'none';
  }

  /*
   * Author: TSPL-T0423
   * Function: navigateTo
   * Description:  Function to navigate to a page
   * Arguments: url : Array : url array to be navigated
   *            params : Array : Optional routing params
   * Return: Nil
  */
  public navigateTo(url: Array<string>, params?) {
    this.router.navigate(url);
  }
}
