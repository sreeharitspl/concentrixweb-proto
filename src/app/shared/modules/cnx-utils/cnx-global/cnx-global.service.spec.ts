import { TestBed, inject } from '@angular/core/testing';

import { CnxGlobalService } from './cnx-global.service';

describe('CnxGlobalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxGlobalService]
    });
  });

  it('should be created', inject([CnxGlobalService], (service: CnxGlobalService) => {
    expect(service).toBeTruthy();
  }));
});
