/*
 * Author: T0510
 * Date: 10/Sept/2018
 * Description: To get login data-keys
 *
 */
export class CnxLogin {

    /*
     * Author: T0510
     * Function: LOGINDATAKEYS
     * Description:  Function to define the initial keys for the english language
     * Arguments: Nil
     * Return: Nil
     */
    public static get LOGINDATAKEYS() {
        return {
            PROFILE_lANGUAGE: 'Language',
            CNX_LGN_USERNAME_GUEST: 'Registered Email Id',
            CNX_LGN_PWD: 'Password',
            CNX_LGN_REMEMBER: 'Remember me',
            CNX_GST_FORGOT_PWD: 'Forgot password?',
            LGN_AGREE: 'I Agree',
            LGN_TERMS_OF_USE: 'Concentrix Terms of Use',
            REG_AND: 'and',
            LGN_PRIVACY_POLICY: 'Privacy Policy',
            CNX_LGN_LOGIN: 'Login',
            CNX_COPY_RIGHT: '2018 Concentrix Corporation. All Rights Reserved.'
        };
    }
}
