import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxPostLcsComponent } from './cnx-post-lcs.component';

describe('CnxPostLcsComponent', () => {
  let component: CnxPostLcsComponent;
  let fixture: ComponentFixture<CnxPostLcsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxPostLcsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxPostLcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
