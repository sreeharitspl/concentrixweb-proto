import { Component, OnInit, Input } from '@angular/core';
import { FeedDetails } from '../../interfaces/feedDetails';

@Component({
  selector: 'app-cnx-post-lcs',
  templateUrl: './cnx-post-lcs.component.html',
  styleUrls: ['./cnx-post-lcs.component.scss']
})
export class CnxPostLcsComponent implements OnInit {

  @Input() feedDetails: FeedDetails;
  @Input() feedType: string;
  constructor() { }

  ngOnInit() {
  }

}
