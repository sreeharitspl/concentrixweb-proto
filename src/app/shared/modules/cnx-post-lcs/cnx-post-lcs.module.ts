import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ModalModule, TooltipModule } from 'ngx-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// import { CnxPostShareModalComponent } from './cnx-post-share-modal/cnx-post-share-modal.component';
// import { CnxPostShareComponent } from './cnx-post-share/cnx-post-share.component';

import { SharedModule } from '../../shared.module';
// import { CnxLikeComponent } from './cnx-like/cnx-like.component';
// import { CnxCommentComponent } from './cnx-comment/cnx-comment.component'; 
// import { CnxCommentModalComponent } from './cnx-comment-modal/cnx-comment-modal.component';
// import { CnxCommentListComponent } from './cnx-comment-list/cnx-comment-list.component';

// import { CnxLCSUserListModalComponent } from './cnx-lcs-user-list-modal/cnx-lcs-user-list-modal.component';
import { CnxSharedUserDetailsModule } from '../cnx-shared-user-details/cnx-shared-user-details.module';
import { CnxUserDetailsCardModule } from '../cnx-user-details-card/cnx-user-details-card.module';
import { CnxUserImageModule } from '../cnx-user-image/cnx-user-image.module';

import { CnxLcsModalModule } from '../cnx-lcs-modal/cnx-lcs-modal.module';
import { CnxLcsModalComponent } from '../cnx-lcs-modal/cnx-lcs-modal.component';
import { CnxPostLcsComponent } from './cnx-post-lcs.component';
import { CnxPostShareModule } from './cnx-post-share/cnx-post-share.module';
import { CnxLikeModule } from './cnx-like/cnx-like.module';
import { CnxCommentModule } from './cnx-comment/cnx-comment.module';
import { PipeModule } from '../../pipes/pipe/pipe.module';
import { CnxPostShareModalModule } from '../cnx-lcs-detailview/cnx-post-share-modal/cnx-post-share-modal.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    // ModalModule.forRoot(),
    TooltipModule.forRoot(),
    SharedModule,
    PipeModule,
    CnxSharedUserDetailsModule,
    InfiniteScrollModule,
    CnxUserDetailsCardModule,
    CnxUserImageModule,
    CnxLcsModalModule,
    CnxCommentModule,
    CnxLikeModule,
    CnxPostShareModule
  ],
  declarations: [
    CnxPostLcsComponent
  ],
  exports: [
    CnxPostLcsComponent
  ],
  entryComponents: [
    CnxLcsModalComponent,
    CnxPostLcsComponent
  ]
})
export class CnxPostLCSModule { }
