/**
 * Author: TSPL-T0445
 * Created Date: 06 Sept 2018
 */
import { Injectable, Input } from '@angular/core';

import { ShareRequestDetails } from '../../models/cnx-lcs-models/cnx-share-request.model';
import { HttpService } from '../../service/http/http.service';
import { CnxLCSUserListResponse } from '../../models/cnx-lcs-models/cnx-lcs-user-list-response.model';
import { CnxConstants } from '../cnx-utils/cnx-constants/cnx-constants';
import { CnxLcsUserListRequest } from '../../models/cnx-lcs-models/cnx-lcs-user-list-request.model';

@Injectable({
  providedIn: 'root'
})

/**
 * Author: TSPL-T0445
 * Created Date: 06 Sept 2018
 * Class Name: CnxPostLcsService
 * Description: Service to manage Likes, comments and shares.
 */
export class CnxPostLcsService {

  shareDetails: ShareRequestDetails;

  constructor(private httpService: HttpService) { }

 /**
  * Author: TSPL-T0445
  * Created Date: 06 Sept 2018
  * Method Name: selectUsersToShare
  * Description: To open the user selection view to select the users.
  * Arguments:
  *     shareDetails: Share : the details to be shared.
  * Return:
  *     void
  */
  selectUsersToShare (shareDetails: ShareRequestDetails) {
    this.shareDetails = shareDetails;
  }

  /**
   * Author: TSPL-T0445
   * Created Date: 06 Sept 2018
   * Method Name: viewSelectedUsers
   * Description: To view the selected users.
   * Arguments:
   *     No arguments
   * Return:
   *     void
   */
  viewSelectedUsers () {
  }

 /**
  * Author: TSPL-T0445
  * Created Date: 06 Sept 2018
  * Method Name: sharePost
  * Description: To share the post, after selecting the users.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
  sharePost() {
  }


 /**
  * Author: TSPL-T0445
  * Created Date: 11 Sept 2018
  * Method Name: getLCSUserList
  * Description: To read the list of shared/liked users.
  * Arguments:
  *     input: details of the feed and action type.
  * Return:
  *     void
  */
 getLCSUserList(inputDetails: CnxLcsUserListRequest, cbSuccess, cbError) {

    let sharedUserResponse: CnxLCSUserListResponse;

    this.httpService.postX('GetContentLikedSharedStatus', inputDetails)
      .subscribe(result => {
        sharedUserResponse = new CnxLCSUserListResponse().deserialize(result);
        cbSuccess(sharedUserResponse);
      },
      error => {
        cbError(error);
      }
      );
  }

 /**
  * Author: TSPL-T0475
  * Created Date: 06-Sept-2018
  * Method Name: getCommentList
  * Description: To get comment list of corresponding post.
  * Arguments:
  *     Param 1: url        : string
  *     Param 2: params     : object
  *     Param 3: cbSuccess  : success callback
  *     Param 4: cbError    : error callback
  *     param 5: showLoader : Boolean
  * Return:
  *     call the fallback method: function:calls cbSuccess on success and calls cbErrors on error
  */
  public getCommentList(url, params, cbSuccess, cbError, showLoader?) {
    if (showLoader !== false) {

    }
    this.httpService.postX(url, params)
      .subscribe(result => {
        cbSuccess(result);
      },
      error => cbError(error)
    );
  }

  /**
  * Author: TSPL-T0475
  * Created Date: 07-Sept-2018
  * Method Name: saveComment
  * Description: To save newly added comment of corresponding post.
  * Arguments:
  *     Param 1: url        : string
  *     Param 2: params     : object
  *     Param 3: cbSuccess  : success callback
  *     Param 4: cbError    : error callback
  *     param 5: showLoader : Boolean
  * Return:
  *     call the fallback method: function:calls cbSuccess on success and calls cbErrors on error
  */
  public saveComment(url, params, cbSuccess, cbError, showLoader?) {
    if (showLoader !== false) {

    }
    this.httpService.postX(url, params)
      .subscribe(result => {
        cbSuccess(result);
      },
      error => cbError(error)
    );
  }
/**
  * Author: T0446
  * Method Name: getContentId
  * Description: This function is using to get the content id based on feed type.
  * Arguments:
  * Param 1: feedDetails: objet
  * Param 2: feedType   : string
  * Return: Content id  : number
  */
 public getContentId(feedDetails, feedType) {
   let returnData: number;
   returnData = 0;
   if (feedType && feedDetails) {
      const keyName = this.getKeyFromFeedDetails(feedDetails, feedType);
      returnData = keyName;
   }
   return returnData;
 }
/**
  * Author: T0446
  * Method Name: getKeyFromFeedDetails
  * Description: This function will return the key name from the feed details object.
  * Arguments:
  * Param 1: feedDetails: objet
  * Param 2: feedType   : string
  * Return: Key Name    : strin
  */
 private getKeyFromFeedDetails(feedDetails, feedType) {
    const keyNameArray = this.keyDefinition();
    let returndata: number;
    returndata = 0;
    if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_NEWS) {
      returndata = feedDetails[keyNameArray[feedType].keyName];
    } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_POLLS) {
      returndata = feedDetails[keyNameArray[feedType].keyName];
    } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_URL) {
      returndata = feedDetails[keyNameArray[feedType].keyName];
    }
    /* else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_Image) {
      returndata = feedDetails[keyNameArray[feedType].keyName];
    } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_CheckIn) {
      returndata = feedDetails[keyNameArray[feedType].keyName];
    } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_URL) {
      returndata = feedDetails[keyNameArray[feedType].keyName];
    } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_Video) {
      returndata = feedDetails[keyNameArray[feedType].keyName];
    } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_POLLS) {
      returndata = feedDetails[keyNameArray[feedType].keyName];
    } */
    return returndata;
 }
/**
  * Author: T0446
  * Method Name: keyDefinition
  * Description: This function will define the key structure.
  * Arguments:Nill
  */
  private keyDefinition() {
    return {
      'News': {
        'keyName': 'NewsId'
      }, 'Polls': {
        'keyName': 'AssessmentId'
      }, 'Image': {
        'keyName': 'ContentId'
      }, 'URL': {
        'keyName': 'ContentId'
      }, 'Url': {
        'keyName': 'ContentId'
      }  // Need to add other feed sturucture latter, now the visibility of the structure is very less
    };
  }
}
