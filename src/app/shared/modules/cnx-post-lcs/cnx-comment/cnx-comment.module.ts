import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxCommentComponent } from './cnx-comment.component';
import { PipeModule } from '../../../pipes/pipe/pipe.module';
import { TooltipModule } from 'ngx-bootstrap';
import { CnxLcsModalModule } from '../../cnx-lcs-modal/cnx-lcs-modal.module';

@NgModule({
  imports: [
    CommonModule,
    PipeModule,
    TooltipModule.forRoot(),
    CnxLcsModalModule
  ],
  declarations: [
    CnxCommentComponent
  ],
  exports: [
    CnxCommentComponent
  ]
})
export class CnxCommentModule { }
