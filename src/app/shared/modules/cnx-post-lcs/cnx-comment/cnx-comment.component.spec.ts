import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxCommentComponent } from './cnx-comment.component';

describe('CnxCommentComponent', () => {
  let component: CnxCommentComponent;
  let fixture: ComponentFixture<CnxCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
