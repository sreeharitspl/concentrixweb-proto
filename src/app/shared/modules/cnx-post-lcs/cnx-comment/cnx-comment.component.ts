 /*
  * Author: TSPL T0475
  * Created on: 05-Sep-2018
  * Description:
    This component to display comment icon,comment count and its click function
  */
import { Component, OnInit , Input} from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

// import { CnxCommentModalComponent } from '../cnx-comment-modal/cnx-comment-modal.component';

import { FeedDetails } from './../../../interfaces/feedDetails';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants'; // Constant class
import { CnxPostLcsService } from '../cnx-post-lcs.service';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxLcsModalComponent } from '../../cnx-lcs-modal/cnx-lcs-modal.component';

@Component({
  selector: 'app-cnx-comment',
  templateUrl: './cnx-comment.component.html',
  styleUrls: ['./cnx-comment.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class CnxCommentComponent implements OnInit {

  commentModalRef: BsModalRef;

  /** get FeedDetails as parameters which is used to get comment list */
  @Input() feedDetails: FeedDetails;
  @Input() feedType: string;
   public actionType: string;
  constructor(
    private commentModalService: BsModalService,
    private postLCSService: CnxPostLcsService,
    public cnxLanguageService: CnxLanguageService
  ) { }
  ngOnInit() {
    // console.log('in comment', this.feedType);
    this.actionType = 'Comment';
  }

  /*
  * Author: TSPL T0475
  * Created on :05-Sep-2018
  * Function: openCommentModal
  * Description:function to open comment modal
  * Parameters : data as intialState, component and configuration parameters
  * Return:nil
  */
  openCommentModal() {
    /** commented due to functionality- on hold */
    const initialState = {
          feedDetails : this.feedDetails,
          contentId : this.feedDetails.ContentId,
          feedType : CnxConstants.MEDIATYPEKEY[this.feedType],
          postType : this.feedType,
          actionType : CnxConstants.LCSACTIONTYPEKEY[this.actionType]
    };
    this.commentModalRef = this.commentModalService.show(CnxLcsModalComponent,
          {
            initialState,
            backdrop: true,
            ignoreBackdropClick: false,
            class: 'modal-xl'
          }
      );
  }

}
