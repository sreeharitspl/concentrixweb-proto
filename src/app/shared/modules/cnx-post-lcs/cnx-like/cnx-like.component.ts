import { Component, OnInit, Input } from '@angular/core';

import { FeedDetails } from '../../../interfaces/feedDetails';

import { CnxFeedService } from '../../../service/cnx-feed/cnx-feed.service';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { CnxPostLcsService } from '../cnx-post-lcs.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { CnxLcsUserListModalData } from '../../../models/cnx-lcs-models/cnx-lcs-user-list-modal-data.model';
// import { CnxLCSUserListModalComponent } from '../cnx-lcs-user-list-modal/cnx-lcs-user-list-modal.component';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxLCSUserListModalComponent } from '../../cnx-lcs-detailview/cnx-lcs-user-list-modal/cnx-lcs-user-list-modal.component';
import { CnxLcsModalComponent } from '../../cnx-lcs-modal/cnx-lcs-modal.component';

@Component({
  selector: 'app-cnx-like',
  templateUrl: './cnx-like.component.html',
  styleUrls: ['./cnx-like.component.scss'],
})
export class CnxLikeComponent implements OnInit {

  @Input() feedDetails: FeedDetails;
  @Input() feedType: string;
  likedUsersModalRef: BsModalRef;

  inputs: {
    LikeStatus: boolean;
    ContentId: number;
    ArticleVideoType: string;
  };
  public actionType: string;
  constructor(
    public feedService: CnxFeedService,
    private postLCSService: CnxPostLcsService,
    private modalService: BsModalService,
    public cnxLanguageService: CnxLanguageService
  ) { }

  ngOnInit() {
    this.actionType = 'Like';
  }
    /*
    * Author: T0393
    * Function: onfaClick
    * Description:  click to hightlight the fav color
    * Arguments: Nil
    * Return:Nil
    */
   onfaClick() {
    this.feedDetails.LikedStatus = !this.feedDetails.LikedStatus;
    this.inputs = {
      LikeStatus: this.feedDetails.LikedStatus,
      ContentId: this.feedDetails.ContentId,
      ArticleVideoType: CnxConstants.MEDIATYPEKEY[this.feedType]
    };
    /* Initialy count will work based on client side logic */
          if (this.feedDetails.LikedStatus) {
            this.feedDetails.LikesCount += 1;
          } else {
            this.feedDetails.LikesCount -= 1;
          }
    /* Ends */
    this.feedService.updateLikeStatus('UpdateArticleVideoLikeStatus', this.inputs, (respData: any) => {
          if (respData.Acknowledge === 1) {
            this.feedDetails.LikesCount = respData.LikesCount;
          } else {
            if (this.feedDetails.LikesCount >= 0) {
              this.feedDetails.LikesCount -= 1;
            }
            alert(respData.Message);
          }
    },
    (errData: any) => {
          if (this.feedDetails.LikedStatus) {
            if (this.feedDetails.LikesCount >= 0) {
              this.feedDetails.LikesCount -= 1;
            }
          } else {
            this.feedDetails.LikesCount += 1;
          }
        console.log('Something went wrong');
    });
  }
  /**
  * Author: TSPL-T0445
  * Created Date: 17 Sept 2018
  * Method Name: listLikedUsers
  * Description: To open the popup for listing the liked users.
  * Arguments:
  *     No arguments
  * Return:
  *     void
   */
  listLikedUsers() {
    let lcsUserListModalData: CnxLcsUserListModalData;
    /** commented due to functionality- on hold */
    if (this.feedDetails.LikesCount > 0) {
    lcsUserListModalData = {
      ContentId : this.postLCSService.getContentId(this.feedDetails, this.feedType),
      MediaType : CnxConstants.MEDIATYPEKEY[this.feedType],
      ActionType: CnxConstants.LCSACTIONTYPEKEY.Like,
      StatusFor : CnxConstants.LCSSTATUSFORKEY.Like,
      SharedWithPeopleView: false,
      ActionIcon: CnxConstants.LCSACTIONICONKEY.Like
    };
      const initialState = {
        lcsUserListModalData : lcsUserListModalData,
        feedDetails : this.feedDetails,
        contentId : this.feedDetails.ContentId,
        feedType : CnxConstants.MEDIATYPEKEY[this.feedType],
        postType : this.feedType,
        actionType : CnxConstants.LCSACTIONTYPEKEY[this.actionType]
      };

      this.likedUsersModalRef = this.modalService.show(CnxLcsModalComponent,
        {
          initialState,
          backdrop: true,
          ignoreBackdropClick: true,
          class: 'modal-xl'
      });
    }
  }

}
