import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxLikeComponent } from './cnx-like.component';
import { PipeModule } from '../../../pipes/pipe/pipe.module';
import { TooltipModule } from 'ngx-bootstrap';
import { CnxLcsModalModule } from '../../cnx-lcs-modal/cnx-lcs-modal.module';

@NgModule({
  imports: [
    CommonModule,
    PipeModule,
    TooltipModule.forRoot(),
    CnxLcsModalModule
  ],
  declarations: [CnxLikeComponent],
  exports: [CnxLikeComponent]
})
export class CnxLikeModule { }
