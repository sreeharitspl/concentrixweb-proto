import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxLikeComponent } from './cnx-like.component';

describe('CnxLikeComponent', () => {
  let component: CnxLikeComponent;
  let fixture: ComponentFixture<CnxLikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxLikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxLikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
