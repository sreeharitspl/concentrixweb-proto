import { TestBed, inject } from '@angular/core/testing';

import { CnxPostLcsService } from './cnx-post-lcs.service';

describe('CnxPostLcsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxPostLcsService]
    });
  });

  it('should be created', inject([CnxPostLcsService], (service: CnxPostLcsService) => {
    expect(service).toBeTruthy();
  }));
});
