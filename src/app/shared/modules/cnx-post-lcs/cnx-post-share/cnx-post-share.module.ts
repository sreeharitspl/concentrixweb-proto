import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxPostShareComponent } from './cnx-post-share.component';
import { PipeModule } from '../../../pipes/pipe/pipe.module';
import { TooltipModule } from 'ngx-bootstrap';
import { CnxLcsDetailviewModule } from '../../cnx-lcs-detailview/cnx-lcs-detailview.module';
import { CnxLcsModalModule } from '../../cnx-lcs-modal/cnx-lcs-modal.module';
import { CnxPostShareModalModule } from '../../cnx-lcs-detailview/cnx-post-share-modal/cnx-post-share-modal.module';

@NgModule({
  imports: [
    CommonModule,
    PipeModule,
    TooltipModule.forRoot(),
    CnxLcsDetailviewModule,
    CnxLcsModalModule,
    CnxPostShareModalModule
  ],
  declarations: [
    CnxPostShareComponent
  ],
  exports: [
    CnxPostShareComponent
  ]
})
export class CnxPostShareModule { }
