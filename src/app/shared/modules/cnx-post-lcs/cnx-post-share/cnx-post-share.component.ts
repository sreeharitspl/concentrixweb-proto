/**
 * Author: TSPL-T0445
 * Created Date: 04 Sept 2018
 */
import { Component, OnInit, ChangeDetectionStrategy, Input  } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { CnxPostShareModalComponent } from '../../cnx-lcs-detailview/cnx-post-share-modal/cnx-post-share-modal.component';
import { FeedDetails } from '../../../interfaces/feedDetails';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { CnxLcsUserListModalData } from '../../../models/cnx-lcs-models/cnx-lcs-user-list-modal-data.model';
import { CnxPostLcsService } from '../cnx-post-lcs.service';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';
import { CnxLcsModalComponent } from '../../cnx-lcs-modal/cnx-lcs-modal.component';

@Component({
  selector: 'app-cnx-share',
  templateUrl: './cnx-post-share.component.html',
  styleUrls: ['./cnx-post-share.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

/**
 * Author: TSPL-T0445
 * Created Date: 04 Sept 2018
 * Class Name: CnxPostShareComponent
 * Description: To manage actions performed on sharing feeds.
 */
export class CnxPostShareComponent implements OnInit {

  @Input() feedType: string;

  shareModalRef: BsModalRef;

  @Input() feedDetails: FeedDetails;
  public actionType: string;


  constructor(
    private modalService: BsModalService,
    private postLCSService: CnxPostLcsService,
    public cnxLanguageService: CnxLanguageService
    ) { }

  ngOnInit() {
    this.actionType = 'Share';
  }

 /**
  * Author: TSPL-T0445
  * Created Date: 04 Sept 2018
  * Method Name: shareContent
  * Description: To open the popup for sharing the feed.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
  shareContent() {
     /** commented due to functionality- on hold */
    this.feedDetails.FeedType = this.feedType;
    // this.feedDetails.ContentId = this.postLCSService.getContentId(this.feedDetails, this.feedType);
    const initialState = { feedDetails: this.feedDetails };
    this.shareModalRef = this.modalService.show(CnxPostShareModalComponent, { initialState, backdrop: true,
      ignoreBackdropClick: true
    });

  }

 /**
  * Author: TSPL-T0445
  * Created Date: 04 Sept 2018
  * Method Name: listSharedUsers
  * Description: To open the popup for listing the shared users.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
  listSharedUsers() {
  let lcsUserListModalData: CnxLcsUserListModalData;
  lcsUserListModalData = {
    ContentId : this.postLCSService.getContentId(this.feedDetails, this.feedType),
    MediaType : CnxConstants.MEDIATYPEKEY[this.feedType],
    ActionType: CnxConstants.LCSACTIONTYPEKEY.Share,
    StatusFor : CnxConstants.LCSSTATUSFORKEY.Share,
    SharedWithPeopleView: true,
    ActionIcon: CnxConstants.LCSACTIONICONKEY.Share
  };
    const initialState = {
      lcsUserListModalData : lcsUserListModalData,
      feedDetails : this.feedDetails,
      contentId : this.feedDetails.ContentId,
      feedType : CnxConstants.MEDIATYPEKEY[this.feedType],
      postType : this.feedType,
      actionType : CnxConstants.LCSACTIONTYPEKEY[this.actionType]
    };

    this.shareModalRef = this.modalService.show(CnxLcsModalComponent,
      {
        initialState,
        backdrop: true,
        ignoreBackdropClick: true,
        class: 'modal-xl'
    });
  }
}
