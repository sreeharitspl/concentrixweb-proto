import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxPostShareComponent } from './cnx-post-share.component';

describe('CnxPostShareComponent', () => {
  let component: CnxPostShareComponent;
  let fixture: ComponentFixture<CnxPostShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxPostShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxPostShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
