import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';

@Component({
  selector: 'app-cnx-confirmation',
  templateUrl: './cnx-confirmation.component.html',
  styleUrls: ['./cnx-confirmation.component.scss']
})
export class CnxCOnfirmationComponent implements OnInit {
  message: string;
  confirmationMsg: string;
  commentdata: {};
  @Output() commentDeleteEmiter = new EventEmitter<any>();
  constructor(
    public modelService: BsModalService,
    public modalRef: BsModalRef,
    public cnxLanguageService: CnxLanguageService ) { }

  ngOnInit() {
  }
  confirm(): void {
    this.commentDeleteEmiter.emit(this.commentdata);
    this.modalRef.hide();
  }

  decline(): void {
    this.message = 'Declined!';
    this.modalRef.hide();
  }
}
