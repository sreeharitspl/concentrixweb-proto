import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxCOnfirmationComponent } from './cnx-confirmation.component';

describe('CnxCOnfirmationComponent', () => {
  let component: CnxCOnfirmationComponent;
  let fixture: ComponentFixture<CnxCOnfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxCOnfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxCOnfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
