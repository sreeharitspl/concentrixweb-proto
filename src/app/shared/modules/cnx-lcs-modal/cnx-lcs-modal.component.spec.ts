import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxLcsModalComponent } from './cnx-lcs-modal.component';

describe('CnxLcsModalComponent', () => {
  let component: CnxLcsModalComponent;
  let fixture: ComponentFixture<CnxLcsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxLcsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxLcsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
