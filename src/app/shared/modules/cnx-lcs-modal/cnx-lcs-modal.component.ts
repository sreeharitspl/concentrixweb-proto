import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FeedDetails } from '../../interfaces/feedDetails';
import { CnxConstants } from '../cnx-utils/cnx-constants/cnx-constants';

@Component({
  selector: 'app-cnx-lcs-modal',
  templateUrl: './cnx-lcs-modal.component.html',
  styleUrls: ['./cnx-lcs-modal.component.scss']
})
export class CnxLcsModalComponent implements OnInit {

  /*  Values got from intialstate of Modal */
  public contentId;
  public feedType;
  public feedDetails;
  public postType;
  public actionType;

  public lcsUserListModalData;

  postTypes: {
    CNC_NEWS: string;
    CNC_POLLS: string;
    CNC_Image: string;
    CNC_CheckIn: string;
    CNC_URL: string;
    CNC_Video: string;
    CNC_Alert: string;
  };
  actionTypes: {
    Like: string,
    Share: string,
    Comment: string
  };

  constructor(public commonModalRef: BsModalRef) { }

  ngOnInit() {
    this.postTypes = CnxConstants.CNX_FEEDTYPEKEY;
    this.actionTypes = CnxConstants.LCSACTIONTYPEKEY;
    console.log(this.feedDetails);
    console.log(this.feedType);
    console.log(this.postType);
    console.log(this.actionType);
  }

}
