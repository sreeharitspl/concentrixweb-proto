import { CnxLcsModalModule } from './cnx-lcs-modal.module';

describe('CnxLcsModalModule', () => {
  let cnxLcsModalModule: CnxLcsModalModule;

  beforeEach(() => {
    cnxLcsModalModule = new CnxLcsModalModule();
  });

  it('should create an instance', () => {
    expect(cnxLcsModalModule).toBeTruthy();
  });
});
