import { VideosContainerModule } from './../cnx-post/videos-container/videos-container.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CnxLcsModalComponent } from './cnx-lcs-modal.component';
import { CnxPostModule } from '../cnx-post/cnx-post.module';
import { CnxLcsDetailviewModule } from '../cnx-lcs-detailview/cnx-lcs-detailview.module';
import { NewsContainerModule } from '../cnx-post/news-container/news-container.module';
import { CnxLcsUserListModalModule } from '../cnx-lcs-detailview/cnx-lcs-user-list-modal/cnx-lcs-user-list-modal.module';
import { CnxPostShareModalModule } from '../cnx-lcs-detailview/cnx-post-share-modal/cnx-post-share-modal.module';
import { CnxCommentModalModule } from '../cnx-lcs-detailview/cnx-comment-modal/cnx-comment-modal.module';
import { CnxPostShareModalComponent } from '../cnx-lcs-detailview/cnx-post-share-modal/cnx-post-share-modal.component';

@NgModule({
  imports: [
    CommonModule,
    InfiniteScrollModule,
    CnxPostModule,
    CnxLcsDetailviewModule,
    NewsContainerModule,
    CnxLcsUserListModalModule,
    CnxPostShareModalModule,
    CnxCommentModalModule,
    VideosContainerModule
  ],
  declarations: [CnxLcsModalComponent],
  exports: [CnxLcsModalComponent],
  entryComponents:[CnxPostShareModalComponent]
})
export class CnxLcsModalModule { }
