import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxHeaderComponent } from './cnx-header/cnx-header.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CnxHeaderComponent],
  exports: [CnxHeaderComponent]
})
export class CnxHeaderModule { }
