import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxHeaderComponent } from './cnx-header.component';

describe('CnxHeaderComponent', () => {
  let component: CnxHeaderComponent;
  let fixture: ComponentFixture<CnxHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
