import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared.module';
import { CnxFooterComponent } from './cnx-footer/cnx-footer.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    CnxFooterComponent
  ],
  declarations: [CnxFooterComponent]
})
export class CnxFooterModule { }
