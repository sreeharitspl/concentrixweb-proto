import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxFooterComponent } from './cnx-footer.component';

describe('CnxFooterComponent', () => {
  let component: CnxFooterComponent;
  let fixture: ComponentFixture<CnxFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
