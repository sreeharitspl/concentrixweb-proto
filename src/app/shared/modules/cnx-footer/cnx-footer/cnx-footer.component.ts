import { Component, OnInit } from '@angular/core';

import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';

@Component({
  selector: 'app-cnx-footer',
  templateUrl: './cnx-footer.component.html',
  styleUrls: ['./cnx-footer.component.scss']
})
export class CnxFooterComponent implements OnInit {

  constructor(public cnxLanguageService: CnxLanguageService) { }

  ngOnInit() {
  }

}
