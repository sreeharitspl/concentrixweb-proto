import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxSharedUserDetailsComponent } from './cnx-shared-user-details.component';

describe('CnxSharedUserDetailsComponent', () => {
  let component: CnxSharedUserDetailsComponent;
  let fixture: ComponentFixture<CnxSharedUserDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxSharedUserDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxSharedUserDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
