/**
 * Author: TSPL-T0445
 * Created Date: 10 Sept 2018
 */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { CnxUserLCSData } from '../../../models/cnx-lcs-models/cnx-user-lcs-data.model';
import { CnxConstants } from '../../cnx-utils/cnx-constants/cnx-constants';
import { CnxLcsUserListModalData } from '../../../models/cnx-lcs-models/cnx-lcs-user-list-modal-data.model';
import { CnxLanguageService } from '../../../service/cnx-language/cnx-language.service';

@Component({
  selector: 'app-cnx-shared-user-details',
  templateUrl: './cnx-shared-user-details.component.html',
  styleUrls: ['./cnx-shared-user-details.component.scss'],
})

/**
 * Author: TSPL-T0445
 * Created Date: 10 Sept 2018
 * Class Name: CnxSharedUserDetailsComponent
 * Description: To show the shared user delails.
 */
export class CnxSharedUserDetailsComponent implements OnInit {

  @Input() userDetails: CnxUserLCSData;
  @Input() mediaType: string;
  @Output() showSharedWithUsers = new EventEmitter<CnxLcsUserListModalData>();

  constructor(public cnxLanguageService: CnxLanguageService ) { }

  ngOnInit() {
  }

 /**
  * Author: TSPL-T0445
  * Created Date: 10 Sept 2018
  * Method Name: getUserProfile
  * Description: To redirect to the user profile.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
  getUserProfile() {

  }

/**
  * Author: TSPL-T0445
  * Created Date: 12 Sept 2018
  * Method Name: showSharedPeople
  * Description: To show the shared with user list.
  * Arguments:
  *     No arguments
  * Return:
  *     void
  */
  showSharedPeople() {
    let sharedUserListRequest: CnxLcsUserListModalData;
    sharedUserListRequest = {
      ContentId : this.userDetails.SharedContentId,
      MediaType : this.mediaType,
      ActionType: CnxConstants.LCSACTIONTYPEKEY.Share,
      StatusFor : CnxConstants.LCSSTATUSFORKEY.sharedWithPeople,
      SharedWithPeopleView: false,
      ActionIcon: CnxConstants.LCSACTIONICONKEY.Share
    };

    this.showSharedWithUsers.emit(sharedUserListRequest);
  }

}
