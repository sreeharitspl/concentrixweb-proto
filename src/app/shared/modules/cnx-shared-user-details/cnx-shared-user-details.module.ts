/**
 * Author: TSPL-T0445
 * Date Created: 10 Sept 2018
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxSharedUserDetailsComponent } from './cnx-shared-user-details/cnx-shared-user-details.component';
import { SharedModule } from '../../shared.module';
import { CnxUserImageModule } from '../cnx-user-image/cnx-user-image.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CnxUserImageModule
  ],
  declarations: [
    CnxSharedUserDetailsComponent
  ],
  exports: [
    CnxSharedUserDetailsComponent
  ]
})

export class CnxSharedUserDetailsModule { }
