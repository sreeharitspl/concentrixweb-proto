import { CnxSharedUserDetailsModule } from './cnx-shared-user-details.module';

describe('CnxSharedUserDetailsModule', () => {
  let cnxSharedUserDetailsModule: CnxSharedUserDetailsModule;

  beforeEach(() => {
    cnxSharedUserDetailsModule = new CnxSharedUserDetailsModule();
  });

  it('should create an instance', () => {
    expect(cnxSharedUserDetailsModule).toBeTruthy();
  });
});
