import { Pipe, PipeTransform } from '@angular/core';
import { CnxLanguageService } from '../../service/cnx-language/cnx-language.service';

@Pipe({
  name: 'translate'
})
export class TranslatePipe implements PipeTransform {

  constructor(public cnxLanguageService: CnxLanguageService) {

  }

  /*
    * Author: T0423
    * Function: transform
    * Description:  Function to get the value for a particular key from language directory
    * Arguments:
    *     Param 1: value : string : Key for which the translation is to be searched
    *     Param 2: args  : any    : Optional Parameters
    * Return:
    *     transformedInput: string: Translated value for the input key
    */
  transform(value: any, args?: any): any {
    let transformedInput = this.cnxLanguageService.languageDictionary[value];
    transformedInput = (transformedInput !== undefined) ? transformedInput : value;
    return transformedInput;
  }

}
