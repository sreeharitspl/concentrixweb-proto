import { Pipe, PipeTransform } from '@angular/core';
import { CnxLanguageService } from '../../service/cnx-language/cnx-language.service';
import { TranslatePipe } from '../translate/translate.pipe';

@Pipe({
  name: 'translatedate'
})
export class TranslatedatePipe implements PipeTransform {
  constructor(
      public cnxLanguageService: CnxLanguageService,
      public translatePipe: TranslatePipe,
    ) {}
  /*
    * Author: T0446
    * Function: transform
    * Description:  Function is using to format the post date
    * Arguments:
    *     Param 1: value : string : Key for which the translation is to be searched
    *     Param 2: args  : any    : Optional Parameters
    * Return:
    *     transformedInput: string: Formated
    */

  transform(value: any, args?: any): any {
    if (value) {
    const input = value;
      const oneHr = 60 * 60 * 1000; // one hr in ms
      let retVal   = '';
      const postDate = new Date(input);
      const date = new Date();
      const y  = date.getUTCFullYear();
      const mo = date.getUTCMonth()+1;
      const d  = date.getUTCDate();
      let h  = date.getUTCHours();
      const m  = date.getUTCMinutes();
      const s  = date.getUTCSeconds();
      const ampm = h >= 12 ? 'PM' : 'AM';
      h = h % 12;
      h = h ? h : 12; // the hour '0' should be '12'
      const conCatMinutes = m < 10 ? '0' + m : m;

      const todayStr = mo + '/' + d + '/' + y + ' ' + h + ':' + conCatMinutes + ':' + s + ' ' + ampm;
      const today = new Date(todayStr);

      const diffTime = Math.abs(today.getTime() - postDate.getTime());
      const onedayTime = 24 * oneHr;
      const twodayTime = 48 * oneHr;
      const sevenDay   = 7 * onedayTime;
      retVal = this.getSystemTimeFromUTC(input);

      if (diffTime < oneHr && diffTime > 0) {
        const minAgo = this.translateFilter('FEW_MIN_AGO');
          retVal = minAgo;
      } else if (diffTime <= onedayTime && diffTime > 0) {
        const hoursAgo = this.translateFilter('HRS_AGO');
          retVal = Math.abs(Math.ceil(diffTime / oneHr)) + ' ' + hoursAgo;
      } else if (diffTime > onedayTime && diffTime <= twodayTime) {
          retVal = this.translateFilter('YESTERDAY');
      } else if (diffTime > twodayTime && diffTime <= sevenDay) {
        const timeDiff = Math.abs(today.getTime() - postDate.getTime());
        const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
          retVal = diffDays + ' ' + this.translateFilter('DAYS_AGO');
      } else if (diffTime > 0 && diffTime > twodayTime) {
          retVal = this.getSystemTimeFromUTC(input);
      }
      return retVal;
  } else {
      return '';
  }
  }

  getSystemTimeFromUTC(UTCstr) {
    UTCstr += ' UTC';
    const date = new Date(UTCstr);
    const m    = this.getZeroString(date.getMonth()+1);
    const d    = this.getZeroString(date.getDate());
    const y    = date.getFullYear();
    const h    = this.getZeroString(date.getHours());
    const mi   = this.getZeroString(date.getMinutes());
    const s    = this.getZeroString(date.getSeconds());
    // tslint:disable-next-line:radix
    const ampm = parseInt(h) >= 12 ? 'PM' : 'AM';
    const retStr = m + '/' + d + '/' + y + ' ' + this.getHours(h) + ' ' + mi + ':' + s + ' ' + ampm;
    return retStr;
}
getZeroString(str) {
  return str < 10 ? '0' + str : str;
}
getHours(hr) {
  let retVal = '';
  // tslint:disable-next-line:radix
  if (parseInt(hr) === 0) {
      retVal = '12';
      // tslint:disable-next-line:radix
  } else if (parseInt(hr) <= 12) {
      // tslint:disable-next-line:radix
      retVal = this.getZeroString(parseInt(hr));
  } else {
      // tslint:disable-next-line:radix
      retVal = this.getZeroString(parseInt(hr) - 12);
  }
  return retVal;
}

translateFilter(item) {
    let returnData = item;
    const localData = this.translatePipe.transform(item, this.cnxLanguageService.lastUpdatedTime);
    if ( localData != null && localData !== '') {
        returnData = localData ? localData : item;
    }
    return returnData;
}
}
