import { Pipe, PipeTransform } from '@angular/core';
import { CnxLanguageService } from '../../service/cnx-language/cnx-language.service';
import { CnxConstants } from '../../modules/cnx-utils/cnx-constants/cnx-constants';
import { TranslatePipe } from '../translate/translate.pipe';

@Pipe({
  name: 'expirydate'
})
export class ExpirydatePipe implements PipeTransform {
  constructor(
    public cnxLanguageService: CnxLanguageService,
    public translatePipe: TranslatePipe ) {
  }

  /*
    * Author: T0446
    * Function: transform
    * Description:  Function is using to format the post dateshow expiry date of a poll
    * Arguments:
    *     Param 1: value : any: Transform text - transformText
    *     Param 2: args  : any: How many days remaing count - expDatediff
    * Return:
    *     transformedInput: string: Expiry text
    */

  transform(value: any, transformText: any, expDatediff: any): any {
    let returnData = this.translatePipe.transform(transformText, this.cnxLanguageService.lastUpdatedTime);
    if (returnData) {
      returnData = returnData.replace(CnxConstants.CNX_GET_REPLACEMENT_CHAR.CNX_REPLACE_D, expDatediff);
   }
   return returnData;
  }

}
