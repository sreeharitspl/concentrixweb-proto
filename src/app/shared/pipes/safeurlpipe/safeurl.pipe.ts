/*
    * Author: T0475
    * Description:  pipe to transform unsafe url to safe url
    * Arguments:
    *     Param 1: url : string : Key for which the translation
    * Return:
    *     transformedInput: string: Safe url
    */
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';

@Pipe({
  name: 'safeurl'
})
export class SafeurlPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url) {
    if (!url) { return ''; }
    if (url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
  }
}
