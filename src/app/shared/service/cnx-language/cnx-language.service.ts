import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';

import { CnxGlobalService } from '../../modules/cnx-utils/cnx-global/cnx-global.service';
import { CnxLocalStorageService } from '../cnx-local-storage/cnx-local-storage.service';
import { CnxConstants } from '../../modules/cnx-utils/cnx-constants/cnx-constants';

@Injectable({
  providedIn: 'root'
})
export class CnxLanguageService {

  constructor() {}

  public languageDictionary: any = {};
  public languageList: any[] = [];
  public lastUpdatedTime: any;

}
