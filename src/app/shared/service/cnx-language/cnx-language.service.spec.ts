import { TestBed, inject } from '@angular/core/testing';

import { CnxLanguageService } from './cnx-language.service';

describe('CnxLanguageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxLanguageService]
    });
  });

  it('should be created', inject([CnxLanguageService], (service: CnxLanguageService) => {
    expect(service).toBeTruthy();
  }));
});
