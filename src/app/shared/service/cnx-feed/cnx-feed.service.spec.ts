import { TestBed, inject } from '@angular/core/testing';

import { CnxFeedService } from './cnx-feed.service';

describe('CnxFeedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxFeedService]
    });
  });

  it('should be created', inject([CnxFeedService], (service: CnxFeedService) => {
    expect(service).toBeTruthy();
  }));
});
