import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { CnxGlobalService } from '../../modules/cnx-utils/cnx-global/cnx-global.service';
import { CnxPollsResponse } from '../../models/cnx-polls-response';
import { PostFeeds } from '../../models/cnx-post-models/post-feeds.model';
import { CnxMapFeeds } from '../../models/cnx-post-models/cnx-map-feeds';
import { CnxDummyFeed } from '../../../report/api-source/cnx-dummy-feed';


@Injectable({
  providedIn: 'root'
})

export class CnxFeedService {
  /**for dummy feed object */
  public dummyFeed: any;

  constructor( private httpService: HttpService,
     private cnxGlobalService: CnxGlobalService) {
    /**set dummy feed with json from CNXDUMMYFEEDARRAY*/
  }
  /*
    * Author: T0446
    * Function: getAllFeed
    * Description:  This function used to fetch timeline data
    * Arguments:
    *     Param 1: url        : string
    *     Param 2: inputs     : object
    *     Param 3: cbSuccess  : success callback
    *     Param 4: cbError    : error callback
    *     param 5: showLoader : Boolean
    * Return:
    *     Feed data: array: Returns feed /posts array
    */
  public getAllFeed(url, inputs, cbSuccess, cbError, showLoader?) {
    this.dummyFeed = CnxDummyFeed.CNXDUMMYFEEDARRAY;
    if (showLoader) {
      this.cnxGlobalService.showLoader();
    }
    this.httpService.postX(url, inputs)
      .subscribe(result => {
        /**Dummy feed starts*/
        // result.Feeds = result.Feeds.concat(this.dummyFeed.Feeds);
        /**Dummy feed ends*/
        result = new CnxMapFeeds().mapFeedData(result);
        cbSuccess(result);
      },
        error => cbError(error)
      /**Dummy feed even if server error occur starts */
      // error => cbError(this.dummyFeed)
      /**Dummy feed even if server error occur ends */
      );
  }
    /*
    * Author: T0446
    * Function: updateLikeStatus
    * Description:  This function used to update like / unlike status of a post
    * Arguments:
    *     Param 1: url        : string
    *     Param 2: inputs     : object
    *     Param 3: cbSuccess  : success callback
    *     Param 4: cbError    : error callback
    *     param 5: showLoader : Boolean
    * Return:
    *     Feed data: array: Returns feed /posts array
    */
  public updateLikeStatus(url, inputs, cbSuccess, cbError, showLoader?) {
    if (showLoader) {
      this.cnxGlobalService.showLoader();
    }
    this.httpService.postX(url, inputs)
      .subscribe(result => {
        cbSuccess(result);
      },
      error => cbError(error)
      );
  }
  /*
    * Author: T0446
    * Function: updateLikeStatus
    * Description:  This function used to update like / unlike status of a post
    * Arguments:
    *     Param 1: url        : string
    *     Param 2: inputs     : object
    *     Param 3: cbSuccess  : success callback
    *     Param 4: cbError    : error callback
    *     param 5: showLoader : Boolean
    * Return:
    *     Feed data: array: Returns feed /posts array
    */
    public getDataForFeed(url, inputs, cbSuccess, cbError, showLoader?) {
      let cnxPollsResponse: CnxPollsResponse;

      if (showLoader) {
        this.cnxGlobalService.showLoader();
      }
      this.httpService.postX(url, inputs)
        .subscribe(result => {
          cnxPollsResponse = new CnxPollsResponse().deserialize(result);
          cbSuccess(cnxPollsResponse);
        },
        error => cbError(error)
        );
    }
}
