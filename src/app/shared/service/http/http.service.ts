import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { environment } from '../../../../environments/environment';
import { CnxConstants } from '../../../shared/modules/cnx-utils/cnx-constants/cnx-constants';
import { CnxLocalStorageService } from '../cnx-local-storage/cnx-local-storage.service';
import { CnxGlobalService } from '../../modules/cnx-utils/cnx-global/cnx-global.service';
import { DatePipe } from '@angular/common';
import { CnxToastrService } from '../cnx-toastr/cnx-toastr.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient, public localStorage: CnxLocalStorageService,
    public globalService: CnxGlobalService, private datePipe: DatePipe, private toastrService: CnxToastrService) { }

    private _getHeadersConfig(isPostX = false) {
        const config = {};
        if (!isPostX) {
            config['Accept'] = 'application/json, text/plain, */*';
            config['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        }
        return config;
    }

    private getUrlParams(params) {
        let paramText = '?';
        for (const key in params) {
            if (params.hasOwnProperty(key) && params[key]) {
                paramText += `${key}=${params[key]}&`;
            }
        }
        return paramText;
    }

    /*
    * Author: TSPL-T0423
    * Function: get
    * Description:  HTTP Get call
    * Arguments: path : API endpoint url
    *            requestParams: API request params
    * Return: Observable for the api call. Resolved or rejected based on the api response
    */
    public get(path: string, params: URLSearchParams = new URLSearchParams()) {
        return this.http
            .get(`${environment.api_url}${path}${this.getUrlParams(params)}`, { headers: new HttpHeaders(this._getHeadersConfig()) })
            .pipe(map(data => {}),
            catchError((err: HttpErrorResponse) => {
                return this.errorResponse(err);
            }));
    }

    /*
    * Author: TSPL-T0423
    * Function: postX
    * Description:  HTTP Post call
    * Arguments: path : API endpoint url
    *            requestParams: API request params
    * Return: Observable for the api call. Resolved or rejected based on the api response
    */
    public postX(path: string, requestParams: Object = {}): Observable<any> {
        const cnxToken = this.localStorage.getItem(CnxConstants.LOCALSTORAGEKEYS.CNX_TOKEN);
        const httpHeaders = new HttpHeaders({
            'Content-Type' : 'application/json',
            'Token': cnxToken ? cnxToken : ''
        });
        requestParams['AppName'] = CnxConstants.APPLICATIONCONSTANTS.CNX_APP_NAME;
        return this.http
            .post(`${environment.api_url}${path}`, requestParams, { headers: httpHeaders })
            .pipe(map(
                data => this.successResponse(data)
            ),
            catchError((err: HttpErrorResponse) => {
                return this.errorResponse(err);
            }));
    }

    /*
    * Author: TSPL-T0423
    * Function: successResponse
    * Description:  Function to parse the api success reponse and handle the fallback of the parent function
    * Arguments: response : API success response
    * Return: If success - returns the object.
    *         If error   - Throws the error.
    */
    private successResponse(response) {
        let successFlag = false;
        let message;
        if (response.Message === 'Invalid Token') {
            this.toastrService.showErrorToster(CnxConstants.ERROR_MESSAGES.CNX_SESSION_EXPIRED);
            this.globalService.navigateTo(['login']);
        } else {
            response.ResponseTime = this.datePipe.transform(new Date(), CnxConstants.APPLICATIONCONSTANTS.CNX_API_TIME_FORMAT);
            if (response != null) {
                if (1 === response.Acknowledge) {
                    successFlag = true;
                } else if (0 === response.Acknowledge) {
                    if (response.ErrorCode != null && response.ErrorCode.length > 0) {
                        if (Array.isArray(response.ErrorCode)) {
                            message = response.ErrorCode[0].Code;
                        } else {
                            message = response.ErrorCode.Code;
                        }
                        this.toastrService.showErrorToster(message);
                    }
                } else {
                    // No Acknowledgement
                    this.toastrService.showErrorToster(CnxConstants.ERROR_MESSAGES.CNX_RESPONSE_NULL);
                }
            } else {
                // Null Response
                this.toastrService.showErrorToster(CnxConstants.ERROR_MESSAGES.CNX_RESPONSE_NULL);
            }
        }

        if (successFlag) {
            return response;
        } else {
            return throwError(response);
        }
    }

    /*
    * Author: TSPL-T0423
    * Function: errorResponse
    * Description:  Function to parse the api error reponse and handle the fallback of the parent function
    * Arguments: response : API error response
    * Return: Show toster message and throws the error.
    */
    private errorResponse(response) {
        let message;
        if (response) {
            if (response.Message === 'Invalid Token') {
                this.toastrService.showErrorToster(CnxConstants.ERROR_MESSAGES.CNX_SESSION_EXPIRED);
                this.globalService.navigateTo(['login']);
            } else {
                if (response.status === '-1') {
                    this.toastrService.showErrorToster(CnxConstants.ERROR_MESSAGES.CNX_TRY_AFTER_SOME_TIME);
                } else {
                    if (response.Acknowledge === 0) {
                        if (Array.isArray(response.data.ErrorCode)) {
                            message = response.data.ErrorCode[0].Code;
                        } else {
                            message = response.data.ErrorCode.Code;
                        }
                        this.toastrService.showErrorToster(message);
                    } else {
                        this.toastrService.showErrorToster(CnxConstants.ERROR_MESSAGES.CNX_RESPONSE_NULL);
                    }
                }
            }
        } else if (response && response.status === '-1') {
            this.toastrService.showErrorToster(CnxConstants.ERROR_MESSAGES.CNX_CONNECTION_TIMEOUT);
        } else {
            this.toastrService.showErrorToster(CnxConstants.ERROR_MESSAGES.CNX_RESPONSE_NULL);
        }
        return throwError(response);
    }
}
