import { TestBed, inject } from '@angular/core/testing';

import { CnxLocalStorageService } from './cnx-local-storage.service';

describe('CnxLocalStorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxLocalStorageService]
    });
  });

  it('should be created', inject([CnxLocalStorageService], (service: CnxLocalStorageService) => {
    expect(service).toBeTruthy();
  }));
});
