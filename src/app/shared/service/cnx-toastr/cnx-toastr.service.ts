import { Injectable } from '@angular/core';
import { CnxLanguageService } from '../cnx-language/cnx-language.service';
import { TranslatePipe } from '../../pipes/translate/translate.pipe';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class CnxToastrService {

  constructor(
    public cnxLanguageService: CnxLanguageService,
    public translatePipe: TranslatePipe,
    private toastr: ToastrService
  ) { }

  /*
   * Author: TSPL-T0423
   * Function: showToster
   * Description:  Function to show toster message
   * Arguments: message : string : message to be displyed
   * Return: Nil
  */
  public showErrorToster(message) {
    const translatedmsg = this.translatePipe.transform(message, this.cnxLanguageService.lastUpdatedTime);
    this.toastr.error(translatedmsg);
  }
}
