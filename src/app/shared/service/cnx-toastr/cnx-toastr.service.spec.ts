import { TestBed, inject } from '@angular/core/testing';

import { CnxToastrService } from './cnx-toastr.service';

describe('CnxToastrService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxToastrService]
    });
  });

  it('should be created', inject([CnxToastrService], (service: CnxToastrService) => {
    expect(service).toBeTruthy();
  }));
});
