/*
    * Author: T0475
    * interface: CnxComments
    * Description: comment detail
    */
export interface CnxComments {
    CommentBy: string;
    CommentId: number;
    CommentMessage: string;
    CommentOn: string;
    ContentId: number;
    EmployeeId: string;
    IsContentFlagged: boolean;
    IsLiked: boolean;
    LikedCount: number;
    ProfileImageUrl: string;
}
