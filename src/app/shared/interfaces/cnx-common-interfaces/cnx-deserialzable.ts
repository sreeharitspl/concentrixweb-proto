/**
 * Author: TSPL-T0445
 * Created Date: 11 Sept 2018
 * Interface Name: CnxDeserialzable
 * Description: Inteface for Deserialization
 */
export interface ICnxDeserialzable {
    deserialize(input: any): this;
}
