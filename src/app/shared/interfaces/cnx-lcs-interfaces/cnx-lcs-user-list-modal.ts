/*
 * Author: T0475
 * interface: ICnxLcsUserListModal
 * Description: LCS user list view model.
 */
export interface ICnxLcsUserListModal {
    ContentId: number;
    MediaType: string;
    StatusFor: string;
}
