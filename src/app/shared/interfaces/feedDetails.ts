/*
 * Created: T0393
 * Description: Feed details to share the information between post-lcs
 */
export interface FeedDetails {
  FeedType: string;
  ContentId: number;
  AllowShare: boolean;
  CommentsCount: number;
  EnableComments: boolean;
  EnableLikes: boolean;
  IsRead: boolean;
  IsShared: boolean;
  LikedStatus: boolean;
  LikesCount: number;
  SharedCount: number;
  SharedURL: string;
  SharedWithCount: number;
  NewsId: number;
}
