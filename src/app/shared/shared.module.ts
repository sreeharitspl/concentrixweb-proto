import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { PipeModule } from './pipes/pipe/pipe.module';
import { CnxLanguageModule } from './modules/cnx-language/cnx-language.module';
import { TranslatePipe } from './pipes/translate/translate.pipe';
import { CnxGlobalService } from './modules/cnx-utils/cnx-global/cnx-global.service';
import { CnxLocalStorageService } from './service/cnx-local-storage/cnx-local-storage.service';
import { TranslatedatePipe } from './pipes/translatedate/translatedate.pipe';
import { ExpirydatePipe } from './pipes/expirydate/expirydate.pipe';
import { CnxCOnfirmationComponent } from './modules/cnx-confirmation/cnx-confirmation/cnx-confirmation.component';
import { SafeurlPipe } from './pipes/safeurlpipe/safeurl.pipe';

@NgModule({
  imports: [
    CommonModule,
    CnxLanguageModule,
    PipeModule,
  ],
  exports: [
    CnxLanguageModule,
    PipeModule,
    TranslatedatePipe,
    ExpirydatePipe,
    SafeurlPipe
  ],
  declarations: [
    TranslatedatePipe,
    ExpirydatePipe,
    CnxCOnfirmationComponent,
    SafeurlPipe,
  ],
  providers: [
    DatePipe
  ],
  entryComponents: [
    CnxCOnfirmationComponent
  ]
})
export class SharedModule { }
