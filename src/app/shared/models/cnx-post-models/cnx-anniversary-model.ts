import { PostFeeds } from './post-feeds.model';

export class CnxAnniversaryModel extends PostFeeds {
    CanRemove: boolean;
    CommentsCount: number;
    FunctionDate: string;
    FunctionTitle: string;
    FunctionType: string;
    IsContentFlagged: number;
    LikedStatus: boolean;
    LikesCount: number;
    WishBy: string;
    WishByEmployeeId: string;
    WishByProfileImageUrl: string;
    WishId: number;
    WishMessage: string;
    WishPostedOn: string;
    WishTo: string;
    WishToEmployeeId: string;
    WishToProfileImageUrl: string;
    mapAlertData(data, feedType) {
        this.mapData(data, feedType);
        this.CanRemove           = data.SenderId;
        this.CommentsCount         = data.SenderName;
        this.FunctionDate  = data.MessageDeliveryId;
        this.FunctionTitle      = data.MessageTypeId;
        this.FunctionType        = data.MessageType;
        this.IsContentFlagged     = data.MessageGroupId;
        this.LikedStatus            = data.Subject;
        this.LikesCount     = data.MessageContent;
        this.WishBy        = data.MessageDate;
        this.WishByEmployeeId      = data.IsMessageRead;
        this.WishByProfileImageUrl     = data.IsFileAttached;
        this.WishId        = data.Attachments;
        this.WishMessage     = data.MessageContent;
        this.WishPostedOn        = data.MessageDate;
        this.WishTo      = data.IsMessageRead;
        this.WishToEmployeeId     = data.IsFileAttached;
        this.WishToProfileImageUrl        = data.Attachments;
        return this;
    }
}

