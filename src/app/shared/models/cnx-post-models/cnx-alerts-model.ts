import { PostFeeds } from './post-feeds.model';

export class CnxAlertsModel extends PostFeeds {
    SenderId: number;
    SenderName: string;
    MessageDeliveryId: number;
    MessageTypeId: number;
    MessageType: string;
    MessageGroupId: number;
    Subject: string;
    MessageContent: string;
    MessageDate: Date;
    IsMessageRead: boolean;
    IsFileAttached: boolean;
    Attachments: Array<any>;
    mapAlertData(data, feedType) {
        this.mapData(data, feedType);
        this.SenderId           = data.SenderId;
        this.SenderName         = data.SenderName;
        this.MessageDeliveryId  = data.MessageDeliveryId;
        this.MessageTypeId      = data.MessageTypeId;
        this.MessageType        = data.MessageType;
        this.MessageGroupId     = data.MessageGroupId;
        this.Subject            = data.Subject;
        this.MessageContent     = data.MessageContent;
        this.MessageDate        = data.MessageDate;
        this.IsMessageRead      = data.IsMessageRead;
        this.IsFileAttached     = data.IsFileAttached;
        this.Attachments        = data.Attachments;
        return this;
    }
}

