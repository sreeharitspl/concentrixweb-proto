import { CnxConstants } from '../../modules/cnx-utils/cnx-constants/cnx-constants';

export class PostFeeds {
    ProfileImageUrl: string;
    AllowShare: boolean;
    ArticleScope: string;
    CommentsCount: number;
    EnableComments: boolean;
    EnableLikes: boolean;
    FeedType: string;
    IsRead: boolean;
    IsShared: boolean;
    LikedStatus: boolean;
    LikesCount: number;
    ContentId: number;
    ContentParentId: number;
    PostedByEmployeeId: number;
    PostedByProfileImageUrl: string;
    PublishedBy: string;
    PublishedOn: string;
    SharedCount: number;
    SharedInfo: {
      SharedByEmployeeId: number;
      SharedByName: string;
      ProfileImageUrl: string;
      SharedOn: string;
      SharedDescription: string;
      FirstSharedWithName: string;
    };
    SharedWithCount: number;
    Thumbnail: string;
    Title: string;
    PostedBy: string;
    PostedOn: string;
    commentStatusCount: number;
    ShortDescription: string;
    mapData(data, feedType) {
      this.ContentId = this.getContentId(data, feedType),
      Object.assign(this, data);
        return this;
    }

    /**
  * Author: T0446
  * Method Name: getContentId
  * Description: This function is using to get the content id based on feed type.
  * Arguments:
  * Param 1: feedDetails: objet
  * Param 2: feedType   : string
  * Return: Content id  : number
  */
 public getContentId(feedDetails, feedType) {
  let returnData: number;
  returnData = 0;
  if (feedType && feedDetails) {
     const keyName = this.getKeyFromFeedDetails(feedDetails, feedType);
     returnData = keyName;
  }
  return returnData;
}
/**
 * Author: T0446
 * Method Name: getKeyFromFeedDetails
 * Description: This function will return the key name from the feed details object.
 * Arguments:
 * Param 1: feedDetails: objet
 * Param 2: feedType   : string
 * Return: Key Name    : strin
 */
    private getKeyFromFeedDetails(feedDetails, feedType) {
      const keyNameArray = this.keyDefinition();
      let returndata: number;
      returndata = 0;
      if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_NEWS) {
        returndata = feedDetails[keyNameArray[feedType].keyName];
      } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_POLLS) {
        returndata = feedDetails[keyNameArray[feedType].keyName];
      } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_URL) {
        returndata = feedDetails[keyNameArray[feedType].keyName];
      } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_Video) {
        returndata = feedDetails[keyNameArray[feedType].keyName];
      }
      /* else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_Image) {
        returndata = feedDetails[keyNameArray[feedType].keyName];
      } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_CheckIn) {
        returndata = feedDetails[keyNameArray[feedType].keyName];
      } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_URL) {
        returndata = feedDetails[keyNameArray[feedType].keyName];
      } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_Video) {
        returndata = feedDetails[keyNameArray[feedType].keyName];
      } else if (feedType === CnxConstants.CNX_FEEDTYPEKEY.CNC_POLLS) {
        returndata = feedDetails[keyNameArray[feedType].keyName];
      } */
      return returndata;
    }
    /**
  * Author: T0446
  * Method Name: keyDefinition
  * Description: This function will define the key structure.
  * Arguments:Nill
  */
    private keyDefinition() {
      return {
        'News': {
          'keyName': 'NewsId'
        }, 'Polls': {
          'keyName': 'AssessmentId'
        }, 'Image': {
          'keyName': 'ContentId'
        }, 'URL': {
          'keyName': 'ContentId'
        }, 'Video': {
          'keyName': 'VideoId'
        }  // Need to add other feed sturucture latter, now the visibility of the structure is very less
      };
    }
}


