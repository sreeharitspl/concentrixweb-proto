import { PostFeeds } from './post-feeds.model';

export class CnxCheckinModel extends PostFeeds {
        EmployeeId: number;
        EmployeeName: string;
        Country: string;
        City: string;
        Time: string;
        Latitude: string;
        Longitude: string;
        LocationImageUrl: string;

    mapCheckInData(data, feedType) {
        this.mapData(data, feedType);
        this.EmployeeId         = data.EmployeeId;
        this.EmployeeName       = data.EmployeeName;
        this.Country            = data.Country;
        this.City               = data.City;
        this.Time               = data.Time;
        this.Latitude           = data.Latitude;
        this.Longitude          = data.Longitude;
        this.LocationImageUrl   = data.LocationImageUrl ? data.LocationImageUrl : '';
        return this;
    }
}
