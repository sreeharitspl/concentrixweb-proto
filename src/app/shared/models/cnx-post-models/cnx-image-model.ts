import { PostFeeds } from './post-feeds.model';

export class CnxImageModel extends PostFeeds {
    Images: Array<string>;
    SharedURL?: any;
    mapImageData(data, feedType) {
        this.mapData(data, feedType);
        this.Images = data.Images;
        this.SharedURL = data.SharedURL;
        return this;
    }
}
