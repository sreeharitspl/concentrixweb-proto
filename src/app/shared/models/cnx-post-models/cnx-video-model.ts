import { PostFeeds } from './post-feeds.model';

export class CnxVideoModel extends PostFeeds {
    VideoId: number;
    VideoParentId: number;
    VideoUrl: string;
    ExpiryDate: Date;
    PublisherName: string;
    SharedURL?: any;

    mapVideoData(data, feedType) {
        this.mapData(data, feedType);
        this.VideoId        = data.VideoId;
        this.VideoParentId  = data.VideoParentId;
        this.VideoUrl       = data.VideoUrl;
        this.ExpiryDate     = data.ExpiryDate;
        this.PublisherName  = data.PublisherName;
        this.SharedURL      = data.SharedURL;
        return this;
    }
}
