import { PostFeeds } from './post-feeds.model';

export class CnxPollsModel extends PostFeeds {
    AssessmentId: string;
    AssessmentStatus: number;
    AssessmentTitle: string;
    AssessmentType: string;
    Category: string;
    Department: string;
    ExpiryDate?: any;

    mapPollsData(data, feedType) {
        this.mapData(data, feedType);
        this.AssessmentId       = data.AssessmentId;
        this.AssessmentStatus   = data.AssessmentStatus;
        this.AssessmentTitle    = data.AssessmentTitle;
        this.AssessmentType     = data.AssessmentType;
        this.Category           = data.Category;
        this.Department         = data.Department;
        this.ExpiryDate         = data.ExpiryDate;
        return this;
    }
}
