import { PostFeeds } from './post-feeds.model';
import { CnxConstants } from '../../modules/cnx-utils/cnx-constants/cnx-constants';
import { CnxUrlModel } from './cnx-url-model';
import { CnxNewsModel } from './cnx-news-model';
import { CnxCheckinModel } from './cnx-checkin-model';
import { CnxPollsModel } from './cnx-polls-model';
import { CnxImageModel } from './cnx-image-model';
import { CnxVideoModel } from './cnx-video-model';
import { CnxAlertsModel } from './cnx-alerts-model';

export class CnxMapFeeds {

     postFeed: PostFeeds;
     newsDetails: CnxNewsModel;
     urlDetail: CnxUrlModel;
     checkInDetails: CnxCheckinModel;
     cnxPollsDetails: CnxPollsModel;
     cnxImageDetails: CnxImageModel;
     cnxVideoDetails: CnxVideoModel;
     cnxAlertDetails: CnxAlertsModel;

   mapFeedData(data) {
        if (data && data.Feeds) {
            data.Feeds.forEach((feedData, index) => {
                if (feedData.FeedType === CnxConstants.CNX_FEEDTYPEKEY['CNC_CheckIn']) {

                    feedData.CheckInDetails = new CnxCheckinModel().mapCheckInData(feedData.CheckInDetails, feedData.FeedType);

                } else if (feedData.FeedType === CnxConstants.CNX_FEEDTYPEKEY['CNC_NEWS']) {

                    feedData.NewsDetails = new CnxNewsModel().mapNewsData(feedData.NewsDetails, feedData.FeedType);

                } else if (feedData.FeedType === CnxConstants.CNX_FEEDTYPEKEY['CNC_POLLS']) {

                    feedData.PollDetails = new CnxPollsModel().mapPollsData(feedData.PollDetails, feedData.FeedType);

                } else if (feedData.FeedType === CnxConstants.CNX_FEEDTYPEKEY['CNC_Image']) {

                    feedData.ImageDetails = new CnxImageModel().mapImageData(feedData.ImageDetails, feedData.FeedType);

                } else if (feedData.FeedType === CnxConstants.CNX_FEEDTYPEKEY['CNC_URL']) {

                    feedData.URLDetails = new CnxUrlModel().mapUrlData(feedData.URLDetails, feedData.FeedType);

                } else if (feedData.FeedType === CnxConstants.CNX_FEEDTYPEKEY['CNC_Video']) {

                    feedData.VideoDetails = new CnxVideoModel().mapVideoData(feedData.VideoDetails, feedData.FeedType);
                } else if (feedData.FeedType === CnxConstants.CNX_FEEDTYPEKEY['CNC_Alert']) {

                    feedData.AlertDetails = new CnxAlertsModel().mapAlertData(feedData.AlertDetails, feedData.FeedType);
                }
            });
        }
        return data;
    }
}
