import { PostFeeds } from './post-feeds.model';

export class CnxNewsModel extends PostFeeds {
    ArticleScope: string;
    ImageLinks: Array<any>;
    VideoUrl?: any;
    SharedURL?: any;
    OnBehalfOf?: any;

    mapNewsData(data, feedType) {
      this.mapData(data, feedType);
      this.ArticleScope = data.ArticleScope;
      this.VideoUrl = data.VideoUrl;
      this.SharedURL = data.SharedURL;
      this.OnBehalfOf = data.OnBehalfOf;
      this.ImageLinks = data.ImageLinks;
      return this;
    }
}
