import { CnxCommonResponse } from './cnx-common-models/cnx-common-response.model';
import { ICnxDeserialzable } from '../interfaces/cnx-common-interfaces/cnx-deserialzable';


export class CnxPollsResponse extends CnxCommonResponse implements ICnxDeserialzable {

    AssessmentId: string;
    AssessmentStatus: number;
    AssessmentTitle: string;
    AssessmentType: string;
    Category: string;
    Department: string;
    DonotShowAgain: boolean;
    DonotSkip: boolean;
    ExpiryDate: Date;
    Options: Array<any>;
    Question: string;
    Response: Array<any>;
    SelectedOption: any;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}
