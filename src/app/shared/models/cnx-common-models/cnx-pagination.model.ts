/**
 * Author: TSPL-T0445
 * Created Date: 13 Sept 2018
 * Class Name: CnxPagination
 * Description: Common pagination model for API request.
 */
export class CnxPagination {
    PageNumber: number;
    PerPage: number;
}
