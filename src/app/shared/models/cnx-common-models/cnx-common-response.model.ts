/**
 * Author: TSPL-T0445
 * Created Date: 11 Sept 2018
 */
import { CnxErrorCode } from './cnx-error-code.model';

/**
 * Author: TSPL-T0445
 * Created Date: 11 Sept 2018
 * Class Name: CnxCommonResponse
 * Description: Common API response model.
 */
export class CnxCommonResponse {
  Acknowledge: number;
  Message: string;
  ErrorCode: CnxErrorCode[];
}
