/**
 * Author: TSPL-T0445
 * Created Date: 11 Sept 2018
 * Class Name: CnxErrorCode
 * Description: Common error model for API response.
 */
export class CnxErrorCode {
    Code: number;
    Description: string;
}
