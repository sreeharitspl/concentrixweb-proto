/**
 * Author: TSPL-T0445
 * Created Date: 06 Sept 2018
 * Class Name: ShareRequestDetails
 * Description: Model class used for share functionality.
 */
export class ShareRequestDetails {

    ContentId: number;
    ContextId: number;
    /**
     *  SharedDiscription - The spelling is incorrect in the API requst.
     *  TODO: Need to change in API.
     */
    SharedDiscription: string;
    TypeOfSharing: string;

}
