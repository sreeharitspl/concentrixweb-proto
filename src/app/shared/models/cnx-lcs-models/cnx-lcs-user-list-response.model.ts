/**
 * Author: TSPL-T0445
 * Created Date: 11 Sept 2018
 */
import { ICnxDeserialzable } from '../../interfaces/cnx-common-interfaces/cnx-deserialzable';

import { CnxCommonResponse } from '../cnx-common-models/cnx-common-response.model';
import { CnxUserLCSData } from './cnx-user-lcs-data.model';

/**
 * Author: TSPL-T0445
 * Created Date: 11 Sept 2018
 * Class Name: CnxLCSUserListResponse
 * Description: Like/Share User List API response model.
 */
export class CnxLCSUserListResponse extends CnxCommonResponse implements ICnxDeserialzable {

    LikedSharedDetails: CnxUserLCSData[];
    /**
     * Author: TSPL-T0445
     * Created Date: 11 Sept 2018
     * Method Name: deserialize
     * Description: To deserialize the input into the object of CnxLCSUserListResponse
     * @param input : any : JSON data to be deserialized into Class Object
     */
    deserialize(input: any) {
        let data: CnxLCSUserListResponse;
        data = this;
        input.LikedSharedDetails.forEach(userDetails => {
            userDetails = new CnxUserLCSData().deserialize(userDetails);
        });
        Object.assign(data, input);
        return this;
      }
}
