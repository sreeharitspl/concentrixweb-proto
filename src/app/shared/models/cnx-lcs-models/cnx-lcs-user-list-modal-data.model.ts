/**
 * Author: TSPL-T0445
 * Created Date: 14 Sept 2018
 */
import { ICnxLcsUserListModal } from '../../interfaces/cnx-lcs-interfaces/cnx-lcs-user-list-modal';

/**
 * Author: TSPL-T0445
 * Created Date: 14 Sept 2018
 * Class Name: CnxLcsUserListModalData
 * Description: Model to be used with the LCS User List view model.
 */

export class CnxLcsUserListModalData implements ICnxLcsUserListModal {
    ContentId: number;
    MediaType: string;
    StatusFor: string;
    ActionType: string;
    SharedWithPeopleView: boolean;
    ActionIcon: string;
}
