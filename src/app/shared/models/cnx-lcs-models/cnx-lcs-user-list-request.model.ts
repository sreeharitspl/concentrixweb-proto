import { CnxPagination } from '../cnx-common-models/cnx-pagination.model';

/**
 * Author: TSPL-T0445
 * Created Date: 13 Sept 2018
 * Class Name: CnxLcsUserListRequest
 * Description: Request model to fetch shared/liked user list.
 */
export class CnxLcsUserListRequest extends CnxPagination {

    ContentId: number;
    MediaType: string;
    StatusFor: string;
}
