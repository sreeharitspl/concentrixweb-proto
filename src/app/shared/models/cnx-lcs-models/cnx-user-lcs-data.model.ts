/**
 * Author: TSPL-T0445
 * Created Date: 11 Sept 2018
 */
import { ICnxDeserialzable } from '../../interfaces/cnx-common-interfaces/cnx-deserialzable';

/**
 * Author: TSPL-T0445
 * Created Date: 11 Sept 2018
 * Class Name: CnxUserLCSData
 * Description: User details for listing users.
 */
export class CnxUserLCSData implements ICnxDeserialzable {

    Department: string;
    Designation: string;
    EmployeeId: string;
    FirstSharedWithName: string;
    FullName: string;
    LikedOn: Date;
    ProfileImageUrl: string;
    SharedContentId: number;
    SharedOn: Date;
    SharedWithCount: number;

    /**
     * Author: TSPL-T0445
     * Created Date: 11 Sept 2018
     * Method Name: deserialize
     * Description: To deserialize the input into the object of CnxUserLCSData
     * @param input : any : JSON data to be deserialized into Class Object
     */
    deserialize(input: any) {
        Object.assign(this, input);
        return this;
      }
}
