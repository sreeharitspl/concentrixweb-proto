import { Component, OnInit, AfterViewInit } from '@angular/core';

import { ApiReportServiceService } from '../api-report-service.service';
import { CnxApiSource } from '../api-source/cnx-api-source';

@Component({
  selector: 'app-api-report',
  templateUrl: './api-report.component.html',
  styleUrls: ['./api-report.component.scss']
})
export class ApiReportComponent implements OnInit, AfterViewInit {

  constructor(public reportService: ApiReportServiceService) { }

  public apiSource: any[];
  public objTrue: number;
  public objFalse: number;

  ngOnInit() {
    this.apiSource = CnxApiSource.CNXAPI;
  }

  ngAfterViewInit() {
    if (this.apiSource) {
      setTimeout(() => {

        for (let index = 0; index < this.apiSource.length; index++) {

          this.checkAPI(this.apiSource[index]);
        }
      }, 2000);
    }
  }
  /*
   * Author: TSPL T0510
   * Function: checkAPI
   * Description: function to check the api response against the schema defined
   * Arguments: Param 1: api : api response we defined
   * Return:nil
   */

  public checkAPI(api) {
    this.reportService.loadAPI(api.source.URL, api.source.Params, (respData: any) => {

      api.status = 200;
      const newObject = {};
      const newObjectType = {};
      const originalResult = respData;
      const expectedResult = api.source.Response;
      this.validateResponse(api, respData);
    },
      (errData: any) => {
        api.status = errData.status;
        alert(errData.statusText);
      });
  }

  /*
  * Author: TSPL T0510
  * Function: validateResponse
  * Description: function to check the api response against the schema defined
  * Arguments: Param 1: api : api response we defined
  * Arguments: Param 2: respData : api response from service
  */
  public validateResponse(api, respData) {
    if (Object.keys(respData).length !== api.source.Response.length) {
      api.message = 'Keys Missing';
    }

    api.source.Response.forEach(item => {

      if (respData.hasOwnProperty(item.key)) {

        if (respData[item.key] && respData[item.key].required && (respData[item.key].required === undefined ||
          respData[item.key].required === null)) {

          item.isValid = false;
          item.message = 'Value Missing';
        } else {
          // Handled with default value
        }
      } else {

        item.isValid = false;
        item.message = 'Key Missing';
      }
      if (item.type === 'Array') {

        for (let i = 0; i < item.arrayObject.length; i++) {

          this.compare(item.arrayObject[i], respData[item.key]);
          if (item.arrayObject[i].isValid === false) {

            item.isValid = false;
          }
        }
      }
      const a = api.source.Response;
      this.objTrue = a.reduce(function (n, count) {
        return n + (count.isValid === true);
      }, 0);
      this.objFalse = a.reduce(function (n, count) {
        return n + (count.isValid === false);
      }, 0);
    });
  }

  /*
  * Author: TSPL T0510
  * Function: compare
  * Description: function to check the api response of arrays or objects
  * Arguments: Param 1: valueA : array
  * Arguments: Param 2: valueB : array
  */
  public compare(valueA, valueB) {

    for (let bIndex = 0; bIndex < valueB.length; bIndex++) {

      if (valueB[bIndex].hasOwnProperty(valueA.key)) {

        valueA.isValid = true;
      } else {
        valueA.isValid = false;
      }
    }
  }
}
