import { TestBed, inject } from '@angular/core/testing';

import { ApiReportServiceService } from './api-report-service.service';

describe('ApiReportServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiReportServiceService]
    });
  });

  it('should be created', inject([ApiReportServiceService], (service: ApiReportServiceService) => {
    expect(service).toBeTruthy();
  }));
});
