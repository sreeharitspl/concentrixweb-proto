import { CnxErrorCode } from './cnx-errorcode';

export class CnxLoginApi {
  /*
  * Author: TSPL T0510
  * Function: CNXLOGIN
  * Description: function to return structure for Validateuser api
  * Return: Validate user return structure
  */

    public static get CNXLOGIN() {
        return {
            LOGIN_API: {
                'URL': 'ValidateUser',
                'Name': 'ValidateUser',
                'Params': {
                    Email: 'concentrix.three@concentrix.com',
                    Password: 'Global@12345',
                    UserName: 'concentrix.three@concentrix.com',
                    DeviceId: 'no_registration_id',
                    DeviceInfo: {},
                    Language: 'en-Us',
                    Latitude: '',
                    Longitude: '',
                    MobileAppVersion: '1.6.14',
                    UserType: 1
                },
                'Response': [
                    {
                        key: 'Acknowledge',
                        type: 'Number',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'CityCode',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'CityName',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'ContactNumber',
                        type: 'Object',
                        required: 0,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'CountryCode',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'CountryName',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'Designation',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'EmployeeEmailId',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'EmployeeId',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'EmployeeName',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'FirstName',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'LastName',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'Location',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'LocationCode',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'LocationName',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'Message',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'ProfileImageUrl',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'Project',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                    {
                        key: 'ErrorCode',
                        type: 'Array',
                        required: 1,
                        isValid: true,
                        message: '',
                        arrayObject: CnxErrorCode.CNXERRORARRAY.ERROR_API
                    },
                    {
                        key: 'Token',
                        type: 'String',
                        required: 1,
                        isValid: true,
                        message: ''
                    },
                ]

            }
        };
    }
}
