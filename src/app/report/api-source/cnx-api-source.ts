import { CnxLoginApi } from './cnx-login-api';


export class CnxApiSource {
  /*
  * Author: TSPL T0510
  * Function: CNXAPI
  * Description: function to return the set of apis in an array format
  * Return: array format for validateuser api
  */

    public static get CNXAPI() {
        return [
            {
                'name': 'ValidateUser',
                'source': CnxLoginApi.CNXLOGIN.LOGIN_API,
                'status': 0,
                'message': ''
            }
        ];
    }
}
