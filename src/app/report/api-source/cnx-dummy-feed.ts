
export class CnxDummyFeed {

    /*
    * Author: TSPL T0475
    * Function: CNXDUMMYFEEDARRAY
    * Description: function to return dummy feed api
    * Return: json format of dummy feed
    */
      public static get CNXDUMMYFEEDARRAY() {
          return {
                Feeds: [
                  {
                    FeedType: 'Video',
                    NewsDetails: null,
                    VideoDetails: {
                      VideoId: 95,
                      VideoParentId: 267,
                      Title: 'Artificial Intelligence combined with Human Intelligence enhances the Customer Experience',
                      ShortDescription: 'Artificial Intelligence combined with Human Intelligence enhances the Customer Experience ',
                      VideoUrl: 'https://youtu.be/wFRXdxCN-hk',
                      // tslint:disable-next-line:max-line-length
                      Thumbnail: 'https://concentrixone.concentrix.com/AdminPortal/CnxonedataProd/MediaAssets/Communications/Video/VideoURLMar18201820554132115402564.png',
                      ExpiryDate: '7/31/2018 12:00:00 AM',
                      PublisherName: 'Communication Administrator',
                      PostedOn: '3/19/2018 12:54:51 AM',
                      IsRead: true,
                      LikesCount: 7,
                      CommentsCount: 1,
                      AllowShare: false,
                      OnBehalfOf: null,
                      EnableComments: true,
                      EnableLikes: false,
                      LikedStatus: true,
                      SharedCount: 50,
                      SharedWithCount: 100,
                      IsShared: true,
                      SharedInfo: {
                        SharedByEmployeeId: '5003641',
                        SharedByName: 'Madhan M',
                        ProfileImageUrl: '',
                        SharedOn: '04/13/2018 6:42:21 AM'
                      }
                    },
                    ImageDetails: null,
                    URLDetails: null
                   },
                  {
                    FeedType: 'Image',
                    NewsDetails: null,
                    VideoDetails: null,
                    ImageDetails: {
                      ContentId: 95,
                      ContentParentId: 267,
                      Title: 'Artificial Intelligence combined with Human Intelligence enhances the Customer Experience',
                      ShortDescription: 'Artificial Intelligence combined with Human Intelligence enhances the Customer Experience ',
                      Images: [
                        {
                          Image: 'https://concentrixone.concentrix.com/AdminPortal/CnxonedataProd/MediaAssets/Communications/Video/VideoURLMar18201820554132115402564.png'
                        }
                      ],
                      PostedBy: 'Madhan M',
                      PostedByEmployeeId: '5003641',
                      PostedByProfileImageUrl: '',
                      PostedOn: '3/19/2018 12:54:51 AM',
                      IsRead: true,
                      LikesCount: 7,
                      CommentsCount: 1,
                      AllowShare: false,
                      EnableComments: true,
                      EnableLikes: false,
                      LikedStatus: true,
                      SharedCount: 50,
                      SharedWithCount: 100,
                      IsShared: true,
                      SharedInfo: {
                        SharedByEmployeeId: '5003644',
                        SharedByName: 'Nanda R',
                        ProfileImageUrl: '',
                        SharedOn: '04/13/2018 6:42:21 AM'
                      }
                    },
                    URLDetails: null
                  },
                  {
                    FeedType: 'URL',
                    NewsDetails: null,
                    VideoDetails: null,
                    ImageDetails: null,
                    URLDetails: {
                      ContentId: 95,
                      ContentParentId: 267,
                      Title: 'Artificial Intelligence combined with Human Intelligence enhances the Customer Experience',
                      ShortDescription: 'Artificial Intelligence combined with Human Intelligence enhances the Customer Experience ',
                      SharedURL: 'https://concentrixone.concentrix.com/MobileService/ConcentrixOne/IamTheSharedURL',
                      URLMetaData: {
                        Title: 'Google',
                        // tslint:disable-next-line:max-line-length
                        Description: 'Search the world\'s information, including webpages, images, videos and more. Google has many special features to help you find exactly what you\'re looking for.',
                        Image: 'http://www.google.com/images/branding/googlelogo/1x/googlelogo_white_background_color_272x92dp.png',
                        Url: 'https://www.google.com/'
                      },
                      PostedBy: 'Madhan M',
                      PostedByEmployeeId: '5003641',
                      PostedByProfileImageUrl: '',
                      PostedOn: '3/19/2018 12:54:51 AM',
                      IsRead: true,
                      LikesCount: 7,
                      CommentsCount: 1,
                      AllowShare: false,
                      EnableComments: true,
                      EnableLikes: false,
                      LikedStatus: true,
                      SharedCount: 50,
                      SharedWithCount: 100,
                      IsShared: true,
                      SharedInfo: {
                        SharedByEmployeeId: '5003644',
                        SharedByName: 'Nanda R',
                        ProfileImageUrl: '',
                        SharedOn: '04/13/2018 6:42:21 AM'
                      }
                    }
                  },
                  {
                    FeedType: 'Alert',
                    NewsDetails: null,
                    VideoDetails: null,
                    AlertDetails: {
                      SenderId: 'PIntComm',
                      SenderName: 'Internal Communication',
                      MessageDeliveryId: 8089589,
                      MessageTypeId: 3,
                      MessageType: 'Push Notifications',
                      MessageGroupId: 1,
                      Subject: 'Reporting of High Severity Incidents',
                      MessageContent: 'Reporting of High Severity Incidents',
                      MessageDate: '10/10/2018 11:40:01 AM',
                      IsMessageRead: true,
                      IsFileAttached: false,
                      Attachments: []
                    },
                    CheckInDetails: null,
                    ImageDetails: null,
                    URLDetails: null,
                    PollDetails: null,
                    QuizDetails: null
                  }
                ],
                Acknowledge: 1,
                Message: 'Success',
                // tslint:disable-next-line:no-unused-expression
                ErrorCode: [
                  {
                    Code: '100000',
                    Description: 'Success'
                  }
                ]
        };
    }
}
