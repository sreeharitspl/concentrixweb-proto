
export class CnxErrorCode {

  /*
  * Author: TSPL T0510
  * Function: CNXERRORARRAY
  * Description: function to return Error code keys in api
  * Return: array format for errorcode object
  */
    public static get CNXERRORARRAY() {
        return {
            ERROR_API: [
                {
                    key: 'Codes',
                    type: 'String',
                    required: 1,
                    isValid: true,
                    message: ''
                },
                {
                    key: 'Description',
                    type: 'String',
                    required: 1,
                    isValid: true,
                    message: ''
                }
            ]
        };
    }
}
