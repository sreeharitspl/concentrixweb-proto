import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ApiReportComponent } from './api-report/api-report.component';


const routes: Routes = [
    { path: '', component: ApiReportComponent, pathMatch: 'full' },
  ];

  @NgModule({
    imports: [ RouterModule.forChild(routes) ]
  })

  export class ReportRoutingModule {}
