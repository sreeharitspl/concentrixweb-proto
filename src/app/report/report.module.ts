import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { ApiReportComponent } from './api-report/api-report.component';
import { ReportRoutingModule } from './report.routing';

@NgModule({
  imports: [
    CommonModule,
    ReportRoutingModule,
    AccordionModule.forRoot(),
  ],
  declarations: [ApiReportComponent]
})
export class ReportModule { }
