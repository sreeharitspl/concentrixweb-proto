import { Injectable } from '@angular/core';
import { HttpService } from '../shared/service/http/http.service';

@Injectable({
  providedIn: 'root'
})
export class ApiReportServiceService {

  constructor(public httpService: HttpService) { }

  /*
   * Author: T0510
   * Function: loadAPI
   * Description:  This function used to Validate api response
   * Arguments:
   *     Param 1: url        : string
   *     Param 2: inputs     : object
   *     Param 3: cbSuccess  : success callback
   *     Param 4: cbError    : error callback
   *     param 5: showLoader : Boolean
   */
  public loadAPI(url, params, cbSuccess, cbError, showLoader?) {
    this.httpService.postX(url, params)
      .subscribe(result => {
        cbSuccess(result);
      },
        error => cbError(error)
      );
  }
}
