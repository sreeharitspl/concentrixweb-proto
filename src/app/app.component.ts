import { Component } from '@angular/core';
import { CnxLanguageService } from './shared/service/cnx-language/cnx-language.service';
import { CnxLocalStorageService } from './shared/service/cnx-local-storage/cnx-local-storage.service';
import { CnxConstants } from './shared/modules/cnx-utils/cnx-constants/cnx-constants';
import { CnxGlobalService } from './shared/modules/cnx-utils/cnx-global/cnx-global.service';
import { CnxLogin } from './shared/modules/cnx-utils/cnx-datasource/cnx-login';
import { CnxLanguageApiService } from './shared/modules/cnx-language/cnx-language-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  testModel: {ContentId: number,  EnableLikes: boolean, LikesCount: number,
    LikedStatus: boolean, AllowShare: boolean, SharedCount: number, FeedType: string}
          = {ContentId: 343, EnableLikes: true, LikesCount: 10, LikedStatus:
            true, AllowShare: true, SharedCount: 2, FeedType: 'Article'};
  // Boolean to check whether the language source have been fetched from server
  public isLanguageLoaded = false;
  public showLanguageSpinner = true;

  constructor(public cnxLanguageService: CnxLanguageService, private localStorage: CnxLocalStorageService,
              public cnxGlobalService: CnxGlobalService, private languageApiService: CnxLanguageApiService) {
    const selectedLanguage = this.localStorage.getItem(CnxConstants.LOCALSTORAGEKEYS.CNX_SELECTED_LANGUAGE);

    this.getDictionary(selectedLanguage);

    if (!selectedLanguage) {
      const fetchLanguageDirectory = true;
      this.getLanguageList(fetchLanguageDirectory);
    } else {
      const fetchLanguageDirectory = false;
      this.getLanguageList(fetchLanguageDirectory);
      this.getLanguageDirectory(selectedLanguage);
    }
  }

  /*
   * Author: T0423
   * Function: getLanguageList
   * Description:  Function to fetch all the avalable language list
   * Arguments: Nil
   * Return: Nil
   */
  public getLanguageList(fetchLanguageDirectory) {
    const params = {};
    this.languageApiService.getAllLanguages('GetResourceLanguages', params, (respData: any) => {

      if (fetchLanguageDirectory) {
        this.getLanguageDirectory(CnxConstants.APPLICATIONCONSTANTS.CNX_DEFAULT_LANGUAGE);
      }
    },
      (errData: any) => {
        alert(errData.statusText);
      }, this.showLanguageSpinner);
  }

  /*
   * Author: T0423
   * Function: getLanguageDirectory
   * Description:  Function to fetch the language directory of selected language
   * Arguments: Nil
   * Return: Nil
   */
  public getLanguageDirectory(selectedLanguage) {
    const params = { 'Language': selectedLanguage };
    this.languageApiService.getLanguageDictionary('GetResource', params, (respData: any) => {
      this.isLanguageLoaded = true;
    },
      (errData: any) => {
        alert(errData.statusText);
      }, this.showLanguageSpinner);
  }
  /*
   * Author: T0510
   * Function: getDictionary
   * Description:  Function to get the dictionary from Local storage
   * Arguments: selectedLanguage
   * Return: Nil
   */
  public getDictionary(selectedLanguage) {
    let languageDictionary = this.localStorage.getItem(CnxConstants.LOCALSTORAGEKEYS.CNX_LOGIN_TRANSLATION);
    // This sets the dictionary from the local storage if it is present in the local storage
    if (languageDictionary) {
      languageDictionary = JSON.parse(languageDictionary);
      this.setLanguageDictionary(languageDictionary);
    } else if (!selectedLanguage) {
      // This sets the dictionary for the initial load time with default english values
      this.setLanguageDictionary(CnxLogin.LOGINDATAKEYS);
    }
  }

  /*
   * Author: T0510
   * Function: setLanguageDictionary
   * Description:  Function to set the dictionary from either local storage or from local file.
   * Arguments: dictionary
   * Return: Nil
   */
  public setLanguageDictionary(dictionary) {
    this.cnxLanguageService.languageDictionary = dictionary;
    this.showLanguageSpinner = false;
    this.cnxGlobalService.hideLoader();
  }
}
