import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ModalModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { EmbedVideo } from 'ngx-embed-video';

import { AppComponent } from './app.component';
import { CnxTimeLineModule } from './core/modules/cnx-time-line/cnx-time-line.module';
import { AppRoutingModule } from './app.routing';
import { AuthModule } from './auth/auth.module';
import { SharedModule } from './shared/shared.module';
import { CnxDashboardModule } from './core/modules/cnx-dashboard/cnx-dashboard.module';
import { CnxPostLCSModule } from './shared/modules/cnx-post-lcs/cnx-post-lcs.module';

import { CnxHttpInterceptor } from './cnx-http-interceptor';
import { CnxLanguageService } from './shared/service/cnx-language/cnx-language.service';
import { CnxLocalStorageService } from './shared/service/cnx-local-storage/cnx-local-storage.service';
import { CnxGlobalService } from './shared/modules/cnx-utils/cnx-global/cnx-global.service';
import { CnxToastrService } from './shared/service/cnx-toastr/cnx-toastr.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CnxTimeLineModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    AuthModule,
    AppRoutingModule,
    SharedModule,
    CnxDashboardModule,
    InfiniteScrollModule,
    CnxPostLCSModule,
    ModalModule.forRoot(),
    ToastrModule.forRoot(),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CnxHttpInterceptor,
      multi: true
    },
    CnxLanguageService,
    CnxGlobalService,
    CnxLocalStorageService,
    CnxToastrService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
