import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxContactUsRoutingModule } from './cnx-contactus-routing';

@NgModule({
  imports: [
    CommonModule,
    CnxContactUsRoutingModule
  ],
  declarations: []
})
export class CnxContactusModule { }
