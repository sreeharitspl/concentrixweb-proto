import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxContactusComponent } from './cnx-contactus.component';

describe('CnxContactusComponent', () => {
  let component: CnxContactusComponent;
  let fixture: ComponentFixture<CnxContactusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxContactusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxContactusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
