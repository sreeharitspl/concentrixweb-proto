/*
* Created By : TSPL-T0475
* Date       : 19-Sept-2018
* Purpose    : Kudos page routing defenition.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CnxContactusComponent } from './cnx-contactus/cnx-contactus.component';

const routes: Routes = [
  { path: 'contactus', component: CnxContactusComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CnxContactusComponent
  ]
})

export class CnxContactUsRoutingModule { }
