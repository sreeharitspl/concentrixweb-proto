import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxMyTeamComponent } from './cnx-my-team.component';

describe('CnxMyTeamComponent', () => {
  let component: CnxMyTeamComponent;
  let fixture: ComponentFixture<CnxMyTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxMyTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxMyTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
