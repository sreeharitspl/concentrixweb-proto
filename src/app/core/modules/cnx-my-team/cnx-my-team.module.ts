import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxMyTeamRoutingModule } from './cnx-my-team-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxMyTeamRoutingModule
  ],
})
export class CnxMyTeamModule { }
