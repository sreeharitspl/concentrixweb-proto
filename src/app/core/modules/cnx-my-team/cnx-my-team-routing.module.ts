import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CnxMyTeamComponent } from './cnx-my-team/cnx-my-team.component';

const routes: Routes = [{ path: 'myteam', component: CnxMyTeamComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxMyTeamComponent]
})
export class CnxMyTeamRoutingModule { }
