import { CnxIncidentReportingModule } from './cnx-incident-reporting.module';

describe('CnxIncidentReportingModule', () => {
  let cnxIncidentReportingModule: CnxIncidentReportingModule;

  beforeEach(() => {
    cnxIncidentReportingModule = new CnxIncidentReportingModule();
  });

  it('should create an instance', () => {
    expect(cnxIncidentReportingModule).toBeTruthy();
  });
});
