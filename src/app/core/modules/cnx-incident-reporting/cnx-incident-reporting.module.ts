import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxIncidentReportingRoutingModule } from './cnx-incident-reporting-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxIncidentReportingRoutingModule
  ],
  declarations: []
})
export class CnxIncidentReportingModule { }
