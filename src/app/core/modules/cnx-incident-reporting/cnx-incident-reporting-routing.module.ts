import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxIncidentReportingComponent } from './cnx-incident-reporting/cnx-incident-reporting.component';

const routes: Routes = [{ path: 'incidentreporting', component: CnxIncidentReportingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxIncidentReportingComponent]
})
export class CnxIncidentReportingRoutingModule { }
