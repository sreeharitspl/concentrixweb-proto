import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxIncidentReportingComponent } from './cnx-incident-reporting.component';

describe('CnxIncidentReportingComponent', () => {
  let component: CnxIncidentReportingComponent;
  let fixture: ComponentFixture<CnxIncidentReportingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxIncidentReportingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxIncidentReportingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
