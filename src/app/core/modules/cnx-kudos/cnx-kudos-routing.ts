/*
* Created By : TSPL-T0423
* Date       : 19-Sept-2018
* Purpose    : Kudos page routing defenition.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CnxKudosComponent } from './cnx-kudos/cnx-kudos.component';

const routes: Routes = [
  { path: 'kudos', component: CnxKudosComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CnxKudosComponent
  ]
})

export class CnxKudosRoutingModule { }
