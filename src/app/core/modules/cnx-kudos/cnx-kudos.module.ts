import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxKudosRoutingModule } from './cnx-kudos-routing';

@NgModule({
  imports: [
    CommonModule,
    CnxKudosRoutingModule
  ],
  declarations: []
})
export class CnxKudosModule { }
