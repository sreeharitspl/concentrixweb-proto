import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cnx-kudos',
  templateUrl: './cnx-kudos.component.html',
  styleUrls: ['./cnx-kudos.component.scss']
})
export class CnxKudosComponent implements OnInit {
  public viewMode: String;
  constructor() {
  }

  ngOnInit() {
    this.viewMode = 'tab1';
  }

}
