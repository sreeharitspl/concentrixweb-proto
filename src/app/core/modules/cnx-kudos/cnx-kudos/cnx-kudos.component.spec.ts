import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxKudosComponent } from './cnx-kudos.component';

describe('CnxKudosComponent', () => {
  let component: CnxKudosComponent;
  let fixture: ComponentFixture<CnxKudosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxKudosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxKudosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
