import { CnxCultureRoutingModule } from './cnx-culture-routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [
    CommonModule,
    CnxCultureRoutingModule
  ],
  declarations: []
})
export class CnxCultureModule { }
