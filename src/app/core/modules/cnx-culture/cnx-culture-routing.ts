/*
* Created By : TSPL-T0545
* Date       : 22-Nov-2018
* Purpose    : Culture page routing defenition.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CnxCultureComponent } from './cnx-culture/cnx-culture.component';


const routes: Routes = [
  { path: 'culture', component: CnxCultureComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CnxCultureComponent
  ]
})

export class CnxCultureRoutingModule { }
