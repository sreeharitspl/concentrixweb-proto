import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxCultureComponent } from './cnx-culture.component';

describe('CnxCultureComponent', () => {
  let component: CnxCultureComponent;
  let fixture: ComponentFixture<CnxCultureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxCultureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxCultureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
