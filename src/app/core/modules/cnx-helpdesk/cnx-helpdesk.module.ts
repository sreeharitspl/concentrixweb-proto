import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxHelpdeskRoutingModule } from './cnx-helpdesk-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxHelpdeskRoutingModule
  ],
  declarations: []
})
export class CnxHelpdeskModule { }
