import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxHelpdeskComponent } from './cnx-helpdesk.component';

describe('CnxHelpdeskComponent', () => {
  let component: CnxHelpdeskComponent;
  let fixture: ComponentFixture<CnxHelpdeskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxHelpdeskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxHelpdeskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
