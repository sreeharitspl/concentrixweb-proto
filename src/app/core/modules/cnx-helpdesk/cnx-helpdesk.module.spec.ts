import { CnxHelpdeskModule } from './cnx-helpdesk.module';

describe('CnxHelpdeskModule', () => {
  let cnxHelpdeskModule: CnxHelpdeskModule;

  beforeEach(() => {
    cnxHelpdeskModule = new CnxHelpdeskModule();
  });

  it('should create an instance', () => {
    expect(cnxHelpdeskModule).toBeTruthy();
  });
});
