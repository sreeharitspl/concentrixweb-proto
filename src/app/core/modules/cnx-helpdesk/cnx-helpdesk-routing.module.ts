import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxHelpdeskComponent } from './cnx-helpdesk/cnx-helpdesk.component';

const routes: Routes = [{ path: 'helpdesk', component: CnxHelpdeskComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxHelpdeskComponent]
})
export class CnxHelpdeskRoutingModule { }
