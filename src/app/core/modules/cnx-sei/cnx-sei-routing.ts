/*
* Created By : TSPL-T0423
* Date       : 19-Sept-2018
* Purpose    : Kudos page routing defenition.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CnxSeiComponent } from './cnx-sei/cnx-sei.component';

const routes: Routes = [
  { path: 'sei', component: CnxSeiComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CnxSeiComponent
  ]
})

export class CnxSEIRoutingModule { }
