import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxSEIRoutingModule } from './cnx-sei-routing';

@NgModule({
  imports: [
    CommonModule,
    CnxSEIRoutingModule
  ],
  declarations: []
})
export class CnxSeiModule { }
