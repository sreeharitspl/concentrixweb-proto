import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxSeiComponent } from './cnx-sei.component';

describe('CnxSeiComponent', () => {
  let component: CnxSeiComponent;
  let fixture: ComponentFixture<CnxSeiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxSeiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxSeiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
