import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxTrainingComponent } from './cnx-training/cnx-training.component';

const routes: Routes = [{ path: 'training', component: CnxTrainingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxTrainingComponent]
})
export class CnxTrainingRoutingModule { }
