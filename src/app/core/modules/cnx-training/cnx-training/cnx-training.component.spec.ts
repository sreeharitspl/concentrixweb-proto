import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxTrainingComponent } from './cnx-training.component';

describe('CnxTrainingComponent', () => {
  let component: CnxTrainingComponent;
  let fixture: ComponentFixture<CnxTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
