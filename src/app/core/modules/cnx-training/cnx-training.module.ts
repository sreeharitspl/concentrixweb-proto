import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxTrainingRoutingModule } from './cnx-training-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxTrainingRoutingModule
  ],
  declarations: []
})
export class CnxTrainingModule { }
