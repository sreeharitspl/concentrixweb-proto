import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxPrivacyPolicyRoutingModule } from './cnx-privacy-policy-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxPrivacyPolicyRoutingModule
  ],
  declarations: []
})
export class CnxPrivacyPolicyModule { }
