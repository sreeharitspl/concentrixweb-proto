import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxPrivacyPolicyComponent } from './cnx-privacy-policy/cnx-privacy-policy.component';

const routes: Routes = [{ path: 'privacypolicy', component: CnxPrivacyPolicyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxPrivacyPolicyComponent]
})
export class CnxPrivacyPolicyRoutingModule { }
