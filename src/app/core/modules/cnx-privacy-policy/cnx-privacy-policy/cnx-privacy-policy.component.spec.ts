import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxPrivacyPolicyComponent } from './cnx-privacy-policy.component';

describe('CnxPrivacyPolicyComponent', () => {
  let component: CnxPrivacyPolicyComponent;
  let fixture: ComponentFixture<CnxPrivacyPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxPrivacyPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxPrivacyPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
