import { CnxPrivacyPolicyModule } from './cnx-privacy-policy.module';

describe('CnxPrivacyPolicyModule', () => {
  let cnxPrivacyPolicyModule: CnxPrivacyPolicyModule;

  beforeEach(() => {
    cnxPrivacyPolicyModule = new CnxPrivacyPolicyModule();
  });

  it('should create an instance', () => {
    expect(cnxPrivacyPolicyModule).toBeTruthy();
  });
});
