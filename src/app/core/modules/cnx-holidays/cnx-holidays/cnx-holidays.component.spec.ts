import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxHolidaysComponent } from './cnx-holidays.component';

describe('CnxHolidaysComponent', () => {
  let component: CnxHolidaysComponent;
  let fixture: ComponentFixture<CnxHolidaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxHolidaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxHolidaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
