import { CnxHolidaysModule } from './cnx-holidays.module';

describe('CnxHolidaysModule', () => {
  let cnxHolidaysModule: CnxHolidaysModule;

  beforeEach(() => {
    cnxHolidaysModule = new CnxHolidaysModule();
  });

  it('should create an instance', () => {
    expect(cnxHolidaysModule).toBeTruthy();
  });
});
