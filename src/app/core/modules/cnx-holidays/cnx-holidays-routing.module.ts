import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxHolidaysComponent } from './cnx-holidays/cnx-holidays.component';

const routes: Routes = [{ path: 'holidays', component: CnxHolidaysComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxHolidaysComponent]
})
export class CnxHolidaysRoutingModule { }
