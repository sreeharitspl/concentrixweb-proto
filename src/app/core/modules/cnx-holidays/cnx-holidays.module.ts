import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxHolidaysRoutingModule } from './cnx-holidays-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxHolidaysRoutingModule
  ],
  declarations: []
})
export class CnxHolidaysModule { }
