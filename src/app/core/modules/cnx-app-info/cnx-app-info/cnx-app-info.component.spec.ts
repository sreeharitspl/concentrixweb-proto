import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxAppInfoComponent } from './cnx-app-info.component';

describe('CnxAppInfoComponent', () => {
  let component: CnxAppInfoComponent;
  let fixture: ComponentFixture<CnxAppInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxAppInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxAppInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
