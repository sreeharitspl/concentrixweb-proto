import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxAppInfoRoutingModule } from './cnx-app-info-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxAppInfoRoutingModule
  ],
  declarations: []
})
export class CnxAppInfoModule { }
