import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxAppInfoComponent } from './cnx-app-info/cnx-app-info.component';

const routes: Routes = [{ path: 'appinfo', component: CnxAppInfoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxAppInfoComponent]
})
export class CnxAppInfoRoutingModule { }
