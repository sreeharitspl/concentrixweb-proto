import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxSharedContentRoutingModule } from './cnx-shared-content-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxSharedContentRoutingModule
  ],
  declarations: []
})
export class CnxSharedContentModule { }
