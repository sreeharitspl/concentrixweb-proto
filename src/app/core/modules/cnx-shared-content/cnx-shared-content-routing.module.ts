import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxSharedContentComponent } from './cnx-shared-content/cnx-shared-content.component';

const routes: Routes = [{ path: 'sharedcontent', component: CnxSharedContentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxSharedContentComponent]
})
export class CnxSharedContentRoutingModule { }
