import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxSharedContentComponent } from './cnx-shared-content.component';

describe('CnxSharedContentComponent', () => {
  let component: CnxSharedContentComponent;
  let fixture: ComponentFixture<CnxSharedContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxSharedContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxSharedContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
