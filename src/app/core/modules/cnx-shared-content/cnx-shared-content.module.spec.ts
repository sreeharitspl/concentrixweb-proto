import { CnxSharedContentModule } from './cnx-shared-content.module';

describe('CnxSharedContentModule', () => {
  let cnxSharedContentModule: CnxSharedContentModule;

  beforeEach(() => {
    cnxSharedContentModule = new CnxSharedContentModule();
  });

  it('should create an instance', () => {
    expect(cnxSharedContentModule).toBeTruthy();
  });
});
