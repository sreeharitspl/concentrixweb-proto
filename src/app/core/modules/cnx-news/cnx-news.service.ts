import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared/service/http/http.service';
import { CnxGlobalService } from '../../../shared/modules/cnx-utils/cnx-global/cnx-global.service';
import { CnxMapFeeds } from '../../../shared/models/cnx-post-models/cnx-map-feeds';
import { CnxNewsModel } from '../../../shared/models/cnx-post-models/cnx-news-model';
import { CnxConstants } from '../../../shared/modules/cnx-utils/cnx-constants/cnx-constants';

@Injectable({
  providedIn: 'root'
})
export class CnxNewsService {

  constructor(
    private httpService: HttpService,
    private cnxGlobalService: CnxGlobalService
  ) { }

  /*
   * Author: T0475
   * Function: getNewsList
   * Description:  to get news list
   */
  public getNewsList(url, params, cbSuccess, cbError, showLoader?) {
    if (showLoader) {
      this.cnxGlobalService.showLoader();
    }
    this.httpService.postX(url, params)
    .subscribe(result => {
      result = this.mapNews(result);
      cbSuccess(result);
    },
        error => cbError(error)
      );
  }
   private mapNews(result) {
    result.NewsData.forEach((data, index) => {
        data = new CnxNewsModel().mapNewsData(data, data.FeedType);
    });
    return  result;
   }
}
