import { Component, OnInit } from '@angular/core';
import { CnxNewsService } from '../cnx-news.service';
import { CnxConstants } from '../../../../shared/modules/cnx-utils/cnx-constants/cnx-constants';

@Component({
  selector: 'app-cnx-news',
  templateUrl: './cnx-news.component.html',
  styleUrls: ['./cnx-news.component.scss']
})
export class CnxNewsComponent implements OnInit {
  /* Infinit scroll properties */
  invokedStatus = true;
  throttle: number;
  scrollDistance: number;
  scrollUpDistance: number;
  direction = '';
  modalOpen = false;
  newsData = [];
  /* Ends */
  public feedType: string;

  public pageNumber: number;
  /* input object defining */
  inputs: {
    PerPage: number;
    PageNumber: number;
  };
  constructor(public newslistservice: CnxNewsService) { }

  ngOnInit() {
    this.throttle = CnxConstants.MODALSCROLLPARAMETERS.Throttle;
    this.scrollDistance = CnxConstants.MODALSCROLLPARAMETERS.ScrollDistance;
    this.scrollUpDistance = CnxConstants.MODALSCROLLPARAMETERS.ScrollUpDistance;
    this.pageNumber = 0;
    this.feedType = CnxConstants.CNX_FEEDTYPEKEY['CNC_NEWS'];
    this.getNewsList();
  }
  /*
   * Author: T0475
   * Function: getNewsList
   * Description:  to get news list
   * Arguments: Nil
   * Return: Nil
   */
  public getNewsList() {
    if (this.invokedStatus) {
      this.invokedStatus = false;
      this.inputs = {
        PerPage: CnxConstants.PAGINATIONLENGTH.CNX_PAGINATION_COUNT,
        PageNumber: this.pageNumber += 1
      };
      const showLoader = false;
      this.newslistservice.getNewsList('GetEmployeeNews', this.inputs, (respData: any) => {
        if (respData.Acknowledge === 1) {
          // if (respData.Feeds && respData.Feeds.length < CnxConstants.PAGINATIONLENGTH.CNX_PAGINATION_COUNT) {
          //   this.invokedStatus = false;
          // } else {
          //   this.invokedStatus = true;
          // }
          this.setNewsData(respData.NewsData);
        } else {
          alert(respData.Message);
        }
      },
        (errData: any) => {
          this.invokedStatus = true;
          console.log('Something went wrong');
        }, showLoader);
    }
  }

  /*
    * Author: T0475
    * Function: setNewsData
    * Description:  This function used to append news data
    * Arguments:
    *     Arg 1: feed data : array
    */
   setNewsData (newsData) {
    this.newsData = this.newsData.concat(newsData);
    console.log('this.newsData', this.newsData);
  }
}
