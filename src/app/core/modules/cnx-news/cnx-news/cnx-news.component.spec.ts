import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxNewsComponent } from './cnx-news.component';

describe('CnxNewsComponent', () => {
  let component: CnxNewsComponent;
  let fixture: ComponentFixture<CnxNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
