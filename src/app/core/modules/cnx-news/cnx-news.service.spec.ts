import { TestBed, inject } from '@angular/core/testing';

import { CnxNewsService } from './cnx-news.service';

describe('CnxNewsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxNewsService]
    });
  });

  it('should be created', inject([CnxNewsService], (service: CnxNewsService) => {
    expect(service).toBeTruthy();
  }));
});
