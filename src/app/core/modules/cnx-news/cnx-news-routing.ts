/*
* Created By : TSPL-T0475
* Date       : 13-Nov-2018
* Purpose    : News page routing definition.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CnxNewsComponent } from './cnx-news/cnx-news.component';
import { NewsContainerModule } from '../../../shared/modules/cnx-post/news-container/news-container.module';
import { CnxPostLCSModule } from '../../../shared/modules/cnx-post-lcs/cnx-post-lcs.module';

const routes: Routes = [
  { path: 'news', component: CnxNewsComponent }
];

@NgModule({
  imports: [
    CommonModule,
    NewsContainerModule,
    RouterModule.forChild(routes),
    CnxPostLCSModule
  ],
  declarations: [
    CnxNewsComponent
  ]
})

export class CnxNewsRoutingModule { }
