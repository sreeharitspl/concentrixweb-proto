import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxNewsComponent } from './cnx-news/cnx-news.component';
import { CnxNewsRoutingModule } from './cnx-news-routing';
import { NewsContainerModule } from '../../../shared/modules/cnx-post/news-container/news-container.module';
import { CnxPostLCSModule } from '../../../shared/modules/cnx-post-lcs/cnx-post-lcs.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    CommonModule,
    InfiniteScrollModule,
    CnxNewsRoutingModule,
    NewsContainerModule,
    CnxPostLCSModule
  ],
  declarations: []
})
export class CnxNewsModule { }
