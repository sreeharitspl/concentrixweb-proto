import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxBenefitsRoutingModule } from './cnx-benefits-routing.module';
import { SharedModule } from '../../../shared/shared.module';
import { TranslatePipe } from '../../../shared/pipes/translate/translate.pipe';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CnxBenefitsRoutingModule
  ],
})
export class CnxBenefitsModule { }
