import { Component, OnInit } from '@angular/core';
import { CnxLanguageService } from '../../../../shared/service/cnx-language/cnx-language.service';

@Component({
  selector: 'app-cnx-benefits',
  templateUrl: './cnx-benefits.component.html',
  styleUrls: ['./cnx-benefits.component.scss']
})
export class CnxBenefitsComponent implements OnInit {

  constructor(public cnxLanguageService: CnxLanguageService) { }

  ngOnInit() {
  }

}
