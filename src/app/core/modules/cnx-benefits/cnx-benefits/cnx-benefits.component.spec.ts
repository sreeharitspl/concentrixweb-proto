import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxBenefitsComponent } from './cnx-benefits.component';

describe('CnxBenefitsComponent', () => {
  let component: CnxBenefitsComponent;
  let fixture: ComponentFixture<CnxBenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxBenefitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
