import { CnxBenefitsModule } from './cnx-benefits.module';

describe('CnxBenefitsModule', () => {
  let cnxBenefitsModule: CnxBenefitsModule;

  beforeEach(() => {
    cnxBenefitsModule = new CnxBenefitsModule();
  });

  it('should create an instance', () => {
    expect(cnxBenefitsModule).toBeTruthy();
  });
});
