import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CnxBenefitsComponent } from './cnx-benefits/cnx-benefits.component';
import { SharedModule } from '../../../shared/shared.module';

const routes: Routes = [{ path: 'benefits', component: CnxBenefitsComponent }];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ],
  exports: [RouterModule],
  declarations: [CnxBenefitsComponent]
})
export class CnxBenefitsRoutingModule { }
