import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxTermsOfUseRoutingModule } from './cnx-terms-of-use-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxTermsOfUseRoutingModule
  ],
  declarations: []
})
export class CnxTermsOfUseModule { }
