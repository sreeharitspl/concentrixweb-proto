import { CnxTermsOfUseModule } from './cnx-terms-of-use.module';

describe('CnxTermsOfUseModule', () => {
  let cnxTermsOfUseModule: CnxTermsOfUseModule;

  beforeEach(() => {
    cnxTermsOfUseModule = new CnxTermsOfUseModule();
  });

  it('should create an instance', () => {
    expect(cnxTermsOfUseModule).toBeTruthy();
  });
});
