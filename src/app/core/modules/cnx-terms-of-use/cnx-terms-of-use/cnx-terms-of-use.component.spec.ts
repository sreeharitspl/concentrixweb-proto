import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxTermsOfUseComponent } from './cnx-terms-of-use.component';

describe('CnxTermsOfUseComponent', () => {
  let component: CnxTermsOfUseComponent;
  let fixture: ComponentFixture<CnxTermsOfUseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxTermsOfUseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxTermsOfUseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
