import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxTermsOfUseComponent } from './cnx-terms-of-use/cnx-terms-of-use.component';

const routes: Routes = [{ path: 'termsofuse', component: CnxTermsOfUseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxTermsOfUseComponent]
})
export class CnxTermsOfUseRoutingModule { }
