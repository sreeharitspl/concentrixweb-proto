import { CnxOperatingphilosophyRoutingModule } from './cnx-operatingphilosophy-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    CnxOperatingphilosophyRoutingModule
  ],
  declarations: []
})
export class CnxOperatingphilosophyModule { }
