import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxOperatingphilosophyComponent } from './cnx-operatingphilosophy.component';

describe('CnxOperatingphilosophyComponent', () => {
  let component: CnxOperatingphilosophyComponent;
  let fixture: ComponentFixture<CnxOperatingphilosophyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxOperatingphilosophyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxOperatingphilosophyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
