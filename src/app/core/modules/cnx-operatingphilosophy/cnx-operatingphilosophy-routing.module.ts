import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxOperatingphilosophyComponent } from './cnx-operatingphilosophy/cnx-operatingphilosophy.component';

const routes: Routes = [{ path: 'operatingphilosophy', component: CnxOperatingphilosophyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxOperatingphilosophyComponent]
})
export class CnxOperatingphilosophyRoutingModule { }
