import { CnxOperatingphilosophyModule } from './cnx-operatingphilosophy.module';

describe('CnxOperatingphilosophyModule', () => {
  let cnxOperatingphilosophyModule: CnxOperatingphilosophyModule;

  beforeEach(() => {
    cnxOperatingphilosophyModule = new CnxOperatingphilosophyModule();
  });

  it('should create an instance', () => {
    expect(cnxOperatingphilosophyModule).toBeTruthy();
  });
});
