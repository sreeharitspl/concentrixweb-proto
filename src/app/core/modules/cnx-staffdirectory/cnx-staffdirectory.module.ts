import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxStaffdirectoryRoutingModule } from './cnx-staffdirectory-routing';
import { CnxSearchModule } from '../../../shared/modules/cnx-search/cnx-search.module';
import { CnxUserImageModule } from '../../../shared/modules/cnx-user-image/cnx-user-image.module';

@NgModule({
  imports: [
    CommonModule,
    CnxStaffdirectoryRoutingModule,
    CnxSearchModule
  ],
  declarations: []
})
export class CnxStaffdirectoryModule { }
