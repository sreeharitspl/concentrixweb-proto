/*
* Created By : TSPL-T0423
* Date       : 19-Sept-2018
* Purpose    : Kudos page routing defenition.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CnxStaffdirectoryComponent } from './cnx-staffdirectory/cnx-staffdirectory.component';
import { CnxSearchModule } from '../../../shared/modules/cnx-search/cnx-search.module';
import { CnxUserImageModule } from '../../../shared/modules/cnx-user-image/cnx-user-image.module';

const routes: Routes = [
  { path: 'staffdirectory', component: CnxStaffdirectoryComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CnxSearchModule,
    CnxUserImageModule
  ],
  declarations: [
    CnxStaffdirectoryComponent
  ]
})

export class CnxStaffdirectoryRoutingModule { }
