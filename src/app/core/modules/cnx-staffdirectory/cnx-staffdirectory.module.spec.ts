import { CnxStaffdirectoryModule } from './cnx-staffdirectory.module';

describe('CnxStaffdirectoryModule', () => {
  let cnxStaffdirectoryModule: CnxStaffdirectoryModule;

  beforeEach(() => {
    cnxStaffdirectoryModule = new CnxStaffdirectoryModule();
  });

  it('should create an instance', () => {
    expect(cnxStaffdirectoryModule).toBeTruthy();
  });
});
