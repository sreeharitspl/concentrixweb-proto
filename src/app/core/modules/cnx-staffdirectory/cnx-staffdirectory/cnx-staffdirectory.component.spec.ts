import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxStaffdirectoryComponent } from './cnx-staffdirectory.component';

describe('CnxStaffdirectoryComponent', () => {
  let component: CnxStaffdirectoryComponent;
  let fixture: ComponentFixture<CnxStaffdirectoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxStaffdirectoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxStaffdirectoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
