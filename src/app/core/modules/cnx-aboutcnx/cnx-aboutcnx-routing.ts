/*
* Created By : TSPL-T0475
* Date       : 14-Nov-2018
* Purpose    : Kudos page routing defenition.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CnxAboutcnxComponent } from './cnx-aboutcnx/cnx-aboutcnx.component';

const routes: Routes = [
  { path: 'aboutus', component: CnxAboutcnxComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CnxAboutcnxComponent
  ]
})

export class CnxAboutUsRoutingModule { }
