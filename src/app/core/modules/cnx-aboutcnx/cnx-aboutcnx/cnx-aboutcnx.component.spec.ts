import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxAboutcnxComponent } from './cnx-aboutcnx.component';

describe('CnxAboutcnxComponent', () => {
  let component: CnxAboutcnxComponent;
  let fixture: ComponentFixture<CnxAboutcnxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxAboutcnxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxAboutcnxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
