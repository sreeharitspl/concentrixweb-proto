import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxAboutcnxComponent } from './cnx-aboutcnx/cnx-aboutcnx.component';
import { CnxAboutUsRoutingModule } from './cnx-aboutcnx-routing';

@NgModule({
  imports: [
    CommonModule,
    CnxAboutUsRoutingModule
  ],
  declarations: []
})
export class CnxAboutcnxModule { }
