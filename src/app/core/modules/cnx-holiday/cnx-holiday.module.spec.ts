import { CnxHolidayModule } from './cnx-holiday.module';

describe('CnxHolidayModule', () => {
  let cnxHolidayModule: CnxHolidayModule;

  beforeEach(() => {
    cnxHolidayModule = new CnxHolidayModule();
  });

  it('should create an instance', () => {
    expect(cnxHolidayModule).toBeTruthy();
  });
});
