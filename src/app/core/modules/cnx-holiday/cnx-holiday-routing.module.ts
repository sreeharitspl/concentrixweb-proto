import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CnxHolidayComponent } from './cnx-holiday/cnx-holiday.component';

const routes: Routes = [{ path: 'holidays', component: CnxHolidayComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxHolidayComponent, ]
})
export class CnxHolidayRoutingModule { }
