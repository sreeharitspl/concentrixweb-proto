import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxHolidayComponent } from './cnx-holiday.component';

describe('CnxHolidayComponent', () => {
  let component: CnxHolidayComponent;
  let fixture: ComponentFixture<CnxHolidayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxHolidayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxHolidayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
