import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxHolidayRoutingModule } from './cnx-holiday-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxHolidayRoutingModule,
  ],
})
export class CnxHolidayModule { }
