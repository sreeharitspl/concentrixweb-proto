import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxTimeLineComponent } from './cnx-time-line.component';

describe('CnxTimeLineComponent', () => {
  let component: CnxTimeLineComponent;
  let fixture: ComponentFixture<CnxTimeLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxTimeLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxTimeLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
