import { Component, OnInit, Input } from '@angular/core';
import { CnxFeedService } from '../../../../shared/service/cnx-feed/cnx-feed.service';
import { CnxConstants } from '../../../../shared/modules/cnx-utils/cnx-constants/cnx-constants';
import { CnxLanguageService } from '../../../../shared/service/cnx-language/cnx-language.service';
@Component({
  selector: 'app-cnx-time-line',
  templateUrl: './cnx-time-line.component.html',
  styleUrls: ['./cnx-time-line.component.scss']
})
export class CnxTimeLineComponent implements OnInit {
  /* Infinit scroll properties */
  invokedStatus = true;
  throttle: number;
  scrollDistance: number;
  scrollUpDistance: number;
  direction = '';
  modalOpen = false;
  feedData = [];
  pageNumber = 0;
  /* Ends */

  /* input object defining */
  inputs: {
    PerPage: number;
    PageNumber: number;
  };
  postTypes: {
    CNC_NEWS: string;
    CNC_POLLS: string;
    CNC_Image: string;
    CNC_CheckIn: string;
    CNC_URL: string;
    CNC_Video: string;
    CNC_Alert: string;
    CNC_BirthAnniversary: string;
    CNC_WorkAnniversary: string;
  };
  /* End */

  /**to receive serach text from serach component */
  public searchText: string;
  /**search placeholder  */
  public searchPlaceholder: string;

  constructor(
    public feedService: CnxFeedService,
    public cnxLanguageService: CnxLanguageService
  ) {  }
  ngOnInit() {
    this.searchPlaceholder = CnxConstants.PLACEHOLDER.TIMELINESEARCH;
    this.throttle = CnxConstants.MODALSCROLLPARAMETERS.Throttle;
    this.scrollDistance = CnxConstants.MODALSCROLLPARAMETERS.ScrollDistance;
    this.scrollUpDistance = CnxConstants.MODALSCROLLPARAMETERS.ScrollUpDistance;
    this.getFeedDataFromSCroll();
    this.postTypes = CnxConstants.CNX_FEEDTYPEKEY;
  }

    /*
    * Author: T0446
    * Function: getFeedDataFromSCroll
    * Description:  This function used to fetch timeline data
    * Return: Feed data: array: Returns feed /posts array
    */
  getFeedDataFromSCroll() {
    if (this.invokedStatus) {
        this.invokedStatus = false;
        this.inputs = {
          PerPage: CnxConstants.PAGINATIONLENGTH.CNX_PAGINATION_COUNT,
          PageNumber: this.pageNumber += 1
        };
        const showLoader = false;
        this.feedService.getAllFeed('getHomePageFeeds', this.inputs, (respData: any) => {
          if (respData.Acknowledge === 1) {
            if (respData.Feeds && respData.Feeds.length < CnxConstants.PAGINATIONLENGTH.CNX_PAGINATION_COUNT) {
              this.invokedStatus = false;
            } else {
              this.invokedStatus = true;
            }
             this.setFeeddata(respData);
          } else {
            /**Dummy feed even if server error occur starts */
            alert(respData.Message);
            /**Dummy feed even if server error occur starts */
          }
        },
        (errData: any) => {
          this.invokedStatus = true;
          console.log('Something went wrong');
          /**Dummy feed even if server error occur starts */
          // this.setFeeddata(errData);
          /**Dummy feed even if server error occur starts */
        }, showLoader);
      }
  }

  /*
    * Author: T0446
    * Function: setFeeddata
    * Description:  This function used to append timeline data
    * Arguments:
    *     Arg 1: feed data : array
    */
  setFeeddata (feedDatas) {
    this.feedData = this.feedData.concat(feedDatas.Feeds);
  }

  /*
    * Author: T0475
    * Function: searchEventReceiver
    * Description:  This function used to receive serach text passed by search component
    * Arguments:
    *     Arg 1: event : string
    */
  searchEventReceiver($event) {
    this.searchText = $event;
  }
}
