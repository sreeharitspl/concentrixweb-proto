import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccordionModule } from 'ngx-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { CnxPostModule } from '../../../shared/modules/cnx-post/cnx-post.module';
import { CnxPostLCSModule } from '../../../shared/modules/cnx-post-lcs/cnx-post-lcs.module';
import { CnxTimeLineRoutingModule } from './cnx-time-line-routing/cnx-time-line-routing.module';
import { CnxSearchModule } from '../../../shared/modules/cnx-search/cnx-search.module';

@NgModule({
  imports: [
    CommonModule,
    InfiniteScrollModule,
    AccordionModule,
    CnxTimeLineRoutingModule,
    CnxPostModule,
    CnxPostLCSModule,
    CnxSearchModule
  ],
  declarations: [
  ],
  exports : [
  ]
})
export class CnxTimeLineModule { }
