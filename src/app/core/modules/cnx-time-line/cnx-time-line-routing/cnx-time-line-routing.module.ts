import { VideosContainerModule } from './../../../../shared/modules/cnx-post/videos-container/videos-container.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { CnxTimeLineComponent } from '../cnx-time-line/cnx-time-line.component';
import { CnxPostModule } from '../../../../shared/modules/cnx-post/cnx-post.module';
import { SharedModule } from '../../../../shared/shared.module';
import { CnxPostLCSModule } from '../../../../shared/modules/cnx-post-lcs/cnx-post-lcs.module';
import { NewsContainerModule } from '../../../../shared/modules/cnx-post/news-container/news-container.module';
import { CnxSearchModule } from '../../../../shared/modules/cnx-search/cnx-search.module';

const routes: Routes = [
  { path: 'timeline', component: CnxTimeLineComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    InfiniteScrollModule,
    CnxPostModule,
    SharedModule,
    CnxPostLCSModule,
    NewsContainerModule,
    VideosContainerModule,
    CnxSearchModule
  ],
  declarations: [
    CnxTimeLineComponent
  ]
})
export class CnxTimeLineRoutingModule { }
