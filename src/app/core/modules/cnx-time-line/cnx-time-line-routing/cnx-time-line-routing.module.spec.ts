import { CnxTimeLineRoutingModule } from './cnx-time-line-routing.module';

describe('CnxTimeLineRoutingModule', () => {
  let cnxTimeLineRoutingModule: CnxTimeLineRoutingModule;

  beforeEach(() => {
    cnxTimeLineRoutingModule = new CnxTimeLineRoutingModule();
  });

  it('should create an instance', () => {
    expect(cnxTimeLineRoutingModule).toBeTruthy();
  });
});
