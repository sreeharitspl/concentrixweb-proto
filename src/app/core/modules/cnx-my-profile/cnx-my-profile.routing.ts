/*
* Created By : TSPL-T0423
* Date       :19-Sept-2018
* Purpose    : Profile page routing defenition.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CnxMyProfileComponent } from './cnx-my-profile/cnx-my-profile.component';
import { CnxUserImageModule } from '../../../shared/modules/cnx-user-image/cnx-user-image.module';

const routes: Routes = [
  { path: 'profile', component: CnxMyProfileComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CnxUserImageModule
  ],
  declarations: [
    CnxMyProfileComponent
  ]
})

export class CnxMyProfileRoutingModule { }
