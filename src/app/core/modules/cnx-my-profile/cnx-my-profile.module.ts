import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxMyProfileRoutingModule } from './cnx-my-profile.routing';
import { CnxUserImageModule } from '../../../shared/modules/cnx-user-image/cnx-user-image.module';

@NgModule({
  imports: [
    CommonModule,
    CnxMyProfileRoutingModule,
    CnxUserImageModule
  ],
  declarations: []
})
export class CnxMyProfileModule { }
