import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cnx-my-profile',
  templateUrl: './cnx-my-profile.component.html',
  styleUrls: ['./cnx-my-profile.component.scss']
})
export class CnxMyProfileComponent implements OnInit {

  public profileImageUrl: string;
  constructor() { }

  ngOnInit() {
    this.profileImageUrl = 'https://concentrixone.concentrix.com/AdminPortalUAT/CnxonedataUat/MediaFiles/ProfileImages/50137144/Approved/cdv_photo_001-Oct252018052748769844981.jpg';
  }

}
