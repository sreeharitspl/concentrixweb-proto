import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxMyProfileComponent } from './cnx-my-profile.component';

describe('CnxMyProfileComponent', () => {
  let component: CnxMyProfileComponent;
  let fixture: ComponentFixture<CnxMyProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxMyProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxMyProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
