import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CnxNewJoineeComponent } from './cnx-new-joinee/cnx-new-joinee.component';
import { CnxSearchModule } from '../../../shared/modules/cnx-search/cnx-search.module';

const routes: Routes = [{ path: 'newjoinee', component: CnxNewJoineeComponent }];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CnxSearchModule],
  exports: [RouterModule],
  declarations: [CnxNewJoineeComponent]
})
export class CnxNewJoineeRoutingModule { }
