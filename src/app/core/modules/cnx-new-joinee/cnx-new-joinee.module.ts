import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxNewJoineeRoutingModule } from './cnx-new-joinee-routing.module';
import { CnxSearchModule } from '../../../shared/modules/cnx-search/cnx-search.module';

@NgModule({
  imports: [
    CommonModule,
    CnxNewJoineeRoutingModule,
    CnxSearchModule
  ],
})
export class CnxNewJoineeModule { }
