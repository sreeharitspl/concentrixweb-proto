import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxNewJoineeComponent } from './cnx-new-joinee.component';

describe('CnxNewJoineeComponent', () => {
  let component: CnxNewJoineeComponent;
  let fixture: ComponentFixture<CnxNewJoineeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxNewJoineeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxNewJoineeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
