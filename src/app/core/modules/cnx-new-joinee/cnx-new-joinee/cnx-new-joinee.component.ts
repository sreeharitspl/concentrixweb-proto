import { Component, OnInit } from '@angular/core';
import { CnxConstants } from '../../../../shared/modules/cnx-utils/cnx-constants/cnx-constants';

@Component({
  selector: 'app-cnx-new-joinee',
  templateUrl: './cnx-new-joinee.component.html',
  styleUrls: ['./cnx-new-joinee.component.scss']
})
export class CnxNewJoineeComponent implements OnInit {

  /**to receive serach text from serach component */
  public searchText: string;
  /**search placeholder  */
  public searchPlaceholder: string;

  constructor() { }

  ngOnInit() {
    this.searchPlaceholder = CnxConstants.PLACEHOLDER.NEWJOINEE;
  }

  /*
    * Author: T0475
    * Function: searchEventReceiver
    * Description:  This function used to receive serach text passed by search component
    * Arguments:
    *     Arg 1: event : string
    */
   searchEventReceiver($event) {
    this.searchText = $event;
  }
}
