import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxMessagesComponent } from './cnx-messages/cnx-messages.component';
import { CnxMessagesRoutingModule } from './cnx-messages-routing';

@NgModule({
  imports: [
    CommonModule,
    CnxMessagesRoutingModule
  ],
  declarations: []
})
export class CnxMessagesModule { }
