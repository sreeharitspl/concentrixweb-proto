import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cnx-messages',
  templateUrl: './cnx-messages.component.html',
  styleUrls: ['./cnx-messages.component.scss']
})
export class CnxMessagesComponent implements OnInit {
  viewMode: String;
  constructor() {
  }

  ngOnInit() {
    this.viewMode = 'cnx-message-tab1';
  }


}
