import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxMessagesComponent } from './cnx-messages.component';

describe('CnxMessagesComponent', () => {
  let component: CnxMessagesComponent;
  let fixture: ComponentFixture<CnxMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
