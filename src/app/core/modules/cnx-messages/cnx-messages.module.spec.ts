import { CnxMessagesModule } from './cnx-messages.module';

describe('CnxMessagesModule', () => {
  let cnxMessagesModule: CnxMessagesModule;

  beforeEach(() => {
    cnxMessagesModule = new CnxMessagesModule();
  });

  it('should create an instance', () => {
    expect(cnxMessagesModule).toBeTruthy();
  });
});
