/*
* Created By : TSPL-T0475
* Date       : 13-Nov-2018
* Purpose    : Mesage page routing definition.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CnxMessagesComponent } from './cnx-messages/cnx-messages.component';

const routes: Routes = [
  { path: 'messages', component: CnxMessagesComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CnxMessagesComponent
  ]
})

export class CnxMessagesRoutingModule { }
