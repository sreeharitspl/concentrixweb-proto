import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxAppFeedbackRoutingModule } from './cnx-app-feedback-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxAppFeedbackRoutingModule
  ],
  declarations: []
})
export class CnxAppFeedbackModule { }
