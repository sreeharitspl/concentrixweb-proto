import { CnxAppFeedbackModule } from './cnx-app-feedback.module';

describe('CnxAppFeedbackModule', () => {
  let cnxAppFeedbackModule: CnxAppFeedbackModule;

  beforeEach(() => {
    cnxAppFeedbackModule = new CnxAppFeedbackModule();
  });

  it('should create an instance', () => {
    expect(cnxAppFeedbackModule).toBeTruthy();
  });
});
