import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxAppFeedbackComponent } from './cnx-app-feedback/cnx-app-feedback.component';

const routes: Routes = [{ path: 'feedback', component: CnxAppFeedbackComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxAppFeedbackComponent]
})
export class CnxAppFeedbackRoutingModule { }
