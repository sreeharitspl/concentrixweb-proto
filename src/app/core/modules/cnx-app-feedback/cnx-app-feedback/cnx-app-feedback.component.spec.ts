import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxAppFeedbackComponent } from './cnx-app-feedback.component';

describe('CnxAppFeedbackComponent', () => {
  let component: CnxAppFeedbackComponent;
  let fixture: ComponentFixture<CnxAppFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxAppFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxAppFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
