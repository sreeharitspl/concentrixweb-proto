import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CnxUsefulAppsComponent } from './cnx-useful-apps/cnx-useful-apps.component';

const routes: Routes = [{ path: 'usefulapps', component: CnxUsefulAppsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxUsefulAppsComponent]
})
export class CnxUsefulAppsRoutingModule { }
