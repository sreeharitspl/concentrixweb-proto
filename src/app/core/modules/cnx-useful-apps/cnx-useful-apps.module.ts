import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxUsefulAppsRoutingModule } from './cnx-useful-apps-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxUsefulAppsRoutingModule
  ],
})
export class CnxUsefulAppsModule { }
