import { CnxUsefulAppsModule } from './cnx-useful-apps.module';

describe('CnxUsefulAppsModule', () => {
  let cnxUsefulAppsModule: CnxUsefulAppsModule;

  beforeEach(() => {
    cnxUsefulAppsModule = new CnxUsefulAppsModule();
  });

  it('should create an instance', () => {
    expect(cnxUsefulAppsModule).toBeTruthy();
  });
});
