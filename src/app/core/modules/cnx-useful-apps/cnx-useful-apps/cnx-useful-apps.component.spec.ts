import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxUsefulAppsComponent } from './cnx-useful-apps.component';

describe('CnxUsefulAppsComponent', () => {
  let component: CnxUsefulAppsComponent;
  let fixture: ComponentFixture<CnxUsefulAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxUsefulAppsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxUsefulAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
