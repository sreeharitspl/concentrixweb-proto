import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxPollsComponent } from './cnx-polls/cnx-polls.component';

const routes: Routes = [{ path: 'polls', component: CnxPollsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxPollsComponent]
})
export class CnxPollsRoutingModule { }
