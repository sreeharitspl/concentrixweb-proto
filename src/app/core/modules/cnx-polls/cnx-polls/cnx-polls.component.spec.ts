import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxPollsComponent } from './cnx-polls.component';

describe('CnxPollsComponent', () => {
  let component: CnxPollsComponent;
  let fixture: ComponentFixture<CnxPollsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxPollsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxPollsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
