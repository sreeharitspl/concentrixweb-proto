import { CnxPollsModule } from './cnx-polls.module';

describe('CnxPollsModule', () => {
  let cnxPollsModule: CnxPollsModule;

  beforeEach(() => {
    cnxPollsModule = new CnxPollsModule();
  });

  it('should create an instance', () => {
    expect(cnxPollsModule).toBeTruthy();
  });
});
