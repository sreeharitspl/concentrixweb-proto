import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxPollsRoutingModule } from './cnx-polls-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxPollsRoutingModule
  ],
  declarations: []
})
export class CnxPollsModule { }
