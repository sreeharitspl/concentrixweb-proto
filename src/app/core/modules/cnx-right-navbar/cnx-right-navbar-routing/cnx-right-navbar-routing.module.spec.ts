import { CnxRightNavbarRoutingModule } from './cnx-right-navbar-routing.module';

describe('CnxRightNavbarRoutingModule', () => {
  let cnxRightNavbarRoutingModule: CnxRightNavbarRoutingModule;

  beforeEach(() => {
    cnxRightNavbarRoutingModule = new CnxRightNavbarRoutingModule();
  });

  it('should create an instance', () => {
    expect(cnxRightNavbarRoutingModule).toBeTruthy();
  });
});
