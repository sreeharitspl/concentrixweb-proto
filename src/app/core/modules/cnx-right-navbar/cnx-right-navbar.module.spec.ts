import { CnxRightNavbarModule } from './cnx-right-navbar.module';

describe('CnxRightNavbarModule', () => {
  let cnxRightNavbarModule: CnxRightNavbarModule;

  beforeEach(() => {
    cnxRightNavbarModule = new CnxRightNavbarModule();
  });

  it('should create an instance', () => {
    expect(cnxRightNavbarModule).toBeTruthy();
  });
});
