import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxNavKudosComponent } from './cnx-nav-kudos.component';

describe('CnxNavKudosComponent', () => {
  let component: CnxNavKudosComponent;
  let fixture: ComponentFixture<CnxNavKudosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxNavKudosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxNavKudosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
