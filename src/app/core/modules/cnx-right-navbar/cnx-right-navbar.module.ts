import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxRightSidebarComponent } from './cnx-right-sidebar/cnx-right-sidebar.component';
import { CnxNavKudosComponent } from './cnx-nav-kudos/cnx-nav-kudos.component';
import { CnxNavServiceComponent } from './cnx-nav-service/cnx-nav-service.component';
import { CnxUserImageModule } from '../../../shared/modules/cnx-user-image/cnx-user-image.module';

@NgModule({
  imports: [
    CommonModule,
    CnxUserImageModule
  ],
  exports: [
    CnxRightSidebarComponent
  ],
  declarations: [
    CnxRightSidebarComponent,
    CnxNavKudosComponent,
    CnxNavServiceComponent
  ]
})
export class CnxRightNavbarModule { }
