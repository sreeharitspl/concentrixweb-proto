import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxNavServiceComponent } from './cnx-nav-service.component';

describe('CnxNavServiceComponent', () => {
  let component: CnxNavServiceComponent;
  let fixture: ComponentFixture<CnxNavServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxNavServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxNavServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
