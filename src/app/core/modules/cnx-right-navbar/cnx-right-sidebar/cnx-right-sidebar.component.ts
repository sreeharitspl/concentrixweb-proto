import { Component, OnInit, Input } from '@angular/core';
import { CnxConstants } from '../../../../shared/modules/cnx-utils/cnx-constants/cnx-constants';

@Component({
  selector: 'app-cnx-right-sidebar',
  templateUrl: './cnx-right-sidebar.component.html',
  styleUrls: ['./cnx-right-sidebar.component.scss']
})
export class CnxRightSidebarComponent implements OnInit {
  @Input() defaultImage = './assets/images/default_bio.png';
  public emergencyIcon: string;

  constructor() { }

  ngOnInit() {
    this.defaultImage = 'assets/images/default_user.svg';
    this.emergencyIcon = 'assets/images/emergency.png';

  }

  // getDefaultAvatar() {
  //   this.defaultImage = CnxConstants.DEFAULTAVATARURL;
  // }
}
