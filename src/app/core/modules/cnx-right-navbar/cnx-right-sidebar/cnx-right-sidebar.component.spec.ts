import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxRightSidebarComponent } from './cnx-right-sidebar.component';

describe('CnxRightSidebarComponent', () => {
  let component: CnxRightSidebarComponent;
  let fixture: ComponentFixture<CnxRightSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxRightSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxRightSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
