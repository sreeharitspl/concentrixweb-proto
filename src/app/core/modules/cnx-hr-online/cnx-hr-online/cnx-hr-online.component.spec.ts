import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxHrOnlineComponent } from './cnx-hr-online.component';

describe('CnxHrOnlineComponent', () => {
  let component: CnxHrOnlineComponent;
  let fixture: ComponentFixture<CnxHrOnlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxHrOnlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxHrOnlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
