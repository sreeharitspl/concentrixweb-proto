import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxHrOnlineRoutingModule } from './cnx-hr-online-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxHrOnlineRoutingModule
  ],
})
export class CnxHrOnlineModule { }
