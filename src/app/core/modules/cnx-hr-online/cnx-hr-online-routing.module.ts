import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CnxHrOnlineComponent } from './cnx-hr-online/cnx-hr-online.component';

const routes: Routes = [{ path: 'hronline', component: CnxHrOnlineComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxHrOnlineComponent]
})
export class CnxHrOnlineRoutingModule { }
