import { CnxFriendsModule } from './cnx-friends.module';

describe('CnxFriendsModule', () => {
  let cnxFriendsModule: CnxFriendsModule;

  beforeEach(() => {
    cnxFriendsModule = new CnxFriendsModule();
  });

  it('should create an instance', () => {
    expect(cnxFriendsModule).toBeTruthy();
  });
});
