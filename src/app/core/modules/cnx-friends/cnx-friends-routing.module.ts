import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { CnxFriendsComponent } from './cnx-friends/cnx-friends.component';

const routes: Routes = [{ path: 'friends', component: CnxFriendsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes),CommonModule],
  exports: [RouterModule],
  declarations: [CnxFriendsComponent]
})
export class CnxFriendsRoutingModule { }
