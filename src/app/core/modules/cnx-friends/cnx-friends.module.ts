import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxFriendsRoutingModule } from './cnx-friends-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxFriendsRoutingModule
  ],
})
export class CnxFriendsModule { }
