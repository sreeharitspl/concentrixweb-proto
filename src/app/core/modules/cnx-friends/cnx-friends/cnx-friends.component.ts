import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cnx-friends',
  templateUrl: './cnx-friends.component.html',
  styleUrls: ['./cnx-friends.component.scss']
})
export class CnxFriendsComponent implements OnInit {

  public viewMode: String;
  constructor() { }

  ngOnInit() {
    this.viewMode = 'tab1';
  }

}
