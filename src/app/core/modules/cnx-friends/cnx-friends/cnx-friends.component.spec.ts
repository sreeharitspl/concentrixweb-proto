import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxFriendsComponent } from './cnx-friends.component';

describe('CnxFriendsComponent', () => {
  let component: CnxFriendsComponent;
  let fixture: ComponentFixture<CnxFriendsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxFriendsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxFriendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
