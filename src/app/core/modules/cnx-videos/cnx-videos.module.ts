import { VideosContainerModule } from './../../../shared/modules/cnx-post/videos-container/videos-container.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxVideosRoutingModule } from './cnx-videos-routing';
import { CnxNoDataDisplayModule } from '../../../shared/modules/cnx-no-data-display/cnx-no-data-display.module';
import { CnxPostLCSModule } from '../../../shared/modules/cnx-post-lcs/cnx-post-lcs.module';

@NgModule({
  imports: [
    CommonModule,
    CnxVideosRoutingModule,
    VideosContainerModule,
    CnxNoDataDisplayModule,
    CnxPostLCSModule
  ],
  declarations: []
})
export class CnxVideosModule { }
