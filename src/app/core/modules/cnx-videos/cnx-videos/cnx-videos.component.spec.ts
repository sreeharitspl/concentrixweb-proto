import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxVideosComponent } from './cnx-videos.component';

describe('CnxVideosComponent', () => {
  let component: CnxVideosComponent;
  let fixture: ComponentFixture<CnxVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
