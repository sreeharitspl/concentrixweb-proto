import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit } from '@angular/core';
import { CnxVideosService } from '../cnx-videos.service';
import { CnxConstants } from '../../../../shared/modules/cnx-utils/cnx-constants/cnx-constants';
import { CnxDummyFeed } from '../../../../report/api-source/cnx-dummy-feed';

@Component({
  selector: 'app-cnx-videos',
  templateUrl: './cnx-videos.component.html',
  styleUrls: ['./cnx-videos.component.scss']
})
export class CnxVideosComponent implements OnInit {

  public feedType: string;
  public pageNumber: number;
  inputs: {
    PerPage: number,
    PageNumber: number
  };
  videosData = [];
  messageIfNoData: string;

  constructor(public cnxVideoService: CnxVideosService) { }

  ngOnInit() {
    this.pageNumber = 0;
    this.feedType = CnxConstants.CNX_FEEDTYPEKEY['CNC_Video'];
    this.getVideosList();
  }

  /*
   * Function: getVideosList
   * Description:  to get videos list
   * Arguments: Nil
   * Return: Nil
   */
  public getVideosList() {
    this.inputs = {
      PerPage: CnxConstants.PAGINATIONLENGTH.CNX_PAGINATION_COUNT,
      PageNumber: this.pageNumber += 1
    };
    const showLoader = false;
    this.videosData.push(CnxDummyFeed.CNXDUMMYFEEDARRAY['Feeds'][0]); // code for dummy data
    // this.cnxVideoService.getVideosList('GetAllVideos', this.inputs, (respData: any) => {
    //   if (respData.Acknowledge === 1 && respData.VideoList) {
    //     this.messageIfNoData = respData.Message;
    //     this.videosData = respData.VideoList;
    //     console.log('this.videosData ', this.videosData);
    //   } else {
    //     this.messageIfNoData = respData.ErrorCode[0].Code;
    //   }
    // },
    //   (errData: any) => {
    //     console.log('Something went wrong');
    //   }, showLoader);

  }

}
