import { TestBed, inject } from '@angular/core/testing';

import { CnxVideosService } from './cnx-videos.service';

describe('CnxVideosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxVideosService]
    });
  });

  it('should be created', inject([CnxVideosService], (service: CnxVideosService) => {
    expect(service).toBeTruthy();
  }));
});
