/*
* Created By : TSPL-T0475
* Date       : 13-Nov-2018
* Purpose    : Videos page routing definition.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CnxVideosComponent } from './cnx-videos/cnx-videos.component';
import { CnxPostLCSModule } from '../../../shared/modules/cnx-post-lcs/cnx-post-lcs.module';

import { VideosContainerModule } from './../../../shared/modules/cnx-post/videos-container/videos-container.module';
import { CnxNoDataDisplayModule } from 'src/app/shared/modules/cnx-no-data-display/cnx-no-data-display.module';

const routes: Routes = [
  { path: 'videos', component: CnxVideosComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CnxNoDataDisplayModule,
    VideosContainerModule,
    CnxPostLCSModule
  ],
  declarations: [
    CnxVideosComponent
  ]
})

export class CnxVideosRoutingModule { }
