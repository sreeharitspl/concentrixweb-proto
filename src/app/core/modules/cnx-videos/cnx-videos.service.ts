import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared/service/http/http.service';
import { CnxGlobalService } from '../../../shared/modules/cnx-utils/cnx-global/cnx-global.service';
import { CnxVideoModel } from '../../../shared/models/cnx-post-models/cnx-video-model';

@Injectable({
  providedIn: 'root'
})
export class CnxVideosService {

  constructor(
    private httpService: HttpService,
    private cnxGlobalService: CnxGlobalService
  ) { }

  /*
 * Function: getVideosList
 * Description:  to get videos list
 * Arguments: Nil
 * Return: Nil
 */
  public getVideosList(url, params, cbSuccess, cbError, showLoader?) {
    if (showLoader) {
      this.cnxGlobalService.showLoader();
    }
    this.httpService.postX(url, params)
      .subscribe(result => {
        result = this.mapVideo(result);
        cbSuccess(result);
      },
        error => cbError(error)
      );
  }
  private mapVideo(result) {
    if (result.VideoList) {
      result.VideosData.forEach((data, index) => {
        data = new CnxVideoModel().mapVideoData(data, data.FeedType);
      });
    }
    return result;
  }
}
