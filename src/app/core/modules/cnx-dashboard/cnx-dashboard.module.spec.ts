import { CnxDashboardModule } from './cnx-dashboard.module';

describe('CnxDashboardModule', () => {
  let cnxDashboardModule: CnxDashboardModule;

  beforeEach(() => {
    cnxDashboardModule = new CnxDashboardModule();
  });

  it('should create an instance', () => {
    expect(cnxDashboardModule).toBeTruthy();
  });
});
