import { TestBed, inject } from '@angular/core/testing';

import { CnxNavbarService } from './cnx-navbar.service';

describe('CnxNavbarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CnxNavbarService]
    });
  });

  it('should be created', inject([CnxNavbarService], (service: CnxNavbarService) => {
    expect(service).toBeTruthy();
  }));
});
