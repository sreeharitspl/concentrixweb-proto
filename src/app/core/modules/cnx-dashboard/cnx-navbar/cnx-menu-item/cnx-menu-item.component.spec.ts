import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxMenuItemComponent } from './cnx-menu-item.component';

describe('CnxMenuItemComponent', () => {
  let component: CnxMenuItemComponent;
  let fixture: ComponentFixture<CnxMenuItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxMenuItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxMenuItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
