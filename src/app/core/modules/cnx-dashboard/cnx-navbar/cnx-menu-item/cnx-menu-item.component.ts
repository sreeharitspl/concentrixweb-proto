import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

import { CnxLanguageService } from '../../../../../shared/service/cnx-language/cnx-language.service';
import { CnxConstants } from '../../../../../shared/modules/cnx-utils/cnx-constants/cnx-constants';
import { CnxGlobalService } from '../../../../../shared/modules/cnx-utils/cnx-global/cnx-global.service';

@Component({
  selector: 'app-cnx-menu-item',
  templateUrl: './cnx-menu-item.component.html',
  styleUrls: ['./cnx-menu-item.component.scss']
})
export class CnxMenuItemComponent implements OnInit {

  @Input() option: any;
  @Input() optionType: string;
  public pageUrl: string;
  constructor(public cnxLanguageService: CnxLanguageService,
    public globalService: CnxGlobalService, public router: Router) { }

  ngOnInit() {
  }

  /*
   * Author: T0423
   * Function: toggleSubMenu
   * Description:  Show or hide submenu
   */
  toggleSubMenu(option) {
    option.isShow = !option.isShow;
  }

  /*
   * Author: T0423
   * Function: goToOption
   * Description:  Navigate to clicked page
   */
  goToOption(option) {
    if (option.Code === CnxConstants.APPLICATIONCONSTANTS.CNX_SOCIAL_SUBMENU) {
      this.router.navigate([this.router.url]);
      window.open(option.URL, '_blank');
    }
    if (option.Code === CnxConstants.APPLICATIONCONSTANTS.SEI) {
      this.globalService.navigateTo([CnxConstants.SEIPAGEURL.URL]);
    }
    if (option.URL && option.Code !== CnxConstants.APPLICATIONCONSTANTS.CNX_SOCIAL_SUBMENU) {
      this.globalService.navigateTo([option.URL]);
    }
  }

}
