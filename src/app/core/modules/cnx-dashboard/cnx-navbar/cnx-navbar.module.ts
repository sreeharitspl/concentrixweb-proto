import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxLeftSidebarComponent } from './cnx-left-sidebar/cnx-left-sidebar.component';
import { CnxMenuItemComponent } from './cnx-menu-item/cnx-menu-item.component';
import { SharedModule } from '../../../../shared/shared.module';
import { CnxUserImageModule } from '../../../../shared/modules/cnx-user-image/cnx-user-image.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CnxUserImageModule
  ],
  declarations: [
    CnxLeftSidebarComponent,
    CnxMenuItemComponent
  ],
  exports: [
    CnxLeftSidebarComponent,
    CnxMenuItemComponent
  ]
})
export class CnxNavbarModule { }
