import { Component, OnInit, Input } from '@angular/core';

import { CnxMenu } from '../../../../../shared/modules/cnx-utils/cnx-constants/cnx-menu';
import { CnxLanguageService } from '../../../../../shared/service/cnx-language/cnx-language.service';
import { CnxLocalStorageService } from '../../../../../shared/service/cnx-local-storage/cnx-local-storage.service';
import { CnxConstants } from '../../../../../shared/modules/cnx-utils/cnx-constants/cnx-constants';
import { CnxNavbarService } from '../cnx-navbar.service';

@Component({
  selector: 'app-cnx-left-sidebar',
  templateUrl: './cnx-left-sidebar.component.html',
  styleUrls: ['./cnx-left-sidebar.component.scss']
})
export class CnxLeftSidebarComponent implements OnInit {

  public menuOptions: any[] = [];
  public userDetails: any;

  constructor(public cnxLanguageService: CnxLanguageService, private localStorage: CnxLocalStorageService,
    public navbarService: CnxNavbarService) { }

  ngOnInit() {
    // Initialisations
    const userDetails = this.localStorage.getItem(CnxConstants.LOCALSTORAGEKEYS.CNX_LOGIN_USER_DETAILS);
    this.userDetails = userDetails ? JSON.parse(userDetails) : {};
    this.menuOptions = CnxMenu.CNXMENU;

    // Fetching the social media list
    this.getSocialMedia();
  }

  /**
    * Author: TSPL-T0423
    * Created Date: 13 Sept 2018
    * Method Name: getSocialMedia
    * Description: To get the options in social media option of side menu
    * Arguments:
    *     No arguments
    * Return:
    *     void
    */
  public getSocialMedia() {
    const showLoader = false;
    const requestObject = {
        'FeatureCode': CnxConstants.CNX_FEATURECODES.CNX_SOCMEDHAN
    };

    this.navbarService.getSocialMedia('GenericFeatures', requestObject, (respData: any) => {
      if (respData.Acknowledge === 1) {
        if (respData.FeatureContent.length > 0) {
          let menuOptions = this.menuOptions.find(item => item.ItemName === CnxConstants.APPLICATIONCONSTANTS.CNX_MNU_SOCIAL);
          for (let loopVar = 0; loopVar < respData.FeatureContent.length; loopVar++) {
            menuOptions.Submenu.push(
              {
                'ItemId': menuOptions.ItemId + ( (loopVar + 1) * .1),
                'ItemName': respData.FeatureContent[loopVar].ConcentTitle,
                'Icon': '',
                'State': '',
                'Code': CnxConstants.APPLICATIONCONSTANTS.CNX_SOCIAL_SUBMENU,
                'URL': respData.FeatureContent[loopVar].Content,
                'Type': '',
                'IconClass': respData.FeatureContent[loopVar].ConcentTitle.toLowerCase()
              }
            );
          }
        }
      } else {
        alert(respData.Message);
      }
    },
      (errData: any) => {
        alert(errData.statusText);
      }, showLoader);
  }

}
