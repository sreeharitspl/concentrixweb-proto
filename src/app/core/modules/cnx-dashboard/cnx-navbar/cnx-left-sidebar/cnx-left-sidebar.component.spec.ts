import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxLeftSidebarComponent } from './cnx-left-sidebar.component';

describe('CnxLeftSidebarComponent', () => {
  let component: CnxLeftSidebarComponent;
  let fixture: ComponentFixture<CnxLeftSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxLeftSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxLeftSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
