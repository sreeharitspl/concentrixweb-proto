import { Injectable } from '@angular/core';
import { CnxGlobalService } from '../../../../shared/modules/cnx-utils/cnx-global/cnx-global.service';
import { HttpService } from '../../../../shared/service/http/http.service';

@Injectable({
  providedIn: 'root'
})
export class CnxNavbarService {

  constructor(private cnxGlobalService: CnxGlobalService, private httpService: HttpService) { }

  /*
   * Author: T0423
   * Function: getSocialMedia
   * Description:  For getting the social media list
   */
  public getSocialMedia(url, params, cbSuccess, cbError, showLoader?) {
    if (showLoader) {
      this.cnxGlobalService.showLoader();
    }
    this.httpService.postX(url, params)
    .subscribe(result => {
      cbSuccess(result);
    },
    error => cbError(error)
    );
  }
}
