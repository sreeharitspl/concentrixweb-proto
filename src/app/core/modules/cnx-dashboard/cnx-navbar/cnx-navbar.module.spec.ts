import { CnxNavbarModule } from './cnx-navbar.module';

describe('CnxNavbarModule', () => {
  let cnxNavbarModule: CnxNavbarModule;

  beforeEach(() => {
    cnxNavbarModule = new CnxNavbarModule();
  });

  it('should create an instance', () => {
    expect(cnxNavbarModule).toBeTruthy();
  });
});
