import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxDashboardRoutingModule } from './cnx-dashboard.routing/cnx-dashboard.routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxDashboardRoutingModule
  ],
  declarations: [],
  exports: []
})
export class CnxDashboardModule { }
