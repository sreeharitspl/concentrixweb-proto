import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanDashboardComponent } from './can-dashboard.component';

describe('CanDashboardComponent', () => {
  let component: CanDashboardComponent;
  let fixture: ComponentFixture<CanDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
