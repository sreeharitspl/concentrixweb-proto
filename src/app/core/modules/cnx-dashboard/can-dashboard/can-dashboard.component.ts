import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { CnxLanguageService } from '../../../../shared/service/cnx-language/cnx-language.service';

@Component({
  selector: 'app-can-dashboard',
  templateUrl: './can-dashboard.component.html',
  styleUrls: ['./can-dashboard.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class CanDashboardComponent implements OnInit {

  constructor(public cnxLanguageService: CnxLanguageService) { }

  ngOnInit() {
  }

}
