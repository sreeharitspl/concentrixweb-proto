import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CanDashboardComponent } from '../can-dashboard/can-dashboard.component';
import { CnxDashboardmenuModule } from '../../../../shared/modules/cnx-dashboardmenu/cnx-dashboardmenu.module';
import { SharedModule } from '../../../../shared/shared.module';
import { CnxTimeLineModule } from '../../cnx-time-line/cnx-time-line.module';
import { CnxNavbarModule } from '../cnx-navbar/cnx-navbar.module';
import { CnxRightNavbarModule } from '../../cnx-right-navbar/cnx-right-navbar.module';
import { CnxMyProfileModule } from '../../cnx-my-profile/cnx-my-profile.module';
import { CnxKudosModule } from '../../cnx-kudos/cnx-kudos.module';
import { CnxMessagesModule } from '../../cnx-messages/cnx-messages.module';
import { CnxVideosModule } from '../../cnx-videos/cnx-videos.module';
import { CnxNewsModule } from '../../cnx-news/cnx-news.module';
import { CnxAboutcnxModule } from '../../cnx-aboutcnx/cnx-aboutcnx.module';
import { CnxHeaderModule } from '../../../../shared/modules/cnx-header/cnx-header.module';


const routes: Routes = [
  {
    path: 'content', component: CanDashboardComponent,
    children: [
      { path: 'feeds',    loadChildren: '../../cnx-time-line/cnx-time-line.module#CnxTimeLineModule' },
      { path: 'profile',  loadChildren: '../../cnx-my-profile/cnx-my-profile.module#CnxMyProfileModule' },
      { path: 'kudos',    loadChildren: '../../cnx-kudos/cnx-kudos.module#CnxKudosModule' },
      { path: 'staffdirectory',    loadChildren: '../../cnx-staffdirectory/cnx-staffdirectory.module#CnxStaffdirectoryModule' },
      { path: 'sei',      loadChildren: '../../cnx-sei/cnx-sei.module#CnxSeiModule' },
      { path: 'messages', loadChildren: '../../cnx-messages/cnx-messages.module#CnxMessagesModule' },
      { path: 'news',     loadChildren: '../../cnx-news/cnx-news.module#CnxNewsModule' },
      { path: 'videos',   loadChildren: '../../cnx-videos/cnx-videos.module#CnxVideosModule' },
      { path: 'aboutus', loadChildren: '../../cnx-aboutcnx/cnx-aboutcnx.module#CnxAboutcnxModule'},
      { path: 'contactus', loadChildren: '../../cnx-contactus/cnx-contactus.module#CnxContactusModule'},
      { path: 'myteam',    loadChildren: '../../cnx-my-team/cnx-my-team.module#CnxMyTeamModule' },
      { path: 'newjoinee',    loadChildren: '../../cnx-new-joinee/cnx-new-joinee.module#CnxNewJoineeModule' },
      { path: 'friends',    loadChildren: '../../cnx-friends/cnx-friends.module#CnxFriendsModule' },
      { path: 'usefulapps',    loadChildren: '../../cnx-useful-apps/cnx-useful-apps.module#CnxUsefulAppsModule' },
      { path: 'benefits',    loadChildren: '../../cnx-benefits/cnx-benefits.module#CnxBenefitsModule' },
      { path: 'hronline',    loadChildren: '../../cnx-hr-online/cnx-hr-online.module#CnxHrOnlineModule' },
      { path: 'holidays',    loadChildren: '../../cnx-holiday/cnx-holiday.module#CnxHolidayModule' },
      { path: 'incidentreporting', loadChildren: '../../cnx-incident-reporting/cnx-incident-reporting.module#CnxIncidentReportingModule' },
      { path: 'helpdesk',    loadChildren: '../../cnx-helpdesk/cnx-helpdesk.module#CnxHelpdeskModule' },
      { path: 'checkin',    loadChildren: '../../cnx-check-in/cnx-check-in.module#CnxCheckInModule' },
      { path: 'sharedcontent',    loadChildren: '../../cnx-shared-content/cnx-shared-content.module#CnxSharedContentModule' },
      { path: 'polls',    loadChildren: '../../cnx-polls/cnx-polls.module#CnxPollsModule' },
      { path: 'locations',    loadChildren: '../../cnx-locations/cnx-locations.module#CnxLocationsModule' },
      { path: 'culture',    loadChildren: '../../cnx-culture/cnx-culture.module#CnxCultureModule' },
      { path: 'operatingphilosophy', loadChildren: '../../cnx-operatingphilosophy/cnx-operatingphilosophy.module#CnxOperatingphilosophyModule' },
      { path: 'termsofuse',    loadChildren: '../../cnx-terms-of-use/cnx-terms-of-use.module#CnxTermsOfUseModule' },
      { path: 'privacypolicy',    loadChildren: '../../cnx-privacy-policy/cnx-privacy-policy.module#CnxPrivacyPolicyModule' },
      { path: 'appinfo',    loadChildren: '../../cnx-app-info/cnx-app-info.module#CnxAppInfoModule' },
      { path: 'feedback',    loadChildren: '../../cnx-app-feedback/cnx-app-feedback.module#CnxAppFeedbackModule' },
      { path: 'training',    loadChildren: '../../cnx-training/cnx-training.module#CnxTrainingModule' },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    CnxNavbarModule,
    CnxRightNavbarModule,
    CnxTimeLineModule,
    CnxDashboardmenuModule,
    CnxMyProfileModule,
    CnxKudosModule,
    CnxMessagesModule,
    CnxNewsModule,
    CnxVideosModule,
    CnxAboutcnxModule,
    CnxHeaderModule
  ],
  declarations: [
    CanDashboardComponent,
  ]
})
export class CnxDashboardRoutingModule { }
