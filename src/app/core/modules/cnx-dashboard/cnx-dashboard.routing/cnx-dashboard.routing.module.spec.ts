import { CnxDashboard.RoutingModule } from './cnx-dashboard.routing.module';

describe('CnxDashboard.RoutingModule', () => {
  let cnxDashboardRoutingModule: CnxDashboard.RoutingModule;

  beforeEach(() => {
    cnxDashboardRoutingModule = new CnxDashboard.RoutingModule();
  });

  it('should create an instance', () => {
    expect(cnxDashboardRoutingModule).toBeTruthy();
  });
});
