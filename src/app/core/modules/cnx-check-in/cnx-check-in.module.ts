import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CnxCheckInRoutingModule } from './cnx-check-in-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CnxCheckInRoutingModule
  ],
  declarations: []
})
export class CnxCheckInModule { }
