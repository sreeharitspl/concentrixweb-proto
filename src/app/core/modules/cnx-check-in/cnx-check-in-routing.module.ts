import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CnxCheckInComponent } from './cnx-check-in/cnx-check-in.component';

const routes: Routes = [{ path: 'checkin', component: CnxCheckInComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [CnxCheckInComponent]
})
export class CnxCheckInRoutingModule { }
