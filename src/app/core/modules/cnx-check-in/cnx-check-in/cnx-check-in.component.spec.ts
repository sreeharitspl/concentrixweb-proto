import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxCheckInComponent } from './cnx-check-in.component';

describe('CnxCheckInComponent', () => {
  let component: CnxCheckInComponent;
  let fixture: ComponentFixture<CnxCheckInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxCheckInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxCheckInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
