import { CnxLocationsModule } from './cnx-locations.module';

describe('CnxLocationsModule', () => {
  let cnxLocationsModule: CnxLocationsModule;

  beforeEach(() => {
    cnxLocationsModule = new CnxLocationsModule();
  });

  it('should create an instance', () => {
    expect(cnxLocationsModule).toBeTruthy();
  });
});
