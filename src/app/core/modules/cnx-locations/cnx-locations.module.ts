import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CnxLocationsRoutingModule } from './cnx-locations-routing';

@NgModule({
  imports: [
    CommonModule,
    CnxLocationsRoutingModule
  ],
  declarations: []
})
export class CnxLocationsModule { }
