/*
* Created By : TSPL-T0423
* Date       : 19-Sept-2018
* Purpose    : Locations page routing defenition.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { CnxLocationsComponent } from './cnx-locations/cnx-locations.component';

const routes: Routes = [
  { path: 'locations', component: CnxLocationsComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CnxLocationsComponent
  ]
})

export class CnxLocationsRoutingModule { }
