import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CnxLocationsComponent } from './cnx-locations.component';

describe('CnxLocationsComponent', () => {
  let component: CnxLocationsComponent;
  let fixture: ComponentFixture<CnxLocationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnxLocationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CnxLocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
